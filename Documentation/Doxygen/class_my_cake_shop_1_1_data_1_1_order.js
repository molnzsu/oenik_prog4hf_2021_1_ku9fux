var class_my_cake_shop_1_1_data_1_1_order =
[
    [ "Order", "class_my_cake_shop_1_1_data_1_1_order.html#a9799b1579c6a5e4821cb348238b30d68", null ],
    [ "Order", "class_my_cake_shop_1_1_data_1_1_order.html#a7fc335fbf737eaf6797403b2fa65d960", null ],
    [ "Equals", "class_my_cake_shop_1_1_data_1_1_order.html#a7f285aaea1db05fbfc0bc5d4d1d9ca18", null ],
    [ "GetHashCode", "class_my_cake_shop_1_1_data_1_1_order.html#aba8d18130e170e658bf60ab22218e9ca", null ],
    [ "ToString", "class_my_cake_shop_1_1_data_1_1_order.html#aaf3d4fa3ed31bd100dd1f48d72b6b80d", null ],
    [ "Cake", "class_my_cake_shop_1_1_data_1_1_order.html#a502d8164e07cfe80823b113cae3e5b23", null ],
    [ "CakeID", "class_my_cake_shop_1_1_data_1_1_order.html#a8249016e2d407b9fbf8c356a775163f0", null ],
    [ "Customer", "class_my_cake_shop_1_1_data_1_1_order.html#af53f92449c363227087802906b8689ca", null ],
    [ "CustomerID", "class_my_cake_shop_1_1_data_1_1_order.html#a12594a5623932655e0bdd79230e2d4bc", null ],
    [ "OrderDate", "class_my_cake_shop_1_1_data_1_1_order.html#a2b31078149f6af266ddf536a411ade6f", null ],
    [ "OrderID", "class_my_cake_shop_1_1_data_1_1_order.html#af665d131bc48e549e60e0a3e96b9a3af", null ],
    [ "OrderStatus", "class_my_cake_shop_1_1_data_1_1_order.html#a13eb1f3a57165898e550c2c6f4936994", null ]
];