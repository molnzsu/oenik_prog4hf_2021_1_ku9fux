var class_my_cake_shop_1_1_wpf_client_1_1_cake_v_m =
[
    [ "CopyFrom", "class_my_cake_shop_1_1_wpf_client_1_1_cake_v_m.html#a960607cecdab6a9b5fc3795c2d7a93d0", null ],
    [ "ExpirationDate", "class_my_cake_shop_1_1_wpf_client_1_1_cake_v_m.html#aa251c19cac2d5da8ef8730517a7a8f1d", null ],
    [ "Id", "class_my_cake_shop_1_1_wpf_client_1_1_cake_v_m.html#ac55913db260ee0bc5b44f0d6c5771bdb", null ],
    [ "LactoseFree", "class_my_cake_shop_1_1_wpf_client_1_1_cake_v_m.html#adfd25cb9f885655291cbdc68a1b2d3b9", null ],
    [ "MadeDate", "class_my_cake_shop_1_1_wpf_client_1_1_cake_v_m.html#a46a3cf17e4873b64d9dc3a9e090c4272", null ],
    [ "MakerId", "class_my_cake_shop_1_1_wpf_client_1_1_cake_v_m.html#a68f9ad56cb44c3498005bd3775b9008e", null ],
    [ "Name", "class_my_cake_shop_1_1_wpf_client_1_1_cake_v_m.html#a49fc5c6e8c1738aa977ef7d641bf7ac8", null ],
    [ "Popularity", "class_my_cake_shop_1_1_wpf_client_1_1_cake_v_m.html#ac15b66ff7fdfa28bf05d12ee9f497809", null ],
    [ "Price", "class_my_cake_shop_1_1_wpf_client_1_1_cake_v_m.html#a00cf2b81933b3dd1acee99592689b166", null ],
    [ "Type", "class_my_cake_shop_1_1_wpf_client_1_1_cake_v_m.html#a4717155ed931445e7d14fd89a174cb59", null ]
];