var class_my_cake_shop_1_1_wpf_app_1_1_v_m_1_1_main_view_model =
[
    [ "MainViewModel", "class_my_cake_shop_1_1_wpf_app_1_1_v_m_1_1_main_view_model.html#abf9f0cba678b56157dbe045f6583c8f9", null ],
    [ "MainViewModel", "class_my_cake_shop_1_1_wpf_app_1_1_v_m_1_1_main_view_model.html#ad8633b1b52910af28633b5577d51ce89", null ],
    [ "AddCommand", "class_my_cake_shop_1_1_wpf_app_1_1_v_m_1_1_main_view_model.html#af296229ed941e8e7374daad2b48b6c20", null ],
    [ "Cakes", "class_my_cake_shop_1_1_wpf_app_1_1_v_m_1_1_main_view_model.html#a3394f17a79916a023e1874d6691a042a", null ],
    [ "CakeSelected", "class_my_cake_shop_1_1_wpf_app_1_1_v_m_1_1_main_view_model.html#a371fa5223209880e51712400de3932e9", null ],
    [ "DelCommand", "class_my_cake_shop_1_1_wpf_app_1_1_v_m_1_1_main_view_model.html#a39f67398d6d8f4a37b2b2a7a0a47985e", null ],
    [ "ModCommand", "class_my_cake_shop_1_1_wpf_app_1_1_v_m_1_1_main_view_model.html#abca210a82dbffae81841780cee30e147", null ]
];