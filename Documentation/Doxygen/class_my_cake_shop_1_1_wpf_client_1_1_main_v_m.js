var class_my_cake_shop_1_1_wpf_client_1_1_main_v_m =
[
    [ "MainVM", "class_my_cake_shop_1_1_wpf_client_1_1_main_v_m.html#a1394a7907e86a4389c07a0b94f2c27ae", null ],
    [ "MainVM", "class_my_cake_shop_1_1_wpf_client_1_1_main_v_m.html#a2bce6cabc628f7d5fe9584537d0d3cfa", null ],
    [ "AddCmd", "class_my_cake_shop_1_1_wpf_client_1_1_main_v_m.html#a85412b99ecb97797c48c947028fe9447", null ],
    [ "AddFunc", "class_my_cake_shop_1_1_wpf_client_1_1_main_v_m.html#af5c2f3dac5797076a90fce95d5bb3816", null ],
    [ "AllCakes", "class_my_cake_shop_1_1_wpf_client_1_1_main_v_m.html#acfeebcc9db5738aef5083c3a4677045c", null ],
    [ "DelCmd", "class_my_cake_shop_1_1_wpf_client_1_1_main_v_m.html#a9127b7051b6cdcac8d826cc4aa2e2f8c", null ],
    [ "EditorFunc", "class_my_cake_shop_1_1_wpf_client_1_1_main_v_m.html#a28681d690c9a1490d8b5bf527405ea2f", null ],
    [ "LoadCmd", "class_my_cake_shop_1_1_wpf_client_1_1_main_v_m.html#adfe28f851e64d0bd490909d4509841a3", null ],
    [ "ModCmd", "class_my_cake_shop_1_1_wpf_client_1_1_main_v_m.html#a9b7f8c8802356fd7a6f49a04a9b4128e", null ],
    [ "SelectedCake", "class_my_cake_shop_1_1_wpf_client_1_1_main_v_m.html#a224865148703a589d2ea6d4ec4bed165", null ]
];