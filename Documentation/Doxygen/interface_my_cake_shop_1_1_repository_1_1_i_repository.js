var interface_my_cake_shop_1_1_repository_1_1_i_repository =
[
    [ "GetAll", "interface_my_cake_shop_1_1_repository_1_1_i_repository.html#a58110b864a159c148b42623225365b21", null ],
    [ "GetOne", "interface_my_cake_shop_1_1_repository_1_1_i_repository.html#a568ff9d3622130d49b02e6b2c2120322", null ],
    [ "Insert", "interface_my_cake_shop_1_1_repository_1_1_i_repository.html#a59dedb2680cbf60e36089d378f5a9ea8", null ],
    [ "Remove", "interface_my_cake_shop_1_1_repository_1_1_i_repository.html#a6f7874cde177daaabccd0724af0dabf6", null ],
    [ "Remove", "interface_my_cake_shop_1_1_repository_1_1_i_repository.html#ae99485d597931afc77fc5ff7ee1f759d", null ]
];