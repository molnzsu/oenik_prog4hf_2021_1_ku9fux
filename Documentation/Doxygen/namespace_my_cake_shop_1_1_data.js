var namespace_my_cake_shop_1_1_data =
[
    [ "CakeShopDbContext", "class_my_cake_shop_1_1_data_1_1_cake_shop_db_context.html", "class_my_cake_shop_1_1_data_1_1_cake_shop_db_context" ],
    [ "Cake", "class_my_cake_shop_1_1_data_1_1_cake.html", "class_my_cake_shop_1_1_data_1_1_cake" ],
    [ "Confectioner", "class_my_cake_shop_1_1_data_1_1_confectioner.html", "class_my_cake_shop_1_1_data_1_1_confectioner" ],
    [ "Customer", "class_my_cake_shop_1_1_data_1_1_customer.html", "class_my_cake_shop_1_1_data_1_1_customer" ],
    [ "Order", "class_my_cake_shop_1_1_data_1_1_order.html", "class_my_cake_shop_1_1_data_1_1_order" ],
    [ "KeyAttribute", "namespace_my_cake_shop_1_1_data.html#a201917fc314d9eb4a0342a389b3425f3", null ]
];