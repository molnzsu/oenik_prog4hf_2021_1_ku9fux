var namespace_my_cake_shop =
[
    [ "Data", "namespace_my_cake_shop_1_1_data.html", "namespace_my_cake_shop_1_1_data" ],
    [ "Logic", "namespace_my_cake_shop_1_1_logic.html", "namespace_my_cake_shop_1_1_logic" ],
    [ "Program", "namespace_my_cake_shop_1_1_program.html", "namespace_my_cake_shop_1_1_program" ],
    [ "Repository", "namespace_my_cake_shop_1_1_repository.html", "namespace_my_cake_shop_1_1_repository" ],
    [ "Web", "namespace_my_cake_shop_1_1_web.html", "namespace_my_cake_shop_1_1_web" ],
    [ "WpfApp", "namespace_my_cake_shop_1_1_wpf_app.html", "namespace_my_cake_shop_1_1_wpf_app" ],
    [ "WpfClient", "namespace_my_cake_shop_1_1_wpf_client.html", "namespace_my_cake_shop_1_1_wpf_client" ]
];