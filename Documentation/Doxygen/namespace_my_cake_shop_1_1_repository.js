var namespace_my_cake_shop_1_1_repository =
[
    [ "CakeRepository", "class_my_cake_shop_1_1_repository_1_1_cake_repository.html", "class_my_cake_shop_1_1_repository_1_1_cake_repository" ],
    [ "ConfectionerRepository", "class_my_cake_shop_1_1_repository_1_1_confectioner_repository.html", "class_my_cake_shop_1_1_repository_1_1_confectioner_repository" ],
    [ "CustomerRepository", "class_my_cake_shop_1_1_repository_1_1_customer_repository.html", "class_my_cake_shop_1_1_repository_1_1_customer_repository" ],
    [ "OrderRepository", "class_my_cake_shop_1_1_repository_1_1_order_repository.html", "class_my_cake_shop_1_1_repository_1_1_order_repository" ],
    [ "ICakeRepository", "interface_my_cake_shop_1_1_repository_1_1_i_cake_repository.html", "interface_my_cake_shop_1_1_repository_1_1_i_cake_repository" ],
    [ "IConfectionerRepository", "interface_my_cake_shop_1_1_repository_1_1_i_confectioner_repository.html", "interface_my_cake_shop_1_1_repository_1_1_i_confectioner_repository" ],
    [ "ICustomerRepository", "interface_my_cake_shop_1_1_repository_1_1_i_customer_repository.html", "interface_my_cake_shop_1_1_repository_1_1_i_customer_repository" ],
    [ "IOrderRepository", "interface_my_cake_shop_1_1_repository_1_1_i_order_repository.html", "interface_my_cake_shop_1_1_repository_1_1_i_order_repository" ],
    [ "IRepository", "interface_my_cake_shop_1_1_repository_1_1_i_repository.html", "interface_my_cake_shop_1_1_repository_1_1_i_repository" ],
    [ "RepositoryC", "class_my_cake_shop_1_1_repository_1_1_repository_c.html", "class_my_cake_shop_1_1_repository_1_1_repository_c" ]
];