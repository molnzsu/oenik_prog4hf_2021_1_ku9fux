var namespace_my_cake_shop_1_1_wpf_client =
[
    [ "AddWindow", "class_my_cake_shop_1_1_wpf_client_1_1_add_window.html", "class_my_cake_shop_1_1_wpf_client_1_1_add_window" ],
    [ "App", "class_my_cake_shop_1_1_wpf_client_1_1_app.html", "class_my_cake_shop_1_1_wpf_client_1_1_app" ],
    [ "CakeVM", "class_my_cake_shop_1_1_wpf_client_1_1_cake_v_m.html", "class_my_cake_shop_1_1_wpf_client_1_1_cake_v_m" ],
    [ "EditorWindow", "class_my_cake_shop_1_1_wpf_client_1_1_editor_window.html", "class_my_cake_shop_1_1_wpf_client_1_1_editor_window" ],
    [ "IMainLogic", "interface_my_cake_shop_1_1_wpf_client_1_1_i_main_logic.html", "interface_my_cake_shop_1_1_wpf_client_1_1_i_main_logic" ],
    [ "MainLogic", "class_my_cake_shop_1_1_wpf_client_1_1_main_logic.html", "class_my_cake_shop_1_1_wpf_client_1_1_main_logic" ],
    [ "MainVM", "class_my_cake_shop_1_1_wpf_client_1_1_main_v_m.html", "class_my_cake_shop_1_1_wpf_client_1_1_main_v_m" ],
    [ "MainWindow", "class_my_cake_shop_1_1_wpf_client_1_1_main_window.html", "class_my_cake_shop_1_1_wpf_client_1_1_main_window" ],
    [ "MyIoc", "class_my_cake_shop_1_1_wpf_client_1_1_my_ioc.html", "class_my_cake_shop_1_1_wpf_client_1_1_my_ioc" ]
];