var class_my_cake_shop_1_1_repository_1_1_confectioner_repository =
[
    [ "ConfectionerRepository", "class_my_cake_shop_1_1_repository_1_1_confectioner_repository.html#a82a4a4456438267d6b2acbecf428d38f", null ],
    [ "ChangeEmail", "class_my_cake_shop_1_1_repository_1_1_confectioner_repository.html#a1b1811e8119df0c5de41be451f2f7c10", null ],
    [ "ChangePhoneNumber", "class_my_cake_shop_1_1_repository_1_1_confectioner_repository.html#adcc51811719a08c5568f603d22b033ef", null ],
    [ "GetOne", "class_my_cake_shop_1_1_repository_1_1_confectioner_repository.html#aad9d16b58c7cd0f91e8b5bafa6f36ef5", null ],
    [ "Insert", "class_my_cake_shop_1_1_repository_1_1_confectioner_repository.html#a87daef7381497678a463b5076c8757eb", null ],
    [ "Remove", "class_my_cake_shop_1_1_repository_1_1_confectioner_repository.html#a0831f72bdf9aa792bffa6b489393f095", null ],
    [ "Remove", "class_my_cake_shop_1_1_repository_1_1_confectioner_repository.html#aa86d039379bf40c8e8b911b9d018e8e6", null ]
];