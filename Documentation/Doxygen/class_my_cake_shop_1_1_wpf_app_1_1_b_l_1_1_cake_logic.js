var class_my_cake_shop_1_1_wpf_app_1_1_b_l_1_1_cake_logic =
[
    [ "CakeLogic", "class_my_cake_shop_1_1_wpf_app_1_1_b_l_1_1_cake_logic.html#a03f76752046a1be7b720232a189cbc30", null ],
    [ "AddCake", "class_my_cake_shop_1_1_wpf_app_1_1_b_l_1_1_cake_logic.html#ae6ad415f1d1c8e97a2cd358c57d5f4c7", null ],
    [ "CakeToCakeWpf", "class_my_cake_shop_1_1_wpf_app_1_1_b_l_1_1_cake_logic.html#a1a9cebef51c3b79ec2c96cdeb995467f", null ],
    [ "CakeWpfToCake", "class_my_cake_shop_1_1_wpf_app_1_1_b_l_1_1_cake_logic.html#a364c914880d0f85e06597d715916f194", null ],
    [ "DelCake", "class_my_cake_shop_1_1_wpf_app_1_1_b_l_1_1_cake_logic.html#a60c3a19d0ef3c1222d6542437bd2daec", null ],
    [ "GetAllCakes", "class_my_cake_shop_1_1_wpf_app_1_1_b_l_1_1_cake_logic.html#ad051ad1e115f4e5caee35f08902b1eec", null ],
    [ "ModCake", "class_my_cake_shop_1_1_wpf_app_1_1_b_l_1_1_cake_logic.html#a10200fd909ec86e09af29881f71863eb", null ]
];