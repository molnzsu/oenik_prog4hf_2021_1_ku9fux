var interface_my_cake_shop_1_1_logic_1_1_i_stock_logic =
[
    [ "ChangeEmail", "interface_my_cake_shop_1_1_logic_1_1_i_stock_logic.html#a634c1498f574aad8eeba623077d7aece", null ],
    [ "ChangeExpirationDate", "interface_my_cake_shop_1_1_logic_1_1_i_stock_logic.html#aa7d355cfd60d4111a4ac38726c34ad8e", null ],
    [ "ChangePhoneNumber", "interface_my_cake_shop_1_1_logic_1_1_i_stock_logic.html#a306367d88bc286a661389c27d3ff0fbc", null ],
    [ "ChangePopularity", "interface_my_cake_shop_1_1_logic_1_1_i_stock_logic.html#a5aabbea62064103eb785af3462c7e125", null ],
    [ "ChangePrice", "interface_my_cake_shop_1_1_logic_1_1_i_stock_logic.html#a8db562e26b3bc6d3c72f6ed7f8376366", null ],
    [ "GetAllCakes", "interface_my_cake_shop_1_1_logic_1_1_i_stock_logic.html#a7bb11405fa8a9b47a28624cf2330f141", null ],
    [ "GetAllConfectioner", "interface_my_cake_shop_1_1_logic_1_1_i_stock_logic.html#a4d2ec40365ba0c9500af427768daec58", null ],
    [ "GetAllOrder", "interface_my_cake_shop_1_1_logic_1_1_i_stock_logic.html#a10901e9f53fde8704dfb64a1bdc7b140", null ],
    [ "GetConfSumCake", "interface_my_cake_shop_1_1_logic_1_1_i_stock_logic.html#ad6938210ab10d01f2bc9ef77f3768171", null ],
    [ "GetOneCake", "interface_my_cake_shop_1_1_logic_1_1_i_stock_logic.html#ac8a3d572df337f7967f11e8859e068d7", null ],
    [ "GetOneConfectioner", "interface_my_cake_shop_1_1_logic_1_1_i_stock_logic.html#a5361b1611906ce2a96c8f35f3b933c77", null ],
    [ "GetOneOrder", "interface_my_cake_shop_1_1_logic_1_1_i_stock_logic.html#a826e530a2d2984c7cd40751b0cd3b710", null ],
    [ "GetTypeAverages", "interface_my_cake_shop_1_1_logic_1_1_i_stock_logic.html#a056b2a387e35ddbcf2d7d76e5bec0cdd", null ],
    [ "InsertCake", "interface_my_cake_shop_1_1_logic_1_1_i_stock_logic.html#a2ea568f09eb36853b59ef0061467bc83", null ],
    [ "InsertCake", "interface_my_cake_shop_1_1_logic_1_1_i_stock_logic.html#a35585debfc8c50e771231b3481ef26d1", null ],
    [ "InsertConfectioner", "interface_my_cake_shop_1_1_logic_1_1_i_stock_logic.html#a234d4c490ae25868069b04d842fdff07", null ],
    [ "RemoveCake", "interface_my_cake_shop_1_1_logic_1_1_i_stock_logic.html#a5cdf4197a173f8bd07859ef79bd991d6", null ],
    [ "RemoveCake", "interface_my_cake_shop_1_1_logic_1_1_i_stock_logic.html#a861d1a12ca6b9642f80cac3d0e4b0469", null ],
    [ "RemoveConfectioner", "interface_my_cake_shop_1_1_logic_1_1_i_stock_logic.html#a19f34032523197edc080f8d143a268d6", null ],
    [ "RemoveConfectioner", "interface_my_cake_shop_1_1_logic_1_1_i_stock_logic.html#aadf4a9f1c91ef00aefa9e8c98a82416b", null ]
];