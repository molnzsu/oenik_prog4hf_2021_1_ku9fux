var namespace_my_cake_shop_1_1_web_1_1_models =
[
    [ "Cake", "class_my_cake_shop_1_1_web_1_1_models_1_1_cake.html", "class_my_cake_shop_1_1_web_1_1_models_1_1_cake" ],
    [ "CakeListViewModel", "class_my_cake_shop_1_1_web_1_1_models_1_1_cake_list_view_model.html", "class_my_cake_shop_1_1_web_1_1_models_1_1_cake_list_view_model" ],
    [ "ErrorViewModel", "class_my_cake_shop_1_1_web_1_1_models_1_1_error_view_model.html", "class_my_cake_shop_1_1_web_1_1_models_1_1_error_view_model" ],
    [ "MapperFactory", "class_my_cake_shop_1_1_web_1_1_models_1_1_mapper_factory.html", "class_my_cake_shop_1_1_web_1_1_models_1_1_mapper_factory" ]
];