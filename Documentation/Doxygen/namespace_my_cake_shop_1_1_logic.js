var namespace_my_cake_shop_1_1_logic =
[
    [ "Tests", "namespace_my_cake_shop_1_1_logic_1_1_tests.html", "namespace_my_cake_shop_1_1_logic_1_1_tests" ],
    [ "AverageResult", "class_my_cake_shop_1_1_logic_1_1_average_result.html", "class_my_cake_shop_1_1_logic_1_1_average_result" ],
    [ "ConfSumCake", "class_my_cake_shop_1_1_logic_1_1_conf_sum_cake.html", "class_my_cake_shop_1_1_logic_1_1_conf_sum_cake" ],
    [ "CustomerOrders", "class_my_cake_shop_1_1_logic_1_1_customer_orders.html", "class_my_cake_shop_1_1_logic_1_1_customer_orders" ],
    [ "IOrderLogic", "interface_my_cake_shop_1_1_logic_1_1_i_order_logic.html", "interface_my_cake_shop_1_1_logic_1_1_i_order_logic" ],
    [ "IStockLogic", "interface_my_cake_shop_1_1_logic_1_1_i_stock_logic.html", "interface_my_cake_shop_1_1_logic_1_1_i_stock_logic" ],
    [ "OrderLogic", "class_my_cake_shop_1_1_logic_1_1_order_logic.html", "class_my_cake_shop_1_1_logic_1_1_order_logic" ],
    [ "StockLogic", "class_my_cake_shop_1_1_logic_1_1_stock_logic.html", "class_my_cake_shop_1_1_logic_1_1_stock_logic" ]
];