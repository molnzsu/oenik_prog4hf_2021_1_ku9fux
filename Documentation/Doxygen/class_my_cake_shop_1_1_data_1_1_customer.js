var class_my_cake_shop_1_1_data_1_1_customer =
[
    [ "Customer", "class_my_cake_shop_1_1_data_1_1_customer.html#a0d7472f309ab0ba0a2942e7fa1bf07ec", null ],
    [ "Customer", "class_my_cake_shop_1_1_data_1_1_customer.html#a04c81e6a4b5d2322ffbcdfde37f8bc2e", null ],
    [ "Equals", "class_my_cake_shop_1_1_data_1_1_customer.html#afe56aad8b44b3774544a77a94acf4114", null ],
    [ "GetHashCode", "class_my_cake_shop_1_1_data_1_1_customer.html#ab3f22a52b9376348a8c3309c9d352df6", null ],
    [ "ToString", "class_my_cake_shop_1_1_data_1_1_customer.html#a584e735c7df0f3a6ba6170b41e7c1d32", null ],
    [ "Address", "class_my_cake_shop_1_1_data_1_1_customer.html#a1a4eb9889819ddc2e5f2c34d03d22564", null ],
    [ "BirthDate", "class_my_cake_shop_1_1_data_1_1_customer.html#a6b599906840c779d476e7553e64e660e", null ],
    [ "CustomerID", "class_my_cake_shop_1_1_data_1_1_customer.html#a91ffd30ab47d6362f9b08ace849e3e6d", null ],
    [ "Email", "class_my_cake_shop_1_1_data_1_1_customer.html#afad8f88057a17dabaed19a0bfd66f7c0", null ],
    [ "FirstName", "class_my_cake_shop_1_1_data_1_1_customer.html#ac7d2bce8330fbe460f23bed001d67a34", null ],
    [ "LastName", "class_my_cake_shop_1_1_data_1_1_customer.html#a13ac98d78f26bfbce136677bd604fa85", null ],
    [ "Orders", "class_my_cake_shop_1_1_data_1_1_customer.html#ac4feedd128cfed05cf3a2c2d6db0df0c", null ],
    [ "PhoneNumber", "class_my_cake_shop_1_1_data_1_1_customer.html#a81c0278ce770ae505a17fac9b86032df", null ]
];