var namespace_my_cake_shop_1_1_web =
[
    [ "Controllers", "namespace_my_cake_shop_1_1_web_1_1_controllers.html", "namespace_my_cake_shop_1_1_web_1_1_controllers" ],
    [ "Models", "namespace_my_cake_shop_1_1_web_1_1_models.html", "namespace_my_cake_shop_1_1_web_1_1_models" ],
    [ "ApiResult", "class_my_cake_shop_1_1_web_1_1_api_result.html", "class_my_cake_shop_1_1_web_1_1_api_result" ],
    [ "Program", "class_my_cake_shop_1_1_web_1_1_program.html", "class_my_cake_shop_1_1_web_1_1_program" ],
    [ "Startup", "class_my_cake_shop_1_1_web_1_1_startup.html", "class_my_cake_shop_1_1_web_1_1_startup" ]
];