var class_my_cake_shop_1_1_repository_1_1_cake_repository =
[
    [ "CakeRepository", "class_my_cake_shop_1_1_repository_1_1_cake_repository.html#a8f0d37aaf294a39a7d9392bd59e53d03", null ],
    [ "ChangeExpirationDate", "class_my_cake_shop_1_1_repository_1_1_cake_repository.html#aaf9e86531e1e259840f0446a0695855a", null ],
    [ "ChangePopularity", "class_my_cake_shop_1_1_repository_1_1_cake_repository.html#acfa635aecc0ffb89f2159a2510a09400", null ],
    [ "ChangePrice", "class_my_cake_shop_1_1_repository_1_1_cake_repository.html#ac6a57c46431113b08ba1c907921f17ab", null ],
    [ "GetOne", "class_my_cake_shop_1_1_repository_1_1_cake_repository.html#a44462192c8b4f1ec8e8ec478dabd5f26", null ],
    [ "Insert", "class_my_cake_shop_1_1_repository_1_1_cake_repository.html#a56b920087236184a64b2f9f826e3219b", null ],
    [ "Remove", "class_my_cake_shop_1_1_repository_1_1_cake_repository.html#a357fba36b8edd34be7037ff75cb4e953", null ],
    [ "Remove", "class_my_cake_shop_1_1_repository_1_1_cake_repository.html#afd7cc1a0e7189a23f65f5bc77f2085b4", null ]
];