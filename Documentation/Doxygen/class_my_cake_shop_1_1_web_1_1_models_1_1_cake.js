var class_my_cake_shop_1_1_web_1_1_models_1_1_cake =
[
    [ "ExpirationDate", "class_my_cake_shop_1_1_web_1_1_models_1_1_cake.html#a7cc2774fe7986f11bdd6831fcc31f626", null ],
    [ "Id", "class_my_cake_shop_1_1_web_1_1_models_1_1_cake.html#a2f615eb55274ba8abe6d67e049fe6ed3", null ],
    [ "LactoseFree", "class_my_cake_shop_1_1_web_1_1_models_1_1_cake.html#a49ffa8b4e548cfb65c37978b6de62020", null ],
    [ "MadeDate", "class_my_cake_shop_1_1_web_1_1_models_1_1_cake.html#ac422007e2b466320183e36eb826af2c6", null ],
    [ "MakerId", "class_my_cake_shop_1_1_web_1_1_models_1_1_cake.html#a42d91e6f8bc6877b7c7991c503837119", null ],
    [ "Name", "class_my_cake_shop_1_1_web_1_1_models_1_1_cake.html#a62943b3731abf8413c55c96d37e248de", null ],
    [ "Popularity", "class_my_cake_shop_1_1_web_1_1_models_1_1_cake.html#adbcc9f2227c0b1877bb0afb9a0068da0", null ],
    [ "Price", "class_my_cake_shop_1_1_web_1_1_models_1_1_cake.html#ae336539e50e4571f336dbef3a7cbe071", null ],
    [ "Type", "class_my_cake_shop_1_1_web_1_1_models_1_1_cake.html#adc97a05e3312f0efaf82005839fbf567", null ]
];