var class_my_cake_shop_1_1_data_1_1_cake =
[
    [ "Cake", "class_my_cake_shop_1_1_data_1_1_cake.html#ae82aae50c6fc52cede46752eec922ff5", null ],
    [ "Cake", "class_my_cake_shop_1_1_data_1_1_cake.html#a26277bb9f4f19918449ebd773f4c6790", null ],
    [ "Equals", "class_my_cake_shop_1_1_data_1_1_cake.html#a4400abb20b2c7b5581e7b3cac299781d", null ],
    [ "GetHashCode", "class_my_cake_shop_1_1_data_1_1_cake.html#ae9312aba9d1e585a85cf741eb8e920ed", null ],
    [ "ToString", "class_my_cake_shop_1_1_data_1_1_cake.html#a5bbc12daa41d3dc1438ce8424d2cc616", null ],
    [ "CakeID", "class_my_cake_shop_1_1_data_1_1_cake.html#afe04f667f45f96f965809ca73642e1a1", null ],
    [ "ExpirationDate", "class_my_cake_shop_1_1_data_1_1_cake.html#a89161dc4cfda78137044a8f66935ef3c", null ],
    [ "LactoseFree", "class_my_cake_shop_1_1_data_1_1_cake.html#abc119aedd86a9c101452eb431c4caa85", null ],
    [ "MadeDate", "class_my_cake_shop_1_1_data_1_1_cake.html#aadae9811859998826f71ae80b8c91772", null ],
    [ "MakerID", "class_my_cake_shop_1_1_data_1_1_cake.html#ac0327af58b41d9325d58e287dc147d7e", null ],
    [ "Name", "class_my_cake_shop_1_1_data_1_1_cake.html#a63fef5d56dcf7ea97e0870f4f4735945", null ],
    [ "Orders", "class_my_cake_shop_1_1_data_1_1_cake.html#ab1f6a848128849b914c627419a16c2fd", null ],
    [ "Popularity", "class_my_cake_shop_1_1_data_1_1_cake.html#a1df9ad7129aec785ad378af06519f336", null ],
    [ "Price", "class_my_cake_shop_1_1_data_1_1_cake.html#a77ee0de43a1d4df152068fd9780a8439", null ],
    [ "Type", "class_my_cake_shop_1_1_data_1_1_cake.html#aac978a9c2fa31ddd3adf55a9734722a5", null ]
];