var class_my_cake_shop_1_1_data_1_1_cake_shop_db_context =
[
    [ "CakeShopDbContext", "class_my_cake_shop_1_1_data_1_1_cake_shop_db_context.html#a96d6002e0207b88feb50055d988fc10a", null ],
    [ "OnConfiguring", "class_my_cake_shop_1_1_data_1_1_cake_shop_db_context.html#a9cb45b574d7e64599c28aa22cb07460e", null ],
    [ "OnModelCreating", "class_my_cake_shop_1_1_data_1_1_cake_shop_db_context.html#a081f5c435fa38575fc082773d33fc84f", null ],
    [ "Cakes", "class_my_cake_shop_1_1_data_1_1_cake_shop_db_context.html#aa9c104fd3ebb99c448dcb3f8c63c4e4b", null ],
    [ "Confectioners", "class_my_cake_shop_1_1_data_1_1_cake_shop_db_context.html#a506b2acf716da5650d1d3dfd63ff82fa", null ],
    [ "Customers", "class_my_cake_shop_1_1_data_1_1_cake_shop_db_context.html#a99f6db6a2899d2352aa926f2193b0fb8", null ],
    [ "Orders", "class_my_cake_shop_1_1_data_1_1_cake_shop_db_context.html#a3e2cb53496fccdb1793ee1ae28cc7596", null ]
];