var class_my_cake_shop_1_1_web_1_1_controllers_1_1_cakes_api_controller =
[
    [ "CakesApiController", "class_my_cake_shop_1_1_web_1_1_controllers_1_1_cakes_api_controller.html#a5ff86b1bb33003e7ace2b84c736f377c", null ],
    [ "AddOneCake", "class_my_cake_shop_1_1_web_1_1_controllers_1_1_cakes_api_controller.html#ab158076e46ca4ee9fd67041eac8747a6", null ],
    [ "DelOneCakes", "class_my_cake_shop_1_1_web_1_1_controllers_1_1_cakes_api_controller.html#abce64d2c5bdc249d1a83f19979cd867e", null ],
    [ "GetAll", "class_my_cake_shop_1_1_web_1_1_controllers_1_1_cakes_api_controller.html#a0ee6766d2c27dfaa3d4f4df57e7951c7", null ],
    [ "ModOneCake", "class_my_cake_shop_1_1_web_1_1_controllers_1_1_cakes_api_controller.html#ae085330179ee8e9b06007769b7da6c5c", null ]
];