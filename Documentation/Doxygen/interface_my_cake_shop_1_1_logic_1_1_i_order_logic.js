var interface_my_cake_shop_1_1_logic_1_1_i_order_logic =
[
    [ "ChangeAddress", "interface_my_cake_shop_1_1_logic_1_1_i_order_logic.html#a742ce9e1c36ae07d9f2aaa0969ab54c4", null ],
    [ "ChangeEmail", "interface_my_cake_shop_1_1_logic_1_1_i_order_logic.html#a09f515c752a0bec1fd1d7c1681212e3f", null ],
    [ "ChangeOrderStatus", "interface_my_cake_shop_1_1_logic_1_1_i_order_logic.html#a8a515354d1311ca63f26e83030c82be0", null ],
    [ "ChangePhoneNumber", "interface_my_cake_shop_1_1_logic_1_1_i_order_logic.html#a64a38815f1cd658d62565cfcb2d466c8", null ],
    [ "GetAllCakes", "interface_my_cake_shop_1_1_logic_1_1_i_order_logic.html#a66282fa7cf398892721865f0ae0374c4", null ],
    [ "GetAllCustomers", "interface_my_cake_shop_1_1_logic_1_1_i_order_logic.html#ad8a1484a6190597af0d141c623475446", null ],
    [ "GetAllOrder", "interface_my_cake_shop_1_1_logic_1_1_i_order_logic.html#a0064d285a764c59a77a97cdd54dea5b1", null ],
    [ "GetCustomerOrders", "interface_my_cake_shop_1_1_logic_1_1_i_order_logic.html#ac8dfc56681a0d004c3dac803cdeb5e2b", null ],
    [ "GetOneCake", "interface_my_cake_shop_1_1_logic_1_1_i_order_logic.html#a785e932c8b3a61902e0d832a581d0065", null ],
    [ "GetOneCustomer", "interface_my_cake_shop_1_1_logic_1_1_i_order_logic.html#a893cb28803c2b4cdbc3574af8ef3954c", null ],
    [ "GetOneOrder", "interface_my_cake_shop_1_1_logic_1_1_i_order_logic.html#a50196e2c55744d8932f259453ffb287e", null ],
    [ "InsertCustomer", "interface_my_cake_shop_1_1_logic_1_1_i_order_logic.html#a53e0bdf0a0a3634e204c447b99d28139", null ],
    [ "InsertOrder", "interface_my_cake_shop_1_1_logic_1_1_i_order_logic.html#a2f3c1bb6b69041afad35896ea3c95478", null ],
    [ "RemoveCustomer", "interface_my_cake_shop_1_1_logic_1_1_i_order_logic.html#a5c18a30135f3db8052bb59fb31a236d8", null ],
    [ "RemoveCustomer", "interface_my_cake_shop_1_1_logic_1_1_i_order_logic.html#a06dcdecb464494eb4ec81aa4ff1a55e7", null ],
    [ "RemoveOrder", "interface_my_cake_shop_1_1_logic_1_1_i_order_logic.html#a2edec6346d34f2fbf5c214dca39a4248", null ],
    [ "RemoveOrder", "interface_my_cake_shop_1_1_logic_1_1_i_order_logic.html#a8986f420d6d9ed5e16e5dca2eeed00d7", null ]
];