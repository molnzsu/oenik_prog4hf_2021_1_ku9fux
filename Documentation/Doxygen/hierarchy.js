var hierarchy =
[
    [ "MyCakeShop.Web.ApiResult", "class_my_cake_shop_1_1_web_1_1_api_result.html", null ],
    [ "Application", null, [
      [ "MyCakeShop.WpfApp.App", "class_my_cake_shop_1_1_wpf_app_1_1_app.html", null ]
    ] ],
    [ "System.Windows.Application", null, [
      [ "MyCakeShop.WpfApp.App", "class_my_cake_shop_1_1_wpf_app_1_1_app.html", null ],
      [ "MyCakeShop.WpfApp.App", "class_my_cake_shop_1_1_wpf_app_1_1_app.html", null ],
      [ "MyCakeShop.WpfClient.App", "class_my_cake_shop_1_1_wpf_client_1_1_app.html", null ],
      [ "MyCakeShop.WpfClient.App", "class_my_cake_shop_1_1_wpf_client_1_1_app.html", null ],
      [ "MyCakeShop.WpfClient.App", "class_my_cake_shop_1_1_wpf_client_1_1_app.html", null ]
    ] ],
    [ "MyCakeShop.Logic.AverageResult", "class_my_cake_shop_1_1_logic_1_1_average_result.html", null ],
    [ "MyCakeShop.Data.Cake", "class_my_cake_shop_1_1_data_1_1_cake.html", null ],
    [ "MyCakeShop.Web.Models.Cake", "class_my_cake_shop_1_1_web_1_1_models_1_1_cake.html", null ],
    [ "MyCakeShop.Web.Models.CakeListViewModel", "class_my_cake_shop_1_1_web_1_1_models_1_1_cake_list_view_model.html", null ],
    [ "MyCakeShop.Data.Confectioner", "class_my_cake_shop_1_1_data_1_1_confectioner.html", null ],
    [ "MyCakeShop.Logic.ConfSumCake", "class_my_cake_shop_1_1_logic_1_1_conf_sum_cake.html", null ],
    [ "Controller", null, [
      [ "MyCakeShop.Web.Controllers.CakesApiController", "class_my_cake_shop_1_1_web_1_1_controllers_1_1_cakes_api_controller.html", null ],
      [ "MyCakeShop.Web.Controllers.CakesController", "class_my_cake_shop_1_1_web_1_1_controllers_1_1_cakes_controller.html", null ],
      [ "MyCakeShop.Web.Controllers.HomeController", "class_my_cake_shop_1_1_web_1_1_controllers_1_1_home_controller.html", null ]
    ] ],
    [ "MyCakeShop.Data.Customer", "class_my_cake_shop_1_1_data_1_1_customer.html", null ],
    [ "MyCakeShop.Logic.CustomerOrders", "class_my_cake_shop_1_1_logic_1_1_customer_orders.html", null ],
    [ "DbContext", null, [
      [ "MyCakeShop.Data.CakeShopDbContext", "class_my_cake_shop_1_1_data_1_1_cake_shop_db_context.html", null ]
    ] ],
    [ "MyCakeShop.Web.Models.ErrorViewModel", "class_my_cake_shop_1_1_web_1_1_models_1_1_error_view_model.html", null ],
    [ "MyCakeShop.WpfApp.BL.ICakeLogic", "interface_my_cake_shop_1_1_wpf_app_1_1_b_l_1_1_i_cake_logic.html", [
      [ "MyCakeShop.WpfApp.BL.CakeLogic", "class_my_cake_shop_1_1_wpf_app_1_1_b_l_1_1_cake_logic.html", null ]
    ] ],
    [ "System.Windows.Markup.IComponentConnector", null, [
      [ "MyCakeShop.WpfApp.MainWindow", "class_my_cake_shop_1_1_wpf_app_1_1_main_window.html", null ],
      [ "MyCakeShop.WpfApp.UI.EditorWindow", "class_my_cake_shop_1_1_wpf_app_1_1_u_i_1_1_editor_window.html", null ],
      [ "MyCakeShop.WpfApp.UI.EditorWindow", "class_my_cake_shop_1_1_wpf_app_1_1_u_i_1_1_editor_window.html", null ],
      [ "MyCakeShop.WpfClient.AddWindow", "class_my_cake_shop_1_1_wpf_client_1_1_add_window.html", null ],
      [ "MyCakeShop.WpfClient.AddWindow", "class_my_cake_shop_1_1_wpf_client_1_1_add_window.html", null ],
      [ "MyCakeShop.WpfClient.EditorWindow", "class_my_cake_shop_1_1_wpf_client_1_1_editor_window.html", null ],
      [ "MyCakeShop.WpfClient.EditorWindow", "class_my_cake_shop_1_1_wpf_client_1_1_editor_window.html", null ],
      [ "MyCakeShop.WpfClient.MainWindow", "class_my_cake_shop_1_1_wpf_client_1_1_main_window.html", null ],
      [ "MyCakeShop.WpfClient.MainWindow", "class_my_cake_shop_1_1_wpf_client_1_1_main_window.html", null ]
    ] ],
    [ "MyCakeShop.WpfApp.BL.IEditorService", "interface_my_cake_shop_1_1_wpf_app_1_1_b_l_1_1_i_editor_service.html", [
      [ "MyCakeShop.WpfApp.UI.EditorServiceViaWindow", "class_my_cake_shop_1_1_wpf_app_1_1_u_i_1_1_editor_service_via_window.html", null ]
    ] ],
    [ "MyCakeShop.WpfClient.IMainLogic", "interface_my_cake_shop_1_1_wpf_client_1_1_i_main_logic.html", [
      [ "MyCakeShop.WpfClient.MainLogic", "class_my_cake_shop_1_1_wpf_client_1_1_main_logic.html", null ]
    ] ],
    [ "System.Windows.Markup.InternalTypeHelper", null, [
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ],
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ],
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ]
    ] ],
    [ "MyCakeShop.Logic.IOrderLogic", "interface_my_cake_shop_1_1_logic_1_1_i_order_logic.html", [
      [ "MyCakeShop.Logic.OrderLogic", "class_my_cake_shop_1_1_logic_1_1_order_logic.html", null ]
    ] ],
    [ "MyCakeShop.Repository.IRepository< T >", "interface_my_cake_shop_1_1_repository_1_1_i_repository.html", [
      [ "MyCakeShop.Repository.RepositoryC< T >", "class_my_cake_shop_1_1_repository_1_1_repository_c.html", null ]
    ] ],
    [ "MyCakeShop.Repository.IRepository< Cake >", "interface_my_cake_shop_1_1_repository_1_1_i_repository.html", [
      [ "MyCakeShop.Repository.ICakeRepository", "interface_my_cake_shop_1_1_repository_1_1_i_cake_repository.html", [
        [ "MyCakeShop.Repository.CakeRepository", "class_my_cake_shop_1_1_repository_1_1_cake_repository.html", null ]
      ] ]
    ] ],
    [ "MyCakeShop.Repository.IRepository< Confectioner >", "interface_my_cake_shop_1_1_repository_1_1_i_repository.html", [
      [ "MyCakeShop.Repository.IConfectionerRepository", "interface_my_cake_shop_1_1_repository_1_1_i_confectioner_repository.html", [
        [ "MyCakeShop.Repository.ConfectionerRepository", "class_my_cake_shop_1_1_repository_1_1_confectioner_repository.html", null ]
      ] ]
    ] ],
    [ "MyCakeShop.Repository.IRepository< Customer >", "interface_my_cake_shop_1_1_repository_1_1_i_repository.html", [
      [ "MyCakeShop.Repository.ICustomerRepository", "interface_my_cake_shop_1_1_repository_1_1_i_customer_repository.html", [
        [ "MyCakeShop.Repository.CustomerRepository", "class_my_cake_shop_1_1_repository_1_1_customer_repository.html", null ]
      ] ]
    ] ],
    [ "MyCakeShop.Repository.IRepository< Order >", "interface_my_cake_shop_1_1_repository_1_1_i_repository.html", [
      [ "MyCakeShop.Repository.IOrderRepository", "interface_my_cake_shop_1_1_repository_1_1_i_order_repository.html", [
        [ "MyCakeShop.Repository.OrderRepository", "class_my_cake_shop_1_1_repository_1_1_order_repository.html", null ]
      ] ]
    ] ],
    [ "IServiceLocator", null, [
      [ "MyCakeShop.WpfApp.MyIoc", "class_my_cake_shop_1_1_wpf_app_1_1_my_ioc.html", null ],
      [ "MyCakeShop.WpfClient.MyIoc", "class_my_cake_shop_1_1_wpf_client_1_1_my_ioc.html", null ]
    ] ],
    [ "MyCakeShop.Logic.IStockLogic", "interface_my_cake_shop_1_1_logic_1_1_i_stock_logic.html", [
      [ "MyCakeShop.Logic.StockLogic", "class_my_cake_shop_1_1_logic_1_1_stock_logic.html", null ]
    ] ],
    [ "IValueConverter", null, [
      [ "MyCakeShop.WpfApp.UI.TypeToBrushConverter", "class_my_cake_shop_1_1_wpf_app_1_1_u_i_1_1_type_to_brush_converter.html", null ]
    ] ],
    [ "MyCakeShop.Web.Models.MapperFactory", "class_my_cake_shop_1_1_web_1_1_models_1_1_mapper_factory.html", null ],
    [ "ObservableObject", null, [
      [ "ComponetWpfApp.Data.CakeWpf", "class_componet_wpf_app_1_1_data_1_1_cake_wpf.html", null ],
      [ "MyCakeShop.WpfClient.CakeVM", "class_my_cake_shop_1_1_wpf_client_1_1_cake_v_m.html", null ]
    ] ],
    [ "MyCakeShop.Data.Order", "class_my_cake_shop_1_1_data_1_1_order.html", null ],
    [ "MyCakeShop.Logic.Tests.OrderLogicTest", "class_my_cake_shop_1_1_logic_1_1_tests_1_1_order_logic_test.html", null ],
    [ "MyCakeShop.Web.Program", "class_my_cake_shop_1_1_web_1_1_program.html", null ],
    [ "MyCakeShop.Program.ProgramC", "class_my_cake_shop_1_1_program_1_1_program_c.html", null ],
    [ "global.MicrosoftAspNetCore.Mvc.Razor.RazorPage", null, [
      [ "AspNetCore.Views_Cakes_CakesDetails", "class_asp_net_core_1_1_views___cakes___cakes_details.html", null ],
      [ "AspNetCore.Views_Cakes_CakesEdit", "class_asp_net_core_1_1_views___cakes___cakes_edit.html", null ],
      [ "AspNetCore.Views_Cakes_CakesEdit2", "class_asp_net_core_1_1_views___cakes___cakes_edit2.html", null ],
      [ "AspNetCore.Views_Cakes_CakesIndex", "class_asp_net_core_1_1_views___cakes___cakes_index.html", null ],
      [ "AspNetCore.Views_Home_Index", "class_asp_net_core_1_1_views___home___index.html", null ],
      [ "AspNetCore.Views_Home_Privacy", "class_asp_net_core_1_1_views___home___privacy.html", null ],
      [ "AspNetCore.Views_Shared_Error", "class_asp_net_core_1_1_views___shared___error.html", null ],
      [ "AspNetCore.Views_Shared__Layout", "class_asp_net_core_1_1_views___shared_____layout.html", null ],
      [ "AspNetCore.Views_Shared__ValidationScriptsPartial", "class_asp_net_core_1_1_views___shared_____validation_scripts_partial.html", null ],
      [ "AspNetCore.Views__ViewImports", "class_asp_net_core_1_1_views_____view_imports.html", null ],
      [ "AspNetCore.Views__ViewStart", "class_asp_net_core_1_1_views_____view_start.html", null ]
    ] ],
    [ "global.MicrosoftAspNetCore.Mvc.Razor.RazorPage< IEnumerable< MyCakeShop.Web.Models.Cake >>", null, [
      [ "AspNetCore.Views_Cakes_CakesList", "class_asp_net_core_1_1_views___cakes___cakes_list.html", null ]
    ] ],
    [ "MyCakeShop.Repository.RepositoryC< Cake >", "class_my_cake_shop_1_1_repository_1_1_repository_c.html", [
      [ "MyCakeShop.Repository.CakeRepository", "class_my_cake_shop_1_1_repository_1_1_cake_repository.html", null ]
    ] ],
    [ "MyCakeShop.Repository.RepositoryC< Confectioner >", "class_my_cake_shop_1_1_repository_1_1_repository_c.html", [
      [ "MyCakeShop.Repository.ConfectionerRepository", "class_my_cake_shop_1_1_repository_1_1_confectioner_repository.html", null ]
    ] ],
    [ "MyCakeShop.Repository.RepositoryC< Customer >", "class_my_cake_shop_1_1_repository_1_1_repository_c.html", [
      [ "MyCakeShop.Repository.CustomerRepository", "class_my_cake_shop_1_1_repository_1_1_customer_repository.html", null ]
    ] ],
    [ "MyCakeShop.Repository.RepositoryC< Order >", "class_my_cake_shop_1_1_repository_1_1_repository_c.html", [
      [ "MyCakeShop.Repository.OrderRepository", "class_my_cake_shop_1_1_repository_1_1_order_repository.html", null ]
    ] ],
    [ "SimpleIoc", null, [
      [ "MyCakeShop.WpfApp.MyIoc", "class_my_cake_shop_1_1_wpf_app_1_1_my_ioc.html", null ],
      [ "MyCakeShop.WpfClient.MyIoc", "class_my_cake_shop_1_1_wpf_client_1_1_my_ioc.html", null ]
    ] ],
    [ "MyCakeShop.Web.Startup", "class_my_cake_shop_1_1_web_1_1_startup.html", null ],
    [ "MyCakeShop.Logic.Tests.StockLogicTest", "class_my_cake_shop_1_1_logic_1_1_tests_1_1_stock_logic_test.html", null ],
    [ "ViewModelBase", null, [
      [ "MyCakeShop.WpfApp.VM.EditorViewModel", "class_my_cake_shop_1_1_wpf_app_1_1_v_m_1_1_editor_view_model.html", null ],
      [ "MyCakeShop.WpfApp.VM.MainViewModel", "class_my_cake_shop_1_1_wpf_app_1_1_v_m_1_1_main_view_model.html", null ],
      [ "MyCakeShop.WpfClient.MainVM", "class_my_cake_shop_1_1_wpf_client_1_1_main_v_m.html", null ]
    ] ],
    [ "System.Windows.Window", null, [
      [ "MyCakeShop.WpfApp.MainWindow", "class_my_cake_shop_1_1_wpf_app_1_1_main_window.html", null ],
      [ "MyCakeShop.WpfApp.UI.EditorWindow", "class_my_cake_shop_1_1_wpf_app_1_1_u_i_1_1_editor_window.html", null ],
      [ "MyCakeShop.WpfApp.UI.EditorWindow", "class_my_cake_shop_1_1_wpf_app_1_1_u_i_1_1_editor_window.html", null ],
      [ "MyCakeShop.WpfApp.UI.EditorWindow", "class_my_cake_shop_1_1_wpf_app_1_1_u_i_1_1_editor_window.html", null ],
      [ "MyCakeShop.WpfClient.AddWindow", "class_my_cake_shop_1_1_wpf_client_1_1_add_window.html", null ],
      [ "MyCakeShop.WpfClient.AddWindow", "class_my_cake_shop_1_1_wpf_client_1_1_add_window.html", null ],
      [ "MyCakeShop.WpfClient.AddWindow", "class_my_cake_shop_1_1_wpf_client_1_1_add_window.html", null ],
      [ "MyCakeShop.WpfClient.EditorWindow", "class_my_cake_shop_1_1_wpf_client_1_1_editor_window.html", null ],
      [ "MyCakeShop.WpfClient.EditorWindow", "class_my_cake_shop_1_1_wpf_client_1_1_editor_window.html", null ],
      [ "MyCakeShop.WpfClient.EditorWindow", "class_my_cake_shop_1_1_wpf_client_1_1_editor_window.html", null ],
      [ "MyCakeShop.WpfClient.MainWindow", "class_my_cake_shop_1_1_wpf_client_1_1_main_window.html", null ],
      [ "MyCakeShop.WpfClient.MainWindow", "class_my_cake_shop_1_1_wpf_client_1_1_main_window.html", null ],
      [ "MyCakeShop.WpfClient.MainWindow", "class_my_cake_shop_1_1_wpf_client_1_1_main_window.html", null ]
    ] ]
];