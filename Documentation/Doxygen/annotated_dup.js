var annotated_dup =
[
    [ "AspNetCore", "namespace_asp_net_core.html", [
      [ "Views__ViewImports", "class_asp_net_core_1_1_views_____view_imports.html", "class_asp_net_core_1_1_views_____view_imports" ],
      [ "Views__ViewStart", "class_asp_net_core_1_1_views_____view_start.html", "class_asp_net_core_1_1_views_____view_start" ],
      [ "Views_Cakes_CakesDetails", "class_asp_net_core_1_1_views___cakes___cakes_details.html", "class_asp_net_core_1_1_views___cakes___cakes_details" ],
      [ "Views_Cakes_CakesEdit", "class_asp_net_core_1_1_views___cakes___cakes_edit.html", "class_asp_net_core_1_1_views___cakes___cakes_edit" ],
      [ "Views_Cakes_CakesEdit2", "class_asp_net_core_1_1_views___cakes___cakes_edit2.html", "class_asp_net_core_1_1_views___cakes___cakes_edit2" ],
      [ "Views_Cakes_CakesIndex", "class_asp_net_core_1_1_views___cakes___cakes_index.html", "class_asp_net_core_1_1_views___cakes___cakes_index" ],
      [ "Views_Cakes_CakesList", "class_asp_net_core_1_1_views___cakes___cakes_list.html", "class_asp_net_core_1_1_views___cakes___cakes_list" ],
      [ "Views_Home_Index", "class_asp_net_core_1_1_views___home___index.html", "class_asp_net_core_1_1_views___home___index" ],
      [ "Views_Home_Privacy", "class_asp_net_core_1_1_views___home___privacy.html", "class_asp_net_core_1_1_views___home___privacy" ],
      [ "Views_Shared__Layout", "class_asp_net_core_1_1_views___shared_____layout.html", "class_asp_net_core_1_1_views___shared_____layout" ],
      [ "Views_Shared__ValidationScriptsPartial", "class_asp_net_core_1_1_views___shared_____validation_scripts_partial.html", "class_asp_net_core_1_1_views___shared_____validation_scripts_partial" ],
      [ "Views_Shared_Error", "class_asp_net_core_1_1_views___shared___error.html", "class_asp_net_core_1_1_views___shared___error" ]
    ] ],
    [ "ComponetWpfApp", "namespace_componet_wpf_app.html", [
      [ "Data", "namespace_componet_wpf_app_1_1_data.html", [
        [ "CakeWpf", "class_componet_wpf_app_1_1_data_1_1_cake_wpf.html", "class_componet_wpf_app_1_1_data_1_1_cake_wpf" ]
      ] ]
    ] ],
    [ "MyCakeShop", "namespace_my_cake_shop.html", [
      [ "Data", "namespace_my_cake_shop_1_1_data.html", [
        [ "CakeShopDbContext", "class_my_cake_shop_1_1_data_1_1_cake_shop_db_context.html", "class_my_cake_shop_1_1_data_1_1_cake_shop_db_context" ],
        [ "Cake", "class_my_cake_shop_1_1_data_1_1_cake.html", "class_my_cake_shop_1_1_data_1_1_cake" ],
        [ "Confectioner", "class_my_cake_shop_1_1_data_1_1_confectioner.html", "class_my_cake_shop_1_1_data_1_1_confectioner" ],
        [ "Customer", "class_my_cake_shop_1_1_data_1_1_customer.html", "class_my_cake_shop_1_1_data_1_1_customer" ],
        [ "Order", "class_my_cake_shop_1_1_data_1_1_order.html", "class_my_cake_shop_1_1_data_1_1_order" ]
      ] ],
      [ "Logic", "namespace_my_cake_shop_1_1_logic.html", [
        [ "Tests", "namespace_my_cake_shop_1_1_logic_1_1_tests.html", [
          [ "OrderLogicTest", "class_my_cake_shop_1_1_logic_1_1_tests_1_1_order_logic_test.html", "class_my_cake_shop_1_1_logic_1_1_tests_1_1_order_logic_test" ],
          [ "StockLogicTest", "class_my_cake_shop_1_1_logic_1_1_tests_1_1_stock_logic_test.html", "class_my_cake_shop_1_1_logic_1_1_tests_1_1_stock_logic_test" ]
        ] ],
        [ "AverageResult", "class_my_cake_shop_1_1_logic_1_1_average_result.html", "class_my_cake_shop_1_1_logic_1_1_average_result" ],
        [ "ConfSumCake", "class_my_cake_shop_1_1_logic_1_1_conf_sum_cake.html", "class_my_cake_shop_1_1_logic_1_1_conf_sum_cake" ],
        [ "CustomerOrders", "class_my_cake_shop_1_1_logic_1_1_customer_orders.html", "class_my_cake_shop_1_1_logic_1_1_customer_orders" ],
        [ "IOrderLogic", "interface_my_cake_shop_1_1_logic_1_1_i_order_logic.html", "interface_my_cake_shop_1_1_logic_1_1_i_order_logic" ],
        [ "IStockLogic", "interface_my_cake_shop_1_1_logic_1_1_i_stock_logic.html", "interface_my_cake_shop_1_1_logic_1_1_i_stock_logic" ],
        [ "OrderLogic", "class_my_cake_shop_1_1_logic_1_1_order_logic.html", "class_my_cake_shop_1_1_logic_1_1_order_logic" ],
        [ "StockLogic", "class_my_cake_shop_1_1_logic_1_1_stock_logic.html", "class_my_cake_shop_1_1_logic_1_1_stock_logic" ]
      ] ],
      [ "Program", "namespace_my_cake_shop_1_1_program.html", [
        [ "ProgramC", "class_my_cake_shop_1_1_program_1_1_program_c.html", "class_my_cake_shop_1_1_program_1_1_program_c" ]
      ] ],
      [ "Repository", "namespace_my_cake_shop_1_1_repository.html", [
        [ "CakeRepository", "class_my_cake_shop_1_1_repository_1_1_cake_repository.html", "class_my_cake_shop_1_1_repository_1_1_cake_repository" ],
        [ "ConfectionerRepository", "class_my_cake_shop_1_1_repository_1_1_confectioner_repository.html", "class_my_cake_shop_1_1_repository_1_1_confectioner_repository" ],
        [ "CustomerRepository", "class_my_cake_shop_1_1_repository_1_1_customer_repository.html", "class_my_cake_shop_1_1_repository_1_1_customer_repository" ],
        [ "OrderRepository", "class_my_cake_shop_1_1_repository_1_1_order_repository.html", "class_my_cake_shop_1_1_repository_1_1_order_repository" ],
        [ "ICakeRepository", "interface_my_cake_shop_1_1_repository_1_1_i_cake_repository.html", "interface_my_cake_shop_1_1_repository_1_1_i_cake_repository" ],
        [ "IConfectionerRepository", "interface_my_cake_shop_1_1_repository_1_1_i_confectioner_repository.html", "interface_my_cake_shop_1_1_repository_1_1_i_confectioner_repository" ],
        [ "ICustomerRepository", "interface_my_cake_shop_1_1_repository_1_1_i_customer_repository.html", "interface_my_cake_shop_1_1_repository_1_1_i_customer_repository" ],
        [ "IOrderRepository", "interface_my_cake_shop_1_1_repository_1_1_i_order_repository.html", "interface_my_cake_shop_1_1_repository_1_1_i_order_repository" ],
        [ "IRepository", "interface_my_cake_shop_1_1_repository_1_1_i_repository.html", "interface_my_cake_shop_1_1_repository_1_1_i_repository" ],
        [ "RepositoryC", "class_my_cake_shop_1_1_repository_1_1_repository_c.html", "class_my_cake_shop_1_1_repository_1_1_repository_c" ]
      ] ],
      [ "Web", "namespace_my_cake_shop_1_1_web.html", [
        [ "Controllers", "namespace_my_cake_shop_1_1_web_1_1_controllers.html", [
          [ "CakesApiController", "class_my_cake_shop_1_1_web_1_1_controllers_1_1_cakes_api_controller.html", "class_my_cake_shop_1_1_web_1_1_controllers_1_1_cakes_api_controller" ],
          [ "CakesController", "class_my_cake_shop_1_1_web_1_1_controllers_1_1_cakes_controller.html", "class_my_cake_shop_1_1_web_1_1_controllers_1_1_cakes_controller" ],
          [ "HomeController", "class_my_cake_shop_1_1_web_1_1_controllers_1_1_home_controller.html", "class_my_cake_shop_1_1_web_1_1_controllers_1_1_home_controller" ]
        ] ],
        [ "Models", "namespace_my_cake_shop_1_1_web_1_1_models.html", [
          [ "Cake", "class_my_cake_shop_1_1_web_1_1_models_1_1_cake.html", "class_my_cake_shop_1_1_web_1_1_models_1_1_cake" ],
          [ "CakeListViewModel", "class_my_cake_shop_1_1_web_1_1_models_1_1_cake_list_view_model.html", "class_my_cake_shop_1_1_web_1_1_models_1_1_cake_list_view_model" ],
          [ "ErrorViewModel", "class_my_cake_shop_1_1_web_1_1_models_1_1_error_view_model.html", "class_my_cake_shop_1_1_web_1_1_models_1_1_error_view_model" ],
          [ "MapperFactory", "class_my_cake_shop_1_1_web_1_1_models_1_1_mapper_factory.html", "class_my_cake_shop_1_1_web_1_1_models_1_1_mapper_factory" ]
        ] ],
        [ "ApiResult", "class_my_cake_shop_1_1_web_1_1_api_result.html", "class_my_cake_shop_1_1_web_1_1_api_result" ],
        [ "Program", "class_my_cake_shop_1_1_web_1_1_program.html", "class_my_cake_shop_1_1_web_1_1_program" ],
        [ "Startup", "class_my_cake_shop_1_1_web_1_1_startup.html", "class_my_cake_shop_1_1_web_1_1_startup" ]
      ] ],
      [ "WpfApp", "namespace_my_cake_shop_1_1_wpf_app.html", [
        [ "BL", "namespace_my_cake_shop_1_1_wpf_app_1_1_b_l.html", [
          [ "CakeLogic", "class_my_cake_shop_1_1_wpf_app_1_1_b_l_1_1_cake_logic.html", "class_my_cake_shop_1_1_wpf_app_1_1_b_l_1_1_cake_logic" ],
          [ "ICakeLogic", "interface_my_cake_shop_1_1_wpf_app_1_1_b_l_1_1_i_cake_logic.html", "interface_my_cake_shop_1_1_wpf_app_1_1_b_l_1_1_i_cake_logic" ],
          [ "IEditorService", "interface_my_cake_shop_1_1_wpf_app_1_1_b_l_1_1_i_editor_service.html", "interface_my_cake_shop_1_1_wpf_app_1_1_b_l_1_1_i_editor_service" ]
        ] ],
        [ "UI", "namespace_my_cake_shop_1_1_wpf_app_1_1_u_i.html", [
          [ "EditorWindow", "class_my_cake_shop_1_1_wpf_app_1_1_u_i_1_1_editor_window.html", "class_my_cake_shop_1_1_wpf_app_1_1_u_i_1_1_editor_window" ],
          [ "EditorServiceViaWindow", "class_my_cake_shop_1_1_wpf_app_1_1_u_i_1_1_editor_service_via_window.html", "class_my_cake_shop_1_1_wpf_app_1_1_u_i_1_1_editor_service_via_window" ],
          [ "TypeToBrushConverter", "class_my_cake_shop_1_1_wpf_app_1_1_u_i_1_1_type_to_brush_converter.html", "class_my_cake_shop_1_1_wpf_app_1_1_u_i_1_1_type_to_brush_converter" ]
        ] ],
        [ "VM", "namespace_my_cake_shop_1_1_wpf_app_1_1_v_m.html", [
          [ "EditorViewModel", "class_my_cake_shop_1_1_wpf_app_1_1_v_m_1_1_editor_view_model.html", "class_my_cake_shop_1_1_wpf_app_1_1_v_m_1_1_editor_view_model" ],
          [ "MainViewModel", "class_my_cake_shop_1_1_wpf_app_1_1_v_m_1_1_main_view_model.html", "class_my_cake_shop_1_1_wpf_app_1_1_v_m_1_1_main_view_model" ]
        ] ],
        [ "App", "class_my_cake_shop_1_1_wpf_app_1_1_app.html", "class_my_cake_shop_1_1_wpf_app_1_1_app" ],
        [ "MyIoc", "class_my_cake_shop_1_1_wpf_app_1_1_my_ioc.html", "class_my_cake_shop_1_1_wpf_app_1_1_my_ioc" ],
        [ "MainWindow", "class_my_cake_shop_1_1_wpf_app_1_1_main_window.html", "class_my_cake_shop_1_1_wpf_app_1_1_main_window" ]
      ] ],
      [ "WpfClient", "namespace_my_cake_shop_1_1_wpf_client.html", [
        [ "AddWindow", "class_my_cake_shop_1_1_wpf_client_1_1_add_window.html", "class_my_cake_shop_1_1_wpf_client_1_1_add_window" ],
        [ "App", "class_my_cake_shop_1_1_wpf_client_1_1_app.html", "class_my_cake_shop_1_1_wpf_client_1_1_app" ],
        [ "CakeVM", "class_my_cake_shop_1_1_wpf_client_1_1_cake_v_m.html", "class_my_cake_shop_1_1_wpf_client_1_1_cake_v_m" ],
        [ "EditorWindow", "class_my_cake_shop_1_1_wpf_client_1_1_editor_window.html", "class_my_cake_shop_1_1_wpf_client_1_1_editor_window" ],
        [ "IMainLogic", "interface_my_cake_shop_1_1_wpf_client_1_1_i_main_logic.html", "interface_my_cake_shop_1_1_wpf_client_1_1_i_main_logic" ],
        [ "MainLogic", "class_my_cake_shop_1_1_wpf_client_1_1_main_logic.html", "class_my_cake_shop_1_1_wpf_client_1_1_main_logic" ],
        [ "MainVM", "class_my_cake_shop_1_1_wpf_client_1_1_main_v_m.html", "class_my_cake_shop_1_1_wpf_client_1_1_main_v_m" ],
        [ "MainWindow", "class_my_cake_shop_1_1_wpf_client_1_1_main_window.html", "class_my_cake_shop_1_1_wpf_client_1_1_main_window" ],
        [ "MyIoc", "class_my_cake_shop_1_1_wpf_client_1_1_my_ioc.html", "class_my_cake_shop_1_1_wpf_client_1_1_my_ioc" ]
      ] ]
    ] ],
    [ "XamlGeneratedNamespace", "namespace_xaml_generated_namespace.html", [
      [ "GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", "class_xaml_generated_namespace_1_1_generated_internal_type_helper" ]
    ] ]
];