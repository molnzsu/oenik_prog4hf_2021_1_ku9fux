var searchData=
[
  ['onconfiguring_390',['OnConfiguring',['../class_my_cake_shop_1_1_data_1_1_cake_shop_db_context.html#a9cb45b574d7e64599c28aa22cb07460e',1,'MyCakeShop::Data::CakeShopDbContext']]],
  ['onmodelcreating_391',['OnModelCreating',['../class_my_cake_shop_1_1_data_1_1_cake_shop_db_context.html#a081f5c435fa38575fc082773d33fc84f',1,'MyCakeShop::Data::CakeShopDbContext']]],
  ['order_392',['Order',['../class_my_cake_shop_1_1_data_1_1_order.html#a9799b1579c6a5e4821cb348238b30d68',1,'MyCakeShop.Data.Order.Order()'],['../class_my_cake_shop_1_1_data_1_1_order.html#a7fc335fbf737eaf6797403b2fa65d960',1,'MyCakeShop.Data.Order.Order(int orderID, int customerID, int cakeID, DateTime orderDate, string orderStatus)']]],
  ['orderlogic_393',['OrderLogic',['../class_my_cake_shop_1_1_logic_1_1_order_logic.html#a4f76f7f30dad43e42db548d71c77335d',1,'MyCakeShop::Logic::OrderLogic']]],
  ['orderrepository_394',['OrderRepository',['../class_my_cake_shop_1_1_repository_1_1_order_repository.html#a9ff3992d09714d0df4737eab0e6807c7',1,'MyCakeShop::Repository::OrderRepository']]]
];
