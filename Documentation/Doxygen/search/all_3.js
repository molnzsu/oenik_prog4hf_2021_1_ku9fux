var searchData=
[
  ['delcake_64',['DelCake',['../class_my_cake_shop_1_1_wpf_app_1_1_b_l_1_1_cake_logic.html#a60c3a19d0ef3c1222d6542437bd2daec',1,'MyCakeShop.WpfApp.BL.CakeLogic.DelCake()'],['../interface_my_cake_shop_1_1_wpf_app_1_1_b_l_1_1_i_cake_logic.html#a164bca5a9217745c2b8677351415026b',1,'MyCakeShop.WpfApp.BL.ICakeLogic.DelCake()']]],
  ['delcmd_65',['DelCmd',['../class_my_cake_shop_1_1_wpf_client_1_1_main_v_m.html#a9127b7051b6cdcac8d826cc4aa2e2f8c',1,'MyCakeShop::WpfClient::MainVM']]],
  ['delcommand_66',['DelCommand',['../class_my_cake_shop_1_1_wpf_app_1_1_v_m_1_1_main_view_model.html#a39f67398d6d8f4a37b2b2a7a0a47985e',1,'MyCakeShop::WpfApp::VM::MainViewModel']]],
  ['delonecakes_67',['DelOneCakes',['../class_my_cake_shop_1_1_web_1_1_controllers_1_1_cakes_api_controller.html#abce64d2c5bdc249d1a83f19979cd867e',1,'MyCakeShop::Web::Controllers::CakesApiController']]],
  ['details_68',['Details',['../class_my_cake_shop_1_1_web_1_1_controllers_1_1_cakes_controller.html#aa226cbaf3c65e24c6231eedca311e2c9',1,'MyCakeShop::Web::Controllers::CakesController']]]
];
