var indexSectionsWithContent =
{
  0: "abcdefghilmnoprstvx",
  1: "aceghimoprstv",
  2: "acmx",
  3: "acdeghimoprst",
  4: "abcdefhilmnoprst",
  5: "t"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "properties",
  5: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Functions",
  4: "Properties",
  5: "Pages"
};

