var searchData=
[
  ['operationresult_456',['OperationResult',['../class_my_cake_shop_1_1_web_1_1_api_result.html#ada19be8d7ce3fadbe270b27df65e94e1',1,'MyCakeShop::Web::ApiResult']]],
  ['orderdate_457',['OrderDate',['../class_my_cake_shop_1_1_data_1_1_order.html#a2b31078149f6af266ddf536a411ade6f',1,'MyCakeShop::Data::Order']]],
  ['orderid_458',['OrderId',['../class_my_cake_shop_1_1_logic_1_1_customer_orders.html#ae64ac5bac3d2ea71ea29cb2edb85139a',1,'MyCakeShop::Logic::CustomerOrders']]],
  ['orderid_459',['OrderID',['../class_my_cake_shop_1_1_data_1_1_order.html#af665d131bc48e549e60e0a3e96b9a3af',1,'MyCakeShop::Data::Order']]],
  ['orderrepository_460',['OrderRepository',['../class_my_cake_shop_1_1_logic_1_1_order_logic.html#a0ca7af4b5c959d5d853f76f459cdd5b7',1,'MyCakeShop.Logic.OrderLogic.OrderRepository()'],['../class_my_cake_shop_1_1_logic_1_1_stock_logic.html#a342b517a84ee87b225bdfdd9c828a45b',1,'MyCakeShop.Logic.StockLogic.OrderRepository()']]],
  ['orders_461',['Orders',['../class_my_cake_shop_1_1_data_1_1_cake_shop_db_context.html#a3e2cb53496fccdb1793ee1ae28cc7596',1,'MyCakeShop.Data.CakeShopDbContext.Orders()'],['../class_my_cake_shop_1_1_data_1_1_cake.html#ab1f6a848128849b914c627419a16c2fd',1,'MyCakeShop.Data.Cake.Orders()'],['../class_my_cake_shop_1_1_data_1_1_customer.html#ac4feedd128cfed05cf3a2c2d6db0df0c',1,'MyCakeShop.Data.Customer.Orders()']]],
  ['orderstatus_462',['OrderStatus',['../class_my_cake_shop_1_1_data_1_1_order.html#a13eb1f3a57165898e550c2c6f4936994',1,'MyCakeShop::Data::Order']]]
];
