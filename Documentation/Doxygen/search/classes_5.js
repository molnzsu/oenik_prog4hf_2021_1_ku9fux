var searchData=
[
  ['icakelogic_246',['ICakeLogic',['../interface_my_cake_shop_1_1_wpf_app_1_1_b_l_1_1_i_cake_logic.html',1,'MyCakeShop::WpfApp::BL']]],
  ['icakerepository_247',['ICakeRepository',['../interface_my_cake_shop_1_1_repository_1_1_i_cake_repository.html',1,'MyCakeShop::Repository']]],
  ['iconfectionerrepository_248',['IConfectionerRepository',['../interface_my_cake_shop_1_1_repository_1_1_i_confectioner_repository.html',1,'MyCakeShop::Repository']]],
  ['icustomerrepository_249',['ICustomerRepository',['../interface_my_cake_shop_1_1_repository_1_1_i_customer_repository.html',1,'MyCakeShop::Repository']]],
  ['ieditorservice_250',['IEditorService',['../interface_my_cake_shop_1_1_wpf_app_1_1_b_l_1_1_i_editor_service.html',1,'MyCakeShop::WpfApp::BL']]],
  ['imainlogic_251',['IMainLogic',['../interface_my_cake_shop_1_1_wpf_client_1_1_i_main_logic.html',1,'MyCakeShop::WpfClient']]],
  ['iorderlogic_252',['IOrderLogic',['../interface_my_cake_shop_1_1_logic_1_1_i_order_logic.html',1,'MyCakeShop::Logic']]],
  ['iorderrepository_253',['IOrderRepository',['../interface_my_cake_shop_1_1_repository_1_1_i_order_repository.html',1,'MyCakeShop::Repository']]],
  ['irepository_254',['IRepository',['../interface_my_cake_shop_1_1_repository_1_1_i_repository.html',1,'MyCakeShop::Repository']]],
  ['irepository_3c_20cake_20_3e_255',['IRepository&lt; Cake &gt;',['../interface_my_cake_shop_1_1_repository_1_1_i_repository.html',1,'MyCakeShop::Repository']]],
  ['irepository_3c_20confectioner_20_3e_256',['IRepository&lt; Confectioner &gt;',['../interface_my_cake_shop_1_1_repository_1_1_i_repository.html',1,'MyCakeShop::Repository']]],
  ['irepository_3c_20customer_20_3e_257',['IRepository&lt; Customer &gt;',['../interface_my_cake_shop_1_1_repository_1_1_i_repository.html',1,'MyCakeShop::Repository']]],
  ['irepository_3c_20order_20_3e_258',['IRepository&lt; Order &gt;',['../interface_my_cake_shop_1_1_repository_1_1_i_repository.html',1,'MyCakeShop::Repository']]],
  ['istocklogic_259',['IStockLogic',['../interface_my_cake_shop_1_1_logic_1_1_i_stock_logic.html',1,'MyCakeShop::Logic']]]
];
