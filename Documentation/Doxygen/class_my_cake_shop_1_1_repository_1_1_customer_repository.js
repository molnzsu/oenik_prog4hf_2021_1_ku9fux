var class_my_cake_shop_1_1_repository_1_1_customer_repository =
[
    [ "CustomerRepository", "class_my_cake_shop_1_1_repository_1_1_customer_repository.html#ae46a0a95b74e02024178d5151bd5a328", null ],
    [ "ChangeAddress", "class_my_cake_shop_1_1_repository_1_1_customer_repository.html#aa8b09a225f562bd19c05da8d17e4bf23", null ],
    [ "ChangeEmail", "class_my_cake_shop_1_1_repository_1_1_customer_repository.html#a01f9cecc6d8f93590dc1016c36735557", null ],
    [ "ChangePhoneNumber", "class_my_cake_shop_1_1_repository_1_1_customer_repository.html#a81527fda2c4dc81f4b9b63db95925f31", null ],
    [ "GetOne", "class_my_cake_shop_1_1_repository_1_1_customer_repository.html#a88d019ed4a5a8a5385ce4f29872ac26c", null ],
    [ "Insert", "class_my_cake_shop_1_1_repository_1_1_customer_repository.html#aef231bf685934eb71d20e2525ca5c4cb", null ],
    [ "Remove", "class_my_cake_shop_1_1_repository_1_1_customer_repository.html#aec2ab535aff0987571281a302c822d86", null ],
    [ "Remove", "class_my_cake_shop_1_1_repository_1_1_customer_repository.html#a8fb15077314e09cf12ce097e59533ccc", null ]
];