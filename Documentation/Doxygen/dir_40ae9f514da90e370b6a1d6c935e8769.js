var dir_40ae9f514da90e370b6a1d6c935e8769 =
[
    [ "MyCakeShop", "dir_2fada928f14dbda928e10161860bfd1c.html", "dir_2fada928f14dbda928e10161860bfd1c" ],
    [ "MyCakeShop.Data", "dir_9ae2a2809a9f64112057868b8584f7a2.html", "dir_9ae2a2809a9f64112057868b8584f7a2" ],
    [ "MyCakeShop.Logic", "dir_0bad1cc5dab372c5b93f291d6549ac94.html", "dir_0bad1cc5dab372c5b93f291d6549ac94" ],
    [ "MyCakeShop.Logic.Tests", "dir_70717b232f68dba0d04453767368ce1b.html", "dir_70717b232f68dba0d04453767368ce1b" ],
    [ "MyCakeShop.Repository", "dir_d594a5f82b5ecd225a61664d299b62e7.html", "dir_d594a5f82b5ecd225a61664d299b62e7" ],
    [ "MyCakeShop.Web", "dir_ee6e6a23a2efc5480b5a3fc43ea89fef.html", "dir_ee6e6a23a2efc5480b5a3fc43ea89fef" ],
    [ "MyCakeShop.WpfApp", "dir_7474446dff3024a7826f6082840db49f.html", "dir_7474446dff3024a7826f6082840db49f" ],
    [ "MyCakeShop.WpfClient", "dir_aa929322d36423bff3ab8d6347518c5b.html", "dir_aa929322d36423bff3ab8d6347518c5b" ]
];