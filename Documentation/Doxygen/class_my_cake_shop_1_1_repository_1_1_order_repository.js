var class_my_cake_shop_1_1_repository_1_1_order_repository =
[
    [ "OrderRepository", "class_my_cake_shop_1_1_repository_1_1_order_repository.html#a9ff3992d09714d0df4737eab0e6807c7", null ],
    [ "ChangeOrderStatus", "class_my_cake_shop_1_1_repository_1_1_order_repository.html#a88536147785b10a4262c89a3eb3d0bec", null ],
    [ "GetOne", "class_my_cake_shop_1_1_repository_1_1_order_repository.html#a435f25917ecc8e5ef628f281022f8e53", null ],
    [ "Insert", "class_my_cake_shop_1_1_repository_1_1_order_repository.html#aa029da6009bbc591839014997c35d640", null ],
    [ "Remove", "class_my_cake_shop_1_1_repository_1_1_order_repository.html#a612966e2a1b2e88101b2bf778c1dd567", null ],
    [ "Remove", "class_my_cake_shop_1_1_repository_1_1_order_repository.html#a16a701151078e226d64ced0255dc82b6", null ]
];