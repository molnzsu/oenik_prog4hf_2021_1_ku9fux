﻿// <copyright file="ProgramC.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyCakeShop.Program
{
    using System;
    using System.Collections.Generic;
    using ConsoleTools;
    using MyCakeShop.Data;
    using MyCakeShop.Logic;
    using MyCakeShop.Repository;

    /// <summary>
    /// Program class.
    /// </summary>
    public static class ProgramC
    {
        /// <summary>
        /// Main class.
        /// </summary>
        public static void Main()
        {
            CakeShopDbContext ctx = new CakeShopDbContext();
            CakeRepository cakeRepo = new CakeRepository(ctx);
            CustomerRepository custRepo = new CustomerRepository(ctx);
            ConfectionerRepository confRepo = new ConfectionerRepository(ctx);
            OrderRepository orderRepo = new OrderRepository(ctx);
            OrderLogic ordlogic = new OrderLogic(orderRepo, custRepo, cakeRepo);
            StockLogic stockLogic = new StockLogic(cakeRepo, orderRepo, confRepo);

            var menu = new ConsoleMenu().
                Add("GetOneCake", () => { GetOneCake(stockLogic); }).
                Add("GetOneCustomer", () => { GetOneCustomer(ordlogic); }).
                Add("GetOneOrder", () => { GetOneOrder(ordlogic); }).
                Add("GetOneConfectioner", () => { GetOneConfectioner(stockLogic); }).
                Add("InsertCake", () => { InsertCake(stockLogic); }).
                Add("InsertConfectioner", () => { InsertConfectioner(stockLogic); }).
                Add("InsertCustomer", () => { InsertCustomer(ordlogic); }).
                Add("InsertOrder", () => { InsertOrder(ordlogic); }).
                Add("GetAllCake", () => { GetAllCake(stockLogic); }).
                Add("GetAllConfectioner", () => { GetAllConfectioner(stockLogic); }).
                Add("GetAllCustomer", () => { GetAllCustomer(ordlogic); }).
                Add("GetAllOrder", () => { GetAllOrder(ordlogic); }).
                Add("RemoveCake", () => { RemoveCake(stockLogic); }).
                Add("RemoveConfectioner", () => { RemoveConfectioner(stockLogic); }).
                Add("RemoveCustomer", () => { RemoveCustomer(ordlogic); }).
                Add("RemoveOrder", () => { RemoveOrder(ordlogic); }).
                Add("ChangePrice", () => { ChangePrice(stockLogic); }).
                Add("ChangePhoneNumber", () => { ChangePhoneNumber(stockLogic); }).
                Add("ChangeEmail", () => { ChangeEmail(ordlogic); }).
                Add("ChangeOrderStatus", () => { ChangeOrderStatus(ordlogic); }).
                Add("Customer with the ID of their active orders", () => CustomerOrders(ordlogic)).
                Add("How many cakes the confectioners made", () => ConfSumCake(stockLogic)).
                Add("The average popularity of cakes by type", () => AverageResult(stockLogic)).
                Add("Close", ConsoleMenu.Close);

            menu.Show();
            ctx.Dispose();
        }

        private static void ChangePrice(StockLogic logic)
        {
            Console.WriteLine("Enter the id:");
            int id = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter the new price:");
            int newprice = int.Parse(Console.ReadLine());
            logic.ChangePrice(id, newprice);
        }

        private static void ChangePhoneNumber(StockLogic logic)
        {
            Console.WriteLine("Enter the id:");
            int id = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter the new phone number:");
            string newnumber = Console.ReadLine();
            logic.ChangePhoneNumber(id, newnumber);
        }

        private static void ChangeEmail(OrderLogic logic)
        {
            Console.WriteLine("Enter the id:");
            int id = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter the new email:");
            string newemail = Console.ReadLine();
            logic.ChangeEmail(id, newemail);
        }

        private static void ChangeOrderStatus(OrderLogic logic)
        {
            Console.WriteLine("Enter the id:");
            int id = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter the new status:");
            string newstatus = Console.ReadLine();
            logic.ChangeOrderStatus(id, newstatus);
        }

        private static void RemoveCake(StockLogic logic)
        {
            Console.WriteLine("Which item is deleted?");
            int id = int.Parse(Console.ReadLine());
            Cake ob = logic.GetOneCake(id);
            if (ob != null)
            {
                logic.RemoveCake(ob);
                Console.WriteLine("Successful deletion");
            }
            else
            {
                Console.WriteLine("Not found!");
            }

            Console.ReadLine();
        }

        private static void RemoveConfectioner(StockLogic logic)
        {
            Console.WriteLine("Which item is deleted?");
            int id = int.Parse(Console.ReadLine());
            Confectioner ob = logic.GetOneConfectioner(id);
            if (ob != null)
            {
                logic.RemoveConfectioner(ob);
                Console.WriteLine("Successful deletion");
            }
            else
            {
                Console.WriteLine("Not found!");
            }

            Console.ReadLine();
        }

        private static void RemoveCustomer(OrderLogic logic)
        {
            Console.WriteLine("Which item is deleted?");
            int id = int.Parse(Console.ReadLine());
            Customer ob = logic.GetOneCustomer(id);
            if (ob != null)
            {
                logic.RemoveCustomer(ob);
                Console.WriteLine("Successful deletion");
            }
            else
            {
                Console.WriteLine("Not found!");
            }

            Console.ReadLine();
        }

        private static void RemoveOrder(OrderLogic logic)
        {
            Console.WriteLine("Which item is deleted?");
            int id = int.Parse(Console.ReadLine());
            Order ob = logic.GetOneOrder(id);
            if (ob != null)
            {
                logic.RemoveOrder(ob);
                Console.WriteLine("Successful deletion");
            }
            else
            {
                Console.WriteLine("Not found!");
            }

            Console.ReadLine();
        }

        private static void GetAllCake(StockLogic logic)
        {
            IList<Cake> cakes = logic.GetAllCakes();
            foreach (var item in cakes)
            {
                Console.WriteLine(item.ToString());
            }

            Console.ReadLine();
        }

        private static void GetAllConfectioner(StockLogic logic)
        {
            IList<Confectioner> confs = logic.GetAllConfectioner();
            foreach (var item in confs)
            {
                Console.WriteLine(item.ToString());
            }

            Console.ReadLine();
        }

        private static void GetAllCustomer(OrderLogic logic)
        {
            IList<Customer> customs = logic.GetAllCustomers();
            foreach (var item in customs)
            {
                Console.WriteLine(item.ToString());
            }

            Console.ReadLine();
        }

        private static void GetAllOrder(OrderLogic logic)
        {
            IList<Order> orders = logic.GetAllOrder();
            foreach (var item in orders)
            {
                Console.WriteLine(item.ToString());
            }

            Console.ReadLine();
        }

        private static void InsertCake(StockLogic logic)
        {
            Console.WriteLine("Enter the name of the cake to be inserted");
            string name = Console.ReadLine();
            Console.WriteLine("Enter the type:");
            string type = Console.ReadLine();
            Cake cake = new Cake()
            {
                Name = name,
                Type = type,
            };
            logic.InsertCake(cake);
            Console.WriteLine("Successful insertion!");
            Console.ReadLine();
        }

        private static void InsertConfectioner(StockLogic logic)
        {
            Console.WriteLine("Enter the lastname of the confectioner to be inserted");
            string lastname = Console.ReadLine();
            Console.WriteLine("Enter the firstname of the confectioner to be inserted");
            string firstname = Console.ReadLine();
            Confectioner conf = new Confectioner()
            {
                LastName = lastname,
                FirstName = firstname,
            };
            logic.InsertConfectioner(conf);
            Console.WriteLine("Successful insertion!");
            Console.ReadLine();
        }

        private static void InsertCustomer(OrderLogic logic)
        {
            Console.WriteLine("Enter the lastname of the customer to be inserted");
            string lastname = Console.ReadLine();
            Console.WriteLine("Enter the firstname of the customer to be inserted");
            string firstname = Console.ReadLine();
            Customer cust = new Customer()
            {
                LastName = lastname,
                FirstName = firstname,
            };
            logic.InsertCustomer(cust);
            Console.WriteLine("Successful insertion!");
            Console.ReadLine();
        }

        private static void InsertOrder(OrderLogic logic)
        {
            Console.WriteLine("Enter the customerId of the order to be inserted");
            int custID = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter the cakeID of the order to be inserted");
            int cakeId = int.Parse(Console.ReadLine());
            Order order = new Order()
            {
                CustomerID = custID,
                CakeID = cakeId,
                OrderStatus = "Aktív",
            };
            logic.InsertOrder(order);
            Console.WriteLine("Successful insertion!");
            Console.ReadLine();
        }

        private static void GetOneCake(StockLogic logic)
        {
            Console.WriteLine("Enter an identification number");
            string idszoveg = Console.ReadLine();
            int id = int.Parse(idszoveg);
            Cake cake = logic.GetOneCake(id);
            if (cake != null)
            {
                string cake1 = cake.ToString();
                Console.WriteLine(cake1);
            }
            else
            {
                string value = "Id is not found";
                Console.WriteLine(value);
            }

            Console.ReadLine();
        }

        private static void GetOneCustomer(OrderLogic logic)
        {
            Console.WriteLine("Enter an identification number");
            string idszoveg = Console.ReadLine();
            int id = int.Parse(idszoveg);
            Customer cust = logic.GetOneCustomer(id);
            if (cust != null)
            {
                string cust1 = cust.ToString();
                Console.WriteLine(cust1);
            }
            else
            {
                Console.WriteLine("Id is not found");
            }

            Console.ReadLine();
        }

        private static void GetOneConfectioner(StockLogic logic)
        {
            Console.WriteLine("Enter an identification number");
            string idszoveg = Console.ReadLine();
            int id = int.Parse(idszoveg);
            Confectioner conf = logic.GetOneConfectioner(id);
            if (conf != null)
            {
                string conf1 = conf.ToString();
                Console.WriteLine(conf1);
            }
            else
            {
                Console.WriteLine("Id is not found");
            }

            Console.ReadLine();
        }

        private static void GetOneOrder(OrderLogic logic)
        {
            Console.WriteLine("Enter an identification number");
            string idszoveg = Console.ReadLine();
            int id = int.Parse(idszoveg);
            Order ord = logic.GetOneOrder(id);
            if (ord != null)
            {
                string ord1 = ord.ToString();
                Console.WriteLine(ord1);
            }
            else
            {
                Console.WriteLine("Id is not found");
            }

            Console.ReadLine();
        }

        private static void CustomerOrders(OrderLogic logic)
        {
            var items = logic.GetCustomerOrdersTaks();
            foreach (var item in items.Result)
            {
                Console.WriteLine(item);
            }

            Console.ReadLine();
        }

        private static void ConfSumCake(StockLogic logic)
        {
            var items = logic.GetConfSumCakeTask();
            foreach (var item in items.Result)
            {
                Console.WriteLine(item);
            }

            Console.ReadLine();
        }

        private static void AverageResult(StockLogic logic)
        {
            var items = logic.GetAverageResultTaks();
            foreach (var item in items.Result)
            {
                Console.WriteLine(item);
            }

            Console.ReadLine();
        }
    }
}
