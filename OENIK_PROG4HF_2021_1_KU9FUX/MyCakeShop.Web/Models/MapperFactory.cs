﻿// <copyright file="MapperFactory.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyCakeShop.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;

    /// <summary>
    /// Mapper factory.
    /// </summary>
    public static class MapperFactory
    {
        /// <summary>
        /// Mapper creater.
        /// </summary>
        /// <returns>IMapper.</returns>
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<MyCakeShop.Data.Cake, MyCakeShop.Web.Models.Cake>().
                    ForMember(dest => dest.Id, map => map.MapFrom(src => src.CakeID)).
                    ForMember(dest => dest.Name, map => map.MapFrom(src => src.Name)).
                    ForMember(dest => dest.Type, map => map.MapFrom(src => src.Type)).
                    ForMember(dest => dest.MadeDate, map => map.MapFrom(src => src.MadeDate)).
                    ForMember(dest => dest.ExpirationDate, map => map.MapFrom(src => src.ExpirationDate)).
                    ForMember(dest => dest.Price, map => map.MapFrom(src => src.Price)).
                    ForMember(dest => dest.LactoseFree, map => map.MapFrom(src => src.LactoseFree)).
                    ForMember(dest => dest.Popularity, map => map.MapFrom(src => src.Popularity)).
                    ForMember(dest => dest.MakerId, map => map.MapFrom(src => src.MakerID)).ReverseMap();
            });
            return config.CreateMapper();
        }

        /// <summary>
        /// Convert to WebCake.
        /// </summary>
        /// <param name="cakes">Data cake.</param>
        /// <returns>Web cake.</returns>
        public static List<MyCakeShop.Web.Models.Cake> ConvertToWebCakes(List<MyCakeShop.Data.Cake> cakes)
        {
            List<MyCakeShop.Web.Models.Cake> result = new List<Cake>();
            if (cakes != null)
            {
                foreach (var cake in cakes)
                {
                    result.Add(
                        new Cake()
                        {
                            Id = cake.CakeID,
                            Type = cake.Type,
                            Name = cake.Name,
                            Price = cake.Price,
                            MadeDate = cake.MadeDate,
                            ExpirationDate = cake.ExpirationDate,
                            LactoseFree = cake.LactoseFree,
                            Popularity = cake.Popularity,
                            MakerId = cake.MakerID,
                        });
                }
            }

            return result;
        }
    }
}
