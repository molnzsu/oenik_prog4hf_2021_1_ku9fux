﻿// <copyright file="Cake.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyCakeShop.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// Cake class.
    /// </summary>
    public class Cake
    {
        /// <summary>
        /// Gets or sets property. The ID of the cake.
        /// </summary>
        [Display(Name = "Cake id")]
        [Required]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets cake name.
        /// </summary>
        [Display(Name = "Cake name")]
        [Required]
        [StringLength(30, MinimumLength = 3)]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets property. The type of cake.
        /// </summary>
        [Display(Name = "Cake type")]
        [Required]
        [StringLength(30, MinimumLength = 3)]
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets property. The date the cake was made.
        /// </summary>
        [Display(Name = "Cake making date")]
        [Required]
        public DateTime MadeDate { get; set; }

        /// <summary>
        /// Gets or sets property. The expiration date of the cake.
        /// </summary>
        [Display(Name = "Cake expiration date")]
        [Required]
        public DateTime ExpirationDate { get; set; }

        /// <summary>
        /// Gets or sets property. The price of the cake in HUF per piece.
        /// </summary>
        [Display(Name = "Cake price")]
        [Required]
        public int Price { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether gets or sets property. This cake lactose free.
        /// </summary>
        [Display(Name = "Cake is lactose free")]
        [Required]
        public bool LactoseFree { get; set; }

        /// <summary>
        /// Gets or sets property. How popular the particular cake is among consumers.
        /// </summary>
        [Display(Name = "Cake popularity")]
        [Required]
        [Range(1, 5)]
        public int Popularity { get; set; }

        /// <summary>
        /// Gets or sets property. The ID of the maker of the cake.
        /// </summary>
        [Display(Name = "Cake maker id")]
        [Required]
        public int MakerId { get; set; }
    }
}
