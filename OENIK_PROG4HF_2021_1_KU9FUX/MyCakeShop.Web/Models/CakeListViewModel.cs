﻿// <copyright file="CakeListViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyCakeShop.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// Cake list view model.
    /// </summary>
    public class CakeListViewModel
    {
        /// <summary>
        /// Gets or sets list of cakes.
        /// </summary>
        public List<Cake> ListOfCakes { get; set; }

        /// <summary>
        /// Gets or sets edited cake.
        /// </summary>
        public Cake EditedCake { get; set; }
    }
}
