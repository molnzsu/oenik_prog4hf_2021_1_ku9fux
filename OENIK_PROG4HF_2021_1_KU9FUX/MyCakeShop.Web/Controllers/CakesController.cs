﻿// <copyright file="CakesController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyCakeShop.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using Microsoft.AspNetCore.Mvc;
    using MyCakeShop.Logic;
    using MyCakeShop.Web.Models;

    /// <summary>
    /// Cake controller.
    /// </summary>
    public class CakesController : Controller
    {
        private IStockLogic logic;
        private IMapper mapper;
        private CakeListViewModel vm;

        /// <summary>
        /// Initializes a new instance of the <see cref="CakesController"/> class.
        /// </summary>
        /// <param name="logic">Cake logic.</param>
        /// <param name="mapper">Mapper.</param>
        public CakesController(IStockLogic logic, IMapper mapper)
        {
            this.logic = logic;
            this.mapper = mapper;

            this.vm = new CakeListViewModel();
            this.vm.EditedCake = new Models.Cake();
            if (logic != null && mapper != null)
            {
                var cakes = logic.GetAllCakes();
                this.vm.ListOfCakes = mapper.Map<IList<Data.Cake>, List<Models.Cake>>(cakes);
            }
        }

        /// <summary>
        /// Index.
        /// </summary>
        /// <returns>Action result.</returns>
        public IActionResult Index()
        {
            this.ViewData["editAction"] = "AddNew";
            return this.View("CakesIndex", this.vm);
        }

        /// <summary>
        /// Details.
        /// </summary>
        /// <param name="id">Cake id.</param>
        /// <returns>Action result.</returns>
        public IActionResult Details(int id)
        {
            return this.View("CakesDetails", this.GetCakeModel(id));
        }

        /// <summary>
        /// Remove.
        /// </summary>
        /// <param name="id">Cake id.</param>
        /// <returns>Action result.</returns>
        public IActionResult Remove(int id)
        {
            this.TempData["editResult"] = "Delete FAIL";
            if (this.logic.RemoveCake(id))
            {
                this.TempData["editResult"] = "Delete OK";
            }

            return this.RedirectToAction(nameof(this.Index));
        }

        /// <summary>
        /// Add.
        /// </summary>
        /// <returns>Action result.</returns>
        public IActionResult Add()
        {
            this.ViewData["editAction"] = "AddNew";
            Models.CakeListViewModel vm = new Models.CakeListViewModel();
            vm.ListOfCakes = MyCakeShop.Web.Models.MapperFactory.ConvertToWebCakes(this.logic.GetAllCakes().ToList());
            vm.EditedCake = new Models.Cake();

            return this.View("CakesEdit2", vm);
        }

        /// <summary>
        /// Edit.
        /// </summary>
        /// <param name="id">Cake id.</param>
        /// <returns>Action result.</returns>
        public IActionResult Edit(int id)
        {
            Models.CakeListViewModel vm = new Models.CakeListViewModel();
            this.ViewData["editAction"] = "Edit";
            vm.EditedCake = this.GetCakeModel(id);
            return this.View("CakesEdit", vm);
        }

        /// <summary>
        /// Edit.
        /// </summary>
        /// <param name="vm">View Model.</param>
        /// <param name="editAction">Edit action.</param>
        /// <returns>Action result.</returns>
        [HttpPost]
        public IActionResult Edit(Models.CakeListViewModel vm, string editAction)
        {
            if (vm.EditedCake != null)
            {
                var cake = vm.EditedCake;
                if (editAction == "AddNew")
                {
                    try
                    {
                        this.logic.InsertCake(cake.Name, cake.Type, cake.MadeDate, cake.ExpirationDate, cake.Price, cake.LactoseFree, cake.Popularity, cake.MakerId);
                        this.TempData["editResult"] = "The addition was successful";
                    }
                    catch (ArgumentException ex)
                    {
                        this.TempData["editResult"] = "The addition failed: " + ex.Message;
                    }
                }
                else
                {
                    if (this.logic.ChangePrice(cake.Id, cake.Price) && this.logic.ChangePopularity(cake.Id, cake.Popularity) && this.logic.ChangeExpirationDate(cake.Id, cake.ExpirationDate))
                    {
                        this.TempData["editResult"] = "The change was successful";
                    }
                    else
                    {
                        this.TempData["editResult"] = "The change failed";
                    }
                }

                return this.RedirectToAction(nameof(this.Index));
            }
            else
            {
                if (vm != null)
                {
                    this.ViewData["editAction"] = "Edit";
                    vm.EditedCake = vm.EditedCake;
                    return this.View("CakesEdit", vm);
                }
                else
                {
                    return null;
                }
            }
        }

        private Models.Cake GetCakeModel(int id)
        {
            Data.Cake oneCake = this.logic.GetOneCake(id);
            return this.mapper.Map<Data.Cake, Models.Cake>(oneCake);
        }
    }
}
