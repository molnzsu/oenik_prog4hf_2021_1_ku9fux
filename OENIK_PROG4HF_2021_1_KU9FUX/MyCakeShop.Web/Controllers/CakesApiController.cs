﻿// <copyright file="CakesApiController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyCakeShop.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using Microsoft.AspNetCore.Mvc;
    using MyCakeShop.Logic;

    /// <summary>
    /// Cakes api controller class.
    /// </summary>
    public class CakesApiController : Controller
    {
        private IStockLogic logic;
        private IMapper mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="CakesApiController"/> class.
        /// </summary>
        /// <param name="logic">logic.</param>
        /// <param name="mapper">mapper.</param>
        public CakesApiController(IStockLogic logic, IMapper mapper)
        {
            this.logic = logic;
            this.mapper = mapper;
        }

        /// <summary>
        /// Get all cake.
        /// </summary>
        /// <returns>enumerable.</returns>
        [HttpGet]
        [ActionName("all")]
        public IEnumerable<Models.Cake> GetAll()
        {
            var cakes = this.logic.GetAllCakes();
            return this.mapper.Map<IList<Data.Cake>, List<Models.Cake>>(cakes);
        }

        /// <summary>
        /// Delete one cake.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>api result.</returns>
        [HttpGet]
        [ActionName("del")]
        public ApiResult DelOneCakes(int id)
        {
            return new ApiResult() { OperationResult = this.logic.RemoveCake(id) };
        }

        /// <summary>
        /// Add one cake.
        /// </summary>
        /// <param name="cake">cake.</param>
        /// <returns>api result.</returns>
        [HttpPost]
        [ActionName("add")]
        public ApiResult AddOneCake(Models.Cake cake)
        {
            bool success = true;
            try
            {
                if (cake != null)
                {
                    this.logic.InsertCake(cake.Name, cake.Type, cake.MadeDate, cake.ExpirationDate, cake.Price, cake.LactoseFree, cake.Popularity, cake.MakerId);
                }
            }
            catch (ArgumentException)
            {
                success = false;
            }

            return new ApiResult() { OperationResult = success };
        }

        /// <summary>
        /// Modify one cake.
        /// </summary>
        /// <param name="cake">cake.</param>
        /// <returns>api result.</returns>
        [HttpPost]
        [ActionName("mod")]
        public ApiResult ModOneCake(Models.Cake cake)
        {
            if (cake != null)
            {
                return new ApiResult() { OperationResult = this.logic.ChangeExpirationDate(cake.Id, cake.ExpirationDate) && this.logic.ChangePopularity(cake.Id, cake.Popularity) && this.logic.ChangePrice(cake.Id, cake.Price) };
            }
            else
            {
                return new ApiResult() { OperationResult = false };
            }
        }
    }
}
