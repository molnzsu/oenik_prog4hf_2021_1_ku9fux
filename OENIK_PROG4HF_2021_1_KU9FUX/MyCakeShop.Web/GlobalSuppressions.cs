﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Design", "CA1002:Do not expose generic lists", Justification = "Uninteresting", Scope = "member", Target = "~P:MyCakeShop.Web.Models.CakeListViewModel.ListOfCakes")]
[assembly: SuppressMessage("Design", "CA1002:Do not expose generic lists", Justification = "Uninteresting", Scope = "member", Target = "~M:MyCakeShop.Web.Models.MapperFactory.ConvertToWebCakes(System.Collections.Generic.List{MyCakeShop.Data.Cake})~System.Collections.Generic.List{MyCakeShop.Web.Models.Cake}")]
[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "Uninteresting", Scope = "member", Target = "~P:MyCakeShop.Web.Models.CakeListViewModel.ListOfCakes")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "Uninteresting", Scope = "member", Target = "~M:MyCakeShop.Web.Controllers.CakesController.Edit(MyCakeShop.Web.Models.CakeListViewModel,System.String)~Microsoft.AspNetCore.Mvc.IActionResult")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1014:MarkAssembliesWithCLSCompliant", Justification = "Uninteresting")]