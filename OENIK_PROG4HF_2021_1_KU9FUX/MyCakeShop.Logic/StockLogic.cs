﻿// <copyright file="StockLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyCakeShop.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using MyCakeShop.Data;
    using MyCakeShop.Repository;

    /// <summary>
    /// Logic class for the Stock.
    /// </summary>
    public class StockLogic : IStockLogic
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StockLogic"/> class.
        /// </summary>
        /// <param name="cakeRepository">cake repository.</param>
        /// <param name="orderRepository">order repository.</param>
        /// <param name="confectionerRepository">confectioner repository.</param>
        public StockLogic(ICakeRepository cakeRepository, IOrderRepository orderRepository, IConfectionerRepository confectionerRepository)
        {
            this.CakeRepository = cakeRepository;
            this.OrderRepository = orderRepository;
            this.ConfectionerRepository = confectionerRepository;
        }

        /// <summary>
        /// Gets cake repository.
        /// </summary>
        public ICakeRepository CakeRepository { get; }

        /// <summary>
        /// Gets confectioner repository.
        /// </summary>
        public IConfectionerRepository ConfectionerRepository { get; }

        /// <summary>
        /// Gets order repository.
        /// </summary>
        public IOrderRepository OrderRepository { get; }

        /// <summary>
        /// Change phone number.
        /// </summary>
        /// <param name="id">Identification number what object what I want to update.</param>
        /// <param name="newemail">New email.</param>
        public void ChangeEmail(int id, string newemail)
        {
            this.ConfectionerRepository.ChangeEmail(id, newemail);
        }

        /// <summary>
        /// Change expiration date.
        /// </summary>
        /// <param name="id">Identification number what object what I want to update.</param>
        /// <param name="newdate">New date.</param>
        /// <returns>True is succesfull.</returns>
        public bool ChangeExpirationDate(int id, DateTime newdate)
        {
            return this.CakeRepository.ChangeExpirationDate(id, newdate);
        }

        /// <summary>
        /// Change phone number.
        /// </summary>
        /// <param name="id">Identification number what object what I want to update.</param>
        /// <param name="newphonenumber">New phone number.</param>
        public void ChangePhoneNumber(int id, string newphonenumber)
        {
            this.ConfectionerRepository.ChangePhoneNumber(id, newphonenumber);
        }

        /// <summary>
        /// Change popularity.
        /// </summary>
        /// <param name="id">Identification number what object what I want to update.</param>
        /// <param name="newpopularity">New popularity.</param>
        /// <returns>True is succesfull.</returns>
        public bool ChangePopularity(int id, int newpopularity)
        {
            return this.CakeRepository.ChangePopularity(id, newpopularity);
        }

        /// <summary>
        /// Change the price.
        /// </summary>
        /// <param name="id">Identification number what object what I want to update.</param>
        /// <param name="newprice">New price.</param>
        /// <returns>True is succesfull.</returns>
        public bool ChangePrice(int id, int newprice)
        {
            return this.CakeRepository.ChangePrice(id, newprice);
        }

        /// <summary>
        /// Return all cake.
        /// </summary>
        /// <returns>cake list.</returns>
        public IList<Cake> GetAllCakes()
        {
            return this.CakeRepository.GetAll().ToList();
        }

        /// <summary>
        /// Return all confectioner.
        /// </summary>
        /// <returns>confectioner list.</returns>
        public IList<Confectioner> GetAllConfectioner()
        {
            return this.ConfectionerRepository.GetAll().ToList();
        }

        /// <summary>
        /// Returns all Order type objects.
        /// </summary>
        /// <returns>All entity which is type of Order.</returns>
        public IList<Order> GetAllOrder()
        {
            return this.OrderRepository.GetAll().ToList();
        }

        /// <summary>
        /// Confectioners number of cakes.
        /// </summary>
        /// <returns>number of cakes group by confectioner.</returns>
        public IList<ConfSumCake> GetConfSumCake()
        {
            var q = from conf in this.ConfectionerRepository.GetAll()
                    join cake in this.CakeRepository.GetAll() on conf.ConfectionerID equals cake.MakerID
                    group conf by new { conf.ConfectionerID, conf.LastName, conf.FirstName } into grp
                    select new ConfSumCake
                    {
                        Id = grp.Key.ConfectionerID,
                        ConfName = grp.Key.LastName + " " + grp.Key.FirstName,
                        SumCake = grp.Count(),
                    };
            return q.ToList();
        }

        /// <summary>
        /// Task for ConfSumCake.
        /// </summary>
        /// <returns>number of cakes group by confectioner.</returns>
        public Task<IList<ConfSumCake>> GetConfSumCakeTask()
        {
            Task<IList<ConfSumCake>> task = new Task<IList<ConfSumCake>>(() => this.GetConfSumCake());
            task.Start();
            return task;
        }

        /// <summary>
        /// Returns a Cake type object based on id.
        /// </summary>
        /// <param name="id">Entity identification number.</param>
        /// <returns>One cake.</returns>
        public Cake GetOneCake(int id)
        {
            return this.CakeRepository.GetOne(id);
        }

        /// <summary>
        /// Return van confectioner.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>one confectioner.</returns>
        public Confectioner GetOneConfectioner(int id)
        {
            return this.ConfectionerRepository.GetOne(id);
        }

        /// <summary>
        /// Return van confectioner.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>one confectioner.</returns>
        public Order GetOneOrder(int id)
        {
            return this.OrderRepository.GetOne(id);
        }

        /// <summary>
        /// Popularity average result group by type.
        /// </summary>
        /// <returns>averages.</returns>
        public IList<AverageResult> GetTypeAverages()
        {
            var q = from cake in this.CakeRepository.GetAll()
                    join order in this.OrderRepository.GetAll() on cake.CakeID equals order.CakeID
                    group cake by cake.Type into grp
                    orderby grp.Key
                    select new AverageResult()
                    {
                        Type = grp.Key,
                        AveragePopularity = (int)grp.Average(x => x.Popularity),
                    };
            return q.ToList();
        }

        /// <summary>
        /// Task for AverageResult.
        /// </summary>
        /// <returns>averages.</returns>
        public Task<IList<AverageResult>> GetAverageResultTaks()
        {
            Task<IList<AverageResult>> task = new Task<IList<AverageResult>>(() => this.GetTypeAverages());
            task.Start();
            return task;
        }

        /// <summary>
        /// Insert cake.
        /// </summary>
        /// <param name="cake">cake.</param>
        public void InsertCake(Cake cake)
        {
            this.CakeRepository.Insert(cake);
        }

        /// <summary>
        /// Insert confectioner.
        /// </summary>
        /// <param name="conf">confectioner.</param>
        public void InsertConfectioner(Confectioner conf)
        {
            this.ConfectionerRepository.Insert(conf);
        }

        /// <summary>
        /// Remove cake.
        /// </summary>
        /// <param name="cake">cake.</param>
        public void RemoveCake(Cake cake)
        {
            this.CakeRepository.Remove(cake);
        }

        /// <summary>
        /// Remove cake.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>True is succesfull.</returns>
        public bool RemoveCake(int id)
        {
            return this.CakeRepository.Remove(id);
        }

        /// <summary>
        /// Remove confectioner.
        /// </summary>
        /// <param name="conf">confectioner.</param>
        public void RemoveConfectioner(Confectioner conf)
        {
            this.ConfectionerRepository.Remove(conf);
        }

        /// <summary>
        /// Remove confectioner.
        /// </summary>
        /// <param name="id">id.</param>
        public void RemoveConfectioner(int id)
        {
            this.ConfectionerRepository.Remove(id);
        }

        /// <summary>
        /// Insert cake.
        /// </summary>
        /// <param name="name">Name.</param>
        /// <param name="type">Type.</param>
        /// <param name="mdate">Made date.</param>
        /// <param name="edate">Expiration date.</param>
        /// <param name="price">Price.</param>
        /// <param name="lactosefree">Lactose free.</param>
        /// <param name="popularity">Popularity.</param>
        /// <param name="makerid">Maker id.</param>
        public void InsertCake(string name, string type, DateTime mdate, DateTime edate, int price, bool lactosefree, int popularity, int makerid)
        {
            Cake cake = new Cake();
            cake.Name = name;
            cake.Type = type;
            cake.MadeDate = mdate;
            cake.ExpirationDate = edate;
            cake.Price = price;
            cake.LactoseFree = lactosefree;
            cake.Popularity = popularity;
            cake.MakerID = makerid;
            this.CakeRepository.Insert(cake);
        }
    }
}
