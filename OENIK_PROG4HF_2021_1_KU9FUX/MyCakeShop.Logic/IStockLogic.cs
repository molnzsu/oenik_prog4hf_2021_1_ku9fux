﻿// <copyright file="IStockLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyCakeShop.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using MyCakeShop.Data;

    /// <summary>
    /// Logic interface for the Stock.
    /// </summary>
    public interface IStockLogic
    {
        /// <summary>
        /// Returns a Cake type object based on id.
        /// </summary>
        /// <param name="id">Entity identification number.</param>
        /// <returns>One cake.</returns>
        Cake GetOneCake(int id);

        /// <summary>
        /// Return van confectioner.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>one confectioner.</returns>
        Confectioner GetOneConfectioner(int id);

        /// <summary>
        /// Returns a Order type object based on id.
        /// </summary>
        /// <param name="id">Entity identification number.</param>
        /// <returns>One entity.</returns>
        Order GetOneOrder(int id);

        /// <summary>
        /// Return all cake.
        /// </summary>
        /// <returns>cake list.</returns>
        IList<Cake> GetAllCakes();

        /// <summary>
        /// Return all confectioner.
        /// </summary>
        /// <returns>confectioner list.</returns>
        IList<Confectioner> GetAllConfectioner();

        /// <summary>
        /// Returns all Order type objects.
        /// </summary>
        /// <returns>All entity which is type of Order.</returns>
        IList<Order> GetAllOrder();

        /// <summary>
        /// Change the price.
        /// </summary>
        /// <param name="id">Identification number what object what I want to update.</param>
        /// <param name="newprice">New price.</param>
        /// <returns>True is succesfull.</returns>
        bool ChangePrice(int id, int newprice);

        /// <summary>
        /// Change expiration date.
        /// </summary>
        /// <param name="id">Identification number what object what I want to update.</param>
        /// <param name="newdate">New date.</param>
        /// <returns>True is succesfull.</returns>
        bool ChangeExpirationDate(int id, DateTime newdate);

        /// <summary>
        /// Change popularity.
        /// </summary>
        /// <param name="id">Identification number what object what I want to update.</param>
        /// <param name="newpopularity">New popularity.</param>
        /// <returns>True is succesfull.</returns>
        bool ChangePopularity(int id, int newpopularity);

        /// <summary>
        /// Change phone number.
        /// </summary>
        /// <param name="id">Identification number what object what I want to update.</param>
        /// <param name="newphonenumber">New phone number.</param>
        void ChangePhoneNumber(int id, string newphonenumber);

        /// <summary>
        /// Change phone number.
        /// </summary>
        /// <param name="id">Identification number what object what I want to update.</param>
        /// <param name="newemail">New email.</param>
        void ChangeEmail(int id, string newemail);

        /// <summary>
        /// Insert cake.
        /// </summary>
        /// <param name="cake">cake.</param>
        void InsertCake(Cake cake);

        /// <summary>
        /// Insert cake.
        /// </summary>
        /// <param name="name">Name.</param>
        /// <param name="type">Type.</param>
        /// <param name="mdate">Made date.</param>
        /// <param name="edate">Exp date.</param>
        /// <param name="price">Price.</param>
        /// <param name="lactosefree">Lactose free.</param>
        /// <param name="popularity">Popularity.</param>
        /// <param name="makerid">Maker id.</param>
        void InsertCake(string name, string type, DateTime mdate, DateTime edate, int price, bool lactosefree, int popularity, int makerid);

        /// <summary>
        /// Insert confectioner.
        /// </summary>
        /// <param name="conf">confectioner.</param>
        void InsertConfectioner(Confectioner conf);

        /// <summary>
        /// Remove cake.
        /// </summary>
        /// <param name="cake">cake.</param>
        void RemoveCake(Cake cake);

        /// <summary>
        /// Remove cake.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>True is succesfull.</returns>
        bool RemoveCake(int id);

        /// <summary>
        /// Remove confectioner.
        /// </summary>
        /// <param name="conf">confectioner.</param>
        void RemoveConfectioner(Confectioner conf);

        /// <summary>
        /// Remove confectioner.
        /// </summary>
        /// <param name="id">id.</param>
        void RemoveConfectioner(int id);

        /// <summary>
        /// Popularity average result group by type.
        /// </summary>
        /// <returns>averages.</returns>
        IList<AverageResult> GetTypeAverages();

        /// <summary>
        /// Confectioners number of cakes.
        /// </summary>
        /// <returns>number of cakes group by confectioner.</returns>
        IList<ConfSumCake> GetConfSumCake();
    }
}
