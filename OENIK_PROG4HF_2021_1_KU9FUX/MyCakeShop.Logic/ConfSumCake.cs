﻿// <copyright file="ConfSumCake.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyCakeShop.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Class for confectioners number of pastries.
    /// </summary>
    public class ConfSumCake
    {
        /// <summary>
        /// Gets or sets identification number parameter.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets confectioner name parameter.
        /// </summary>
        public string ConfName { get; set; }

        /// <summary>
        /// Gets or sets identification number parameter.
        /// </summary>
        public int SumCake { get; set; }

        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns>A string that represents the current object.</returns>
        public override string ToString()
        {
            return $"Id = {this.Id}, Name = {this.ConfName}, SumCake = {this.SumCake}";
        }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">object.</param>
        /// <returns>True if the specified object is equal to the current object, otherwise, false.</returns>
        public override bool Equals(object obj)
        {
            if (obj is ConfSumCake)
            {
                ConfSumCake other = obj as ConfSumCake;
                return this.Id == other.Id && this.ConfName == other.ConfName && this.SumCake == other.SumCake;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            return this.Id + this.SumCake;
        }
    }
}
