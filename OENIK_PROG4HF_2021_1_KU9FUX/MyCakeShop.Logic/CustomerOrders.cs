﻿// <copyright file="CustomerOrders.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyCakeShop.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Class for customers active orders.
    /// </summary>
    public class CustomerOrders
    {
        /// <summary>
        /// Gets or sets Id parameter.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets name parameter.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets order id parameter.
        /// </summary>
        public int OrderId { get; set; }

        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns>A string that represents the current object.</returns>
        public override string ToString()
        {
            return $"Id = {this.Id}, Name = {this.Name}, OrderId = {this.OrderId}";
        }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">object.</param>
        /// <returns>True if the specified object is equal to the current object, otherwise, false.</returns>
        public override bool Equals(object obj)
        {
            if (obj is CustomerOrders)
            {
                CustomerOrders other = obj as CustomerOrders;
                return this.Id == other.Id && this.Name == other.Name && this.OrderId == other.OrderId;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            return this.Id + this.OrderId;
        }
    }
}
