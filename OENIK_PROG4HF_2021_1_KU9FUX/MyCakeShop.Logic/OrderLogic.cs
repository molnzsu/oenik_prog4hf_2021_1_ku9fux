﻿// <copyright file="OrderLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyCakeShop.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using MyCakeShop.Data;
    using MyCakeShop.Repository;

    /// <summary>
    /// Logic class for the Order.
    /// </summary>
    public class OrderLogic : IOrderLogic
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OrderLogic"/> class.
        /// </summary>
        /// <param name="orderRepository">order repository.</param>
        /// <param name="customerRepository">customer repository.</param>
        /// <param name="cakeRepository">cake repository.</param>
        public OrderLogic(IOrderRepository orderRepository, ICustomerRepository customerRepository, ICakeRepository cakeRepository)
        {
            this.OrderRepository = orderRepository;
            this.CustomerRepository = customerRepository;
            this.CakeRepository = cakeRepository;
        }

        /// <summary>
        /// Gets or sets Cake type repository property.
        /// </summary>
        public ICakeRepository CakeRepository { get; set; }

        /// <summary>
        /// Gets or sets Customer type repository property.
        /// </summary>
        public ICustomerRepository CustomerRepository { get; set; }

        /// <summary>
        /// Gets or sets Order type repository property.
        /// </summary>
        public IOrderRepository OrderRepository { get; set; }

        /// <summary>
        /// Change address.
        /// </summary>
        /// <param name="id">Identification number what object what I want to update.</param>
        /// <param name="newaddress">New address.</param>
        public void ChangeAddress(int id, string newaddress)
        {
            this.CustomerRepository.ChangeAddress(id, newaddress);
        }

        /// <summary>
        /// Change email.
        /// </summary>
        /// <param name="id">Identification number what object what I want to update.</param>
        /// <param name="newemail">New email.</param>
        public void ChangeEmail(int id, string newemail)
        {
            this.CustomerRepository.ChangeEmail(id, newemail);
        }

        /// <summary>
        /// Change order status.
        /// </summary>
        /// <param name="id">Identification number what object what I want to update.</param>
        /// <param name="neworderstatus">New order status.</param>
        public void ChangeOrderStatus(int id, string neworderstatus)
        {
            this.OrderRepository.ChangeOrderStatus(id, neworderstatus);
        }

        /// <summary>
        /// Change phone number.
        /// </summary>
        /// <param name="id">Identification number what object what I want to update.</param>
        /// <param name="newphonenumber">New phone number.</param>
        public void ChangePhoneNumber(int id, string newphonenumber)
        {
            this.CustomerRepository.ChangePhoneNumber(id, newphonenumber);
        }

        /// <summary>
        /// Returns all Cake type objects.
        /// </summary>
        /// <returns>All entity which is type of Cake.</returns>
        public IList<Cake> GetAllCakes()
        {
            return this.CakeRepository.GetAll().ToList();
        }

        /// <summary>
        /// Returns all Customer type objects.
        /// </summary>
        /// <returns>All entity which is type of Customer.</returns>
        public IList<Customer> GetAllCustomers()
        {
            return this.CustomerRepository.GetAll().ToList();
        }

        /// <summary>
        /// Returns all Order type objects.
        /// </summary>
        /// <returns>All entity which is type of Order.</returns>
        public IList<Order> GetAllOrder()
        {
            return this.OrderRepository.GetAll().ToList();
        }

        /// <summary>
        /// Customer active orders.
        /// </summary>
        /// <returns>customer active orders.</returns>
        public IList<CustomerOrders> GetCustomerOrders()
        {
            var q = from cust in this.CustomerRepository.GetAll()
                     join order in this.OrderRepository.GetAll() on cust.CustomerID equals order.CustomerID
                     where order.OrderStatus == "Aktív"
                     orderby cust.LastName
                     select new CustomerOrders
                     {
                         Id = cust.CustomerID,
                         Name = cust.LastName + " " + cust.FirstName,
                         OrderId = order.OrderID,
                     };
            return q.ToList();
        }

        /// <summary>
        /// Task for CustomerOrders.
        /// </summary>
        /// <returns>customerorders list.</returns>
        public Task<IList<CustomerOrders>> GetCustomerOrdersTaks()
        {
            Task<IList<CustomerOrders>> task = new Task<IList<CustomerOrders>>(() => this.GetCustomerOrders());
            task.Start();
            return task;
        }

        /// <summary>
        /// Returns a Cake type object based on id.
        /// </summary>
        /// <param name="id">Entity identification number.</param>
        /// <returns>One entity.</returns>
        public Cake GetOneCake(int id)
        {
            return this.CakeRepository.GetOne(id);
        }

        /// <summary>
        /// Returns a Customer type object based on id.
        /// </summary>
        /// <param name="id">Entity identification number.</param>
        /// <returns>One entity.</returns>
        public Customer GetOneCustomer(int id)
        {
            return this.CustomerRepository.GetOne(id);
        }

        /// <summary>
        /// Returns a Order type object based on id.
        /// </summary>
        /// <param name="id">Entity identification number.</param>
        /// <returns>One entity.</returns>
        public Order GetOneOrder(int id)
        {
            return this.OrderRepository.GetOne(id);
        }

        /// <summary>
        /// Insert customer.
        /// </summary>
        /// <param name="cust">customer object.</param>
        public void InsertCustomer(Customer cust)
        {
            this.CustomerRepository.Insert(cust);
        }

        /// <summary>
        /// Insert order.
        /// </summary>
        /// <param name="order">order object.</param>
        public void InsertOrder(Order order)
        {
            this.OrderRepository.Insert(order);
        }

        /// <summary>
        /// Delete object from database.
        /// </summary>
        /// <param name="cust">Customer type object what I want remove.</param>
        public void RemoveCustomer(Customer cust)
        {
            this.CustomerRepository.Remove(cust);
        }

        /// <summary>
        /// Delete object from database by id.
        /// </summary>
        /// <param name="id">Identification number what object what I want to remove.</param>
        public void RemoveCustomer(int id)
        {
            this.CustomerRepository.Remove(id);
        }

        /// <summary>
        /// Delete object from database.
        /// </summary>
        /// <param name="order">Order type object what I want remove.</param>
        public void RemoveOrder(Order order)
        {
            this.OrderRepository.Remove(order);
        }

        /// <summary>
        /// Delete object from database by id.
        /// </summary>
        /// <param name="id">Identification number what object what I want to remove.</param>
        public void RemoveOrder(int id)
        {
            this.OrderRepository.Remove(id);
        }
    }
}
