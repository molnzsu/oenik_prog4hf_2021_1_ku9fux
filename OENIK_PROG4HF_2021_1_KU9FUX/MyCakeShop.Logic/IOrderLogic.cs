﻿// <copyright file="IOrderLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyCakeShop.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using MyCakeShop.Data;

    /// <summary>
    /// Logic interface for the Order.
    /// </summary>
    public interface IOrderLogic
    {
        /// <summary>
        /// Returns a Order type object based on id.
        /// </summary>
        /// <param name="id">Entity identification number.</param>
        /// <returns>One entity.</returns>
        Order GetOneOrder(int id);

        /// <summary>
        /// Returns a Customer type object based on id.
        /// </summary>
        /// <param name="id">Entity identification number.</param>
        /// <returns>One entity.</returns>
        Customer GetOneCustomer(int id);

        /// <summary>
        /// Returns all Customer type objects.
        /// </summary>
        /// <returns>All entity which is type of Customer.</returns>
        IList<Customer> GetAllCustomers();

        /// <summary>
        /// Returns all Order type objects.
        /// </summary>
        /// <returns>All entity which is type of Order.</returns>
        IList<Order> GetAllOrder();

        /// <summary>
        /// Returns a Cake type object based on id.
        /// </summary>
        /// <param name="id">Entity identification number.</param>
        /// <returns>One entity.</returns>
        Cake GetOneCake(int id);

        /// <summary>
        /// Returns all Cake type objects.
        /// </summary>
        /// <returns>All entity which is type of Cake.</returns>
        IList<Cake> GetAllCakes();

        /// <summary>
        /// Change order status.
        /// </summary>
        /// <param name="id">Identification number what object what I want to update.</param>
        /// <param name="neworderstatus">New order status.</param>
        void ChangeOrderStatus(int id, string neworderstatus);

        /// <summary>
        /// Change phone number.
        /// </summary>
        /// <param name="id">Identification number what object what I want to update.</param>
        /// <param name="newphonenumber">New phone number.</param>
        void ChangePhoneNumber(int id, string newphonenumber);

        /// <summary>
        /// Change email.
        /// </summary>
        /// <param name="id">Identification number what object what I want to update.</param>
        /// <param name="newemail">New email.</param>
        void ChangeEmail(int id, string newemail);

        /// <summary>
        /// Change address.
        /// </summary>
        /// <param name="id">Identification number what object what I want to update.</param>
        /// <param name="newaddress">New address.</param>
        void ChangeAddress(int id, string newaddress);

        /// <summary>
        /// Customer active orders.
        /// </summary>
        /// <returns>customer active orders.</returns>
        IList<CustomerOrders> GetCustomerOrders();

        /// <summary>
        /// Insert customer.
        /// </summary>
        /// <param name="cust">customer object.</param>
        void InsertCustomer(Customer cust);

        /// <summary>
        /// Insert order.
        /// </summary>
        /// <param name="order">order object.</param>
        void InsertOrder(Order order);

        /// <summary>
        /// Delete object from database.
        /// </summary>
        /// <param name="cust">Customer type object what I want remove.</param>
        void RemoveCustomer(Customer cust);

        /// <summary>
        /// Delete object from database by id.
        /// </summary>
        /// <param name="id">Identification number what object what I want to remove.</param>
        void RemoveCustomer(int id);

        /// <summary>
        /// Delete object from database.
        /// </summary>
        /// <param name="order">Order type object what I want remove.</param>
        void RemoveOrder(Order order);

        /// <summary>
        /// Delete object from database by id.
        /// </summary>
        /// <param name="id">Identification number what object what I want to remove.</param>
        void RemoveOrder(int id);
    }
}
