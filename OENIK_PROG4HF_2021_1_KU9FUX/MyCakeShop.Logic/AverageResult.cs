﻿// <copyright file="AverageResult.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyCakeShop.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Class for average result.
    /// </summary>
    public class AverageResult
    {
        /// <summary>
        /// Gets or sets Type parameter.
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets Averagepopularity parameter.
        /// </summary>
        public int AveragePopularity { get; set; }

        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns>A string that represents the current object.</returns>
        public override string ToString()
        {
            return $"Type = {this.Type}, AveragePopularity = {this.AveragePopularity}";
        }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">object.</param>
        /// <returns>True if the specified object is equal to the current object, otherwise, false.</returns>
        public override bool Equals(object obj)
        {
            if (obj is AverageResult)
            {
                AverageResult other = obj as AverageResult;
                return this.Type == other.Type && this.AveragePopularity == other.AveragePopularity;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            return this.AveragePopularity;
        }
    }
}
