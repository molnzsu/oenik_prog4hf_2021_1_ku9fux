﻿// <copyright file="OrderLogicTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyCakeShop.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Moq;
    using MyCakeShop.Data;
    using MyCakeShop.Repository;
    using NUnit.Framework;

    /// <summary>
    /// Logic test for orders.
    /// </summary>
    [TestFixture]
    public class OrderLogicTest
    {
        private Mock<ICustomerRepository> custRepo;
        private Mock<IOrderRepository> orderRepo;
        private Mock<ICakeRepository> cakeRepo;
        private List<CustomerOrders> expectedPrice;

        /// <summary>
        /// GetOneOrder test.
        /// </summary>
        [Test]
        public void TestGetOneOrder()
        {
            Mock<IOrderRepository> mockedRepoO = new Mock<IOrderRepository>(MockBehavior.Loose);
            Mock<ICakeRepository> mockedRepoC = new Mock<ICakeRepository>(MockBehavior.Loose);
            Mock<ICustomerRepository> mockedRepoCu = new Mock<ICustomerRepository>(MockBehavior.Loose);
            List<Order> orders = new List<Order>()
            {
                new Order(1, 2, 1, new DateTime(2020, 10, 29), "Aktív"),
                new Order(2, 5, 5, new DateTime(2020, 10, 24), "Aktív"),
                new Order(3, 7, 2, new DateTime(2020, 10, 25), "Lezárt"),
                new Order(4, 1, 2, new DateTime(2020, 10, 26), "Aktív"),
                new Order(5, 3, 1, new DateTime(2020, 10, 30), "Lezárt"),
            };

            Order expectedOrder = new Order(4, 1, 2, new DateTime(2020, 10, 26), "Aktív");

            int id = 4;

            mockedRepoO.Setup(repo => repo.GetOne(id)).Returns(orders[3]);
            OrderLogic logic = new OrderLogic(mockedRepoO.Object, mockedRepoCu.Object, mockedRepoC.Object);

            var result = logic.GetOneOrder(id);

            Assert.That(result, Is.EqualTo(expectedOrder));

            mockedRepoO.Verify(repo => repo.GetAll(), Times.Never);
            mockedRepoO.Verify(repo => repo.GetOne(4), Times.Once);
        }

        /// <summary>
        /// Change email test.
        /// </summary>
        [Test]
        public void TestChangeEmail()
        {
            Mock<ICustomerRepository> mockedRepo = new Mock<ICustomerRepository>(MockBehavior.Loose);
            Mock<IOrderRepository> mockedRepoO = new Mock<IOrderRepository>(MockBehavior.Loose);
            Mock<ICakeRepository> mockedRepoC = new Mock<ICakeRepository>(MockBehavior.Loose);
            List<Customer> customers = new List<Customer>()
            {
                new Customer(1, "Kiss", "Andrea", "06308215464", new DateTime(1980, 12, 24), "kissa@gmail.com", "Budapest, Balázs u. 11."),
                new Customer(2, "Nagy", "Norbert", "06308215434", new DateTime(1985, 11, 20), "nagynorb@gmail.com", "Budapest, Balzac utca 70."),
                new Customer(3, "Kovács", "Attila", "06308215444", new DateTime(1992, 08, 12), "k.att@gmail.com", "Budapest, Visegrádi utca 100."),
                new Customer(4, "Horváth", "Bernadett", "06308215467", new DateTime(1975, 04, 08), "horvath@gmail.com", "Budapest, Ipoly utca 2."),
                new Customer(5, "Molnár", "Kamilla", "06308215466", new DateTime(1990, 02, 11), "molnkami@gmail.com", "Budapest, Balaton utca 33."),
            };

            int id = 1;

            mockedRepo.Setup(repo => repo.ChangeEmail(It.IsAny<int>(), It.IsAny<string>()));
            OrderLogic logic = new OrderLogic(mockedRepoO.Object, mockedRepo.Object, mockedRepoC.Object);
            logic.ChangeEmail(id, "teszt@gmail.com");

            mockedRepo.Verify(repo => repo.ChangeEmail(id, "teszt@gmail.com"), Times.Once);
        }

        /// <summary>
        /// Remove customer test.
        /// </summary>
        [Test]
        public void TestRemoveCustomer()
        {
            Mock<ICustomerRepository> mockedRepo = new Mock<ICustomerRepository>(MockBehavior.Loose);
            Mock<IOrderRepository> mockedRepoO = new Mock<IOrderRepository>(MockBehavior.Loose);
            Mock<ICakeRepository> mockedRepoC = new Mock<ICakeRepository>(MockBehavior.Loose);
            List<Customer> customers = new List<Customer>()
            {
                new Customer(1, "Kiss", "Andrea", "06308215464", new DateTime(1980, 12, 24), "kissa@gmail.com", "Budapest, Balázs u. 11."),
                new Customer(2, "Nagy", "Norbert", "06308215434", new DateTime(1985, 11, 20), "nagynorb@gmail.com", "Budapest, Balzac utca 70."),
                new Customer(3, "Kovács", "Attila", "06308215444", new DateTime(1992, 08, 12), "k.att@gmail.com", "Budapest, Visegrádi utca 100."),
                new Customer(4, "Horváth", "Bernadett", "06308215467", new DateTime(1975, 04, 08), "horvath@gmail.com", "Budapest, Ipoly utca 2."),
                new Customer(5, "Molnár", "Kamilla", "06308215466", new DateTime(1990, 02, 11), "molnkami@gmail.com", "Budapest, Balaton utca 33."),
            };

            int id = 1;

            mockedRepo.Setup(repo => repo.Remove(It.IsAny<int>()));
            OrderLogic logic = new OrderLogic(mockedRepoO.Object, mockedRepo.Object, mockedRepoC.Object);

            logic.RemoveCustomer(id);

            mockedRepo.Verify(repo => repo.Remove(id), Times.Once);
        }

        /// <summary>
        /// CustOrders test.
        /// </summary>
        [Test]
        public void TestCustOrders()
        {
            var logic = this.CreateLogicWithMocks();
            var actprice = logic.GetCustomerOrders();

            Assert.That(actprice, Is.EquivalentTo(this.expectedPrice));
            this.custRepo.Verify(repo => repo.GetAll(), Times.Once);
            this.orderRepo.Verify(repo => repo.GetAll(), Times.Once);
        }

        /// <summary>
        /// Logic create.
        /// </summary>
        /// <returns>orderlogic.</returns>
        private OrderLogic CreateLogicWithMocks()
        {
            this.orderRepo = new Mock<IOrderRepository>(MockBehavior.Loose);
            this.custRepo = new Mock<ICustomerRepository>(MockBehavior.Loose);
            this.cakeRepo = new Mock<ICakeRepository>(MockBehavior.Loose);

            List<Customer> custs = new List<Customer>()
            {
                new Customer() { CustomerID = 1,  LastName = "Teszt", FirstName = "Jani" },
                new Customer() { CustomerID = 2,  LastName = "Gipsz", FirstName = "Jakab" },
            };
            List<Order> orders = new List<Order>()
            {
                new Order() { OrderID = 1, CustomerID = 1, OrderStatus = "Aktív" },
                new Order() { OrderID = 2, CustomerID = 1, OrderStatus = "Lezárt" },
                new Order() { OrderID = 3, CustomerID = 2, OrderStatus = "Aktív" },
            };

            this.expectedPrice = new List<CustomerOrders>
            {
                new CustomerOrders() { Id = 1, Name = "Teszt Jani", OrderId = 1 },
                new CustomerOrders() { Id = 2, Name = "Gipsz Jakab", OrderId = 3 },
            };

            this.custRepo.Setup(repo => repo.GetAll()).Returns(custs.AsQueryable());
            this.orderRepo.Setup(repo => repo.GetAll()).Returns(orders.AsQueryable());

            return new OrderLogic(this.orderRepo.Object, this.custRepo.Object, this.cakeRepo.Object);
        }
    }
}
