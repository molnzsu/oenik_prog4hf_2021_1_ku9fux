﻿// <copyright file="StockLogicTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyCakeShop.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Moq;
    using MyCakeShop.Data;
    using MyCakeShop.Repository;
    using NUnit.Framework;

    /// <summary>
    /// Logic test for stocks.
    /// </summary>
    [TestFixture]
    public class StockLogicTest
    {
        private Mock<ICakeRepository> cakeRepo;
        private Mock<IOrderRepository> orderRepo;
        private Mock<IConfectionerRepository> confRepo;
        private List<AverageResult> expectedAverages;
        private List<ConfSumCake> expectedSum;

        /// <summary>
        /// GetAllCake test.
        /// </summary>
        [Test]
        public void TestGetAllCake()
        {
            Mock<ICakeRepository> mockedRepo = new Mock<ICakeRepository>(MockBehavior.Loose);
            Mock<IOrderRepository> mockedRepoC = new Mock<IOrderRepository>(MockBehavior.Loose);
            Mock<IConfectionerRepository> mockedRepoCo = new Mock<IConfectionerRepository>(MockBehavior.Loose);
            List<Cake> cakes = new List<Cake>()
            {
                new Cake(1, "Sajtos pogácsa", "sós", new DateTime(2020, 10, 26), new DateTime(2020, 10, 31), 120, false, 1, 3),
                new Cake(2, "Dobostorta", "torta", new DateTime(2020, 10, 25), new DateTime(2020, 10, 31), 5600, false, 5, 5),
                new Cake(3, "Zalakocka", "édes sütemény", new DateTime(2020, 10, 24), new DateTime(2020, 10, 31), 230, false, 3, 4),
                new Cake(4, "Rigó Jancsi", "édes sütemény", new DateTime(2020, 10, 26), new DateTime(2020, 10, 31), 360, true, 4, 3),
                new Cake(5, "Sajtos kifli", "sós", new DateTime(2020, 10, 27), new DateTime(2020, 10, 31), 100, false, 5, 10),
                new Cake(6, "Sajtos croissant", "sós", new DateTime(2020, 10, 26), new DateTime(2020, 10, 29), 230, false, 2, 7),
            };

            List<Cake> expectedcakes = new List<Cake>
            {
                new Cake(1, "Sajtos pogácsa", "sós", new DateTime(2020, 10, 26), new DateTime(2020, 10, 31), 120, false, 1, 3),
                new Cake(2, "Dobostorta", "torta", new DateTime(2020, 10, 25), new DateTime(2020, 10, 31), 5600, false, 5, 5),
                new Cake(3, "Zalakocka", "édes sütemény", new DateTime(2020, 10, 24), new DateTime(2020, 10, 31), 230, false, 3, 4),
                new Cake(4, "Rigó Jancsi", "édes sütemény", new DateTime(2020, 10, 26), new DateTime(2020, 10, 31), 360, true, 4, 3),
                new Cake(5, "Sajtos kifli", "sós", new DateTime(2020, 10, 27), new DateTime(2020, 10, 31), 100, false, 5, 10),
                new Cake(6, "Sajtos croissant", "sós", new DateTime(2020, 10, 26), new DateTime(2020, 10, 29), 230, false, 2, 7),
            };

            mockedRepo.Setup(repo => repo.GetAll()).Returns(cakes.AsQueryable());
            StockLogic logic = new StockLogic(mockedRepo.Object, mockedRepoC.Object, mockedRepoCo.Object);

            var result = logic.GetAllCakes();

            Assert.That(result.Count, Is.EqualTo(expectedcakes.Count));
            Assert.That(result, Is.EquivalentTo(expectedcakes));

            mockedRepo.Verify(repo => repo.GetAll(), Times.Once);
            mockedRepo.Verify(repo => repo.GetOne(1), Times.Exactly(0));
            mockedRepo.Verify(repo => repo.GetOne(It.IsAny<int>()), Times.Never);
        }

        /// <summary>
        /// InsertConfectioner test.
        /// </summary>
        [Test]
        public void TestInsertConfectioner()
        {
            Mock<IConfectionerRepository> confRepo = new Mock<IConfectionerRepository>(MockBehavior.Loose);
            Mock<ICakeRepository> mockedRepo = new Mock<ICakeRepository>(MockBehavior.Loose);
            Mock<IOrderRepository> mockedRepoC = new Mock<IOrderRepository>(MockBehavior.Loose);

            Confectioner conf = new Confectioner(10, "Kiss", "Zsolt", "06303388647", new DateTime(2012, 06, 15), "kisszsolt@gmail.com", new DateTime(1999, 01, 02));
            confRepo.Setup(repo => repo.Insert(It.IsAny<Confectioner>()));
            StockLogic logic = new StockLogic(mockedRepo.Object, mockedRepoC.Object, confRepo.Object);

            logic.InsertConfectioner(conf);

            confRepo.Verify(repo => repo.Insert(conf), Times.Once);
        }

        /// <summary>
        /// AverageResult test.
        /// </summary>
        [Test]
        public void TestAverageresult()
        {
            var logic = this.CreateLogicWithMocks1();
            var actualAverages = logic.GetTypeAverages();

            Assert.That(actualAverages, Is.EquivalentTo(this.expectedAverages));
            this.cakeRepo.Verify(repo => repo.GetAll(), Times.Once);
            this.orderRepo.Verify(repo => repo.GetAll(), Times.Once);
        }

        /// <summary>
        /// ConfSumCake test.
        /// </summary>
        [Test]
        public void TestConfSumCake()
        {
            var logic = this.CreateLogicWithMocks2();
            var actualSum = logic.GetConfSumCake();

            Assert.That(actualSum, Is.EquivalentTo(this.expectedSum));
            this.cakeRepo.Verify(repo => repo.GetAll(), Times.Once);
            this.confRepo.Verify(repo => repo.GetAll(), Times.Once);
        }

        private StockLogic CreateLogicWithMocks1()
        {
            this.cakeRepo = new Mock<ICakeRepository>(MockBehavior.Loose);
            this.orderRepo = new Mock<IOrderRepository>(MockBehavior.Loose);
            this.confRepo = new Mock<IConfectionerRepository>(MockBehavior.Loose);

            List<Cake> cakes = new List<Cake>()
            {
                new Cake() { CakeID = 1, Popularity = 2, Type = "torta" },
                new Cake() { CakeID = 2, Popularity = 4, Type = "torta" },
                new Cake() { CakeID = 3, Popularity = 3, Type = "süti" },
                new Cake() { CakeID = 4, Popularity = 5, Type = "süti" },
            };
            List<Order> orders = new List<Order>()
            {
                new Order() { OrderID = 1, CakeID = 1 },
                new Order() { OrderID = 2, CakeID = 2 },
                new Order() { OrderID = 3, CakeID = 4 },
            };

            this.expectedAverages = new List<AverageResult>
            {
                new AverageResult() { Type = "torta", AveragePopularity = 3 },
                new AverageResult() { Type = "süti", AveragePopularity = 5 },
            };

            this.cakeRepo.Setup(repo => repo.GetAll()).Returns(cakes.AsQueryable());
            this.orderRepo.Setup(repo => repo.GetAll()).Returns(orders.AsQueryable());

            return new StockLogic(this.cakeRepo.Object, this.orderRepo.Object, this.confRepo.Object);
        }

        /// <summary>
        /// Logig create.
        /// </summary>
        /// <returns>stocklogic.</returns>
        private StockLogic CreateLogicWithMocks2()
        {
            this.cakeRepo = new Mock<ICakeRepository>(MockBehavior.Loose);
            this.confRepo = new Mock<IConfectionerRepository>(MockBehavior.Loose);
            this.orderRepo = new Mock<IOrderRepository>(MockBehavior.Loose);

            List<Cake> cakes = new List<Cake>()
            {
                new Cake() { CakeID = 1, MakerID = 1 },
                new Cake() { CakeID = 2, MakerID = 1 },
                new Cake() { CakeID = 3, MakerID = 1 },
                new Cake() { CakeID = 4, MakerID = 2 },
            };
            List<Confectioner> confs = new List<Confectioner>()
            {
                new Confectioner() { ConfectionerID = 1,  LastName = "Teszt", FirstName = "Jani" },
                new Confectioner() { ConfectionerID = 2,  LastName = "Gipsz", FirstName = "Jakab" },
            };

            this.expectedSum = new List<ConfSumCake>
            {
                new ConfSumCake() { Id = 1, ConfName = "Teszt Jani", SumCake = 3 },
                new ConfSumCake() { Id = 2, ConfName = "Gipsz Jakab", SumCake = 1 },
            };

            this.cakeRepo.Setup(repo => repo.GetAll()).Returns(cakes.AsQueryable());
            this.confRepo.Setup(repo => repo.GetAll()).Returns(confs.AsQueryable());

            return new StockLogic(this.cakeRepo.Object, this.orderRepo.Object, this.confRepo.Object);
        }
    }
}
