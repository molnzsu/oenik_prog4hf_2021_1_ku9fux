﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "<Incorrible>", Scope = "member", Target = "~P:MyCakeShop.Data.Cake.Orders")]
[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "<Incorrible>", Scope = "member", Target = "~P:MyCakeShop.Data.Confectioner.Cakes")]
[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "<Incorrible>", Scope = "member", Target = "~P:MyCakeShop.Data.Customer.Orders")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1014:MarkAssembliesWithCLSCompliant", Justification = "<Incorrible>")]
