﻿// <copyright file="Order.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyCakeShop.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Text;

    /// <summary>
    /// This class contains the details of an order.
    /// </summary>
    public class Order
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Order"/> class.
        /// </summary>
        public Order()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Order"/> class.
        /// </summary>
        /// <param name="orderID">The ID of the order.</param>
        /// <param name="customerID">The ID of the customer.</param>
        /// <param name="cakeID">The ID of the cake.</param>
        /// <param name="orderDate">The date of the order.</param>
        /// <param name="orderStatus">The status of the order.</param>
        public Order(int orderID, int customerID, int cakeID, DateTime orderDate, string orderStatus)
        {
            this.OrderID = orderID;
            this.CustomerID = customerID;
            this.CakeID = cakeID;
            this.OrderDate = orderDate;
            this.OrderStatus = orderStatus;
        }

        /// <summary>
        /// Gets or sets property. The ID of the order.
        /// </summary>
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int OrderID { get; set; }

        /// <summary>
        /// Gets or sets property. The date of the order.
        /// </summary>
        [Required]
        public DateTime OrderDate { get; set; }

        /// <summary>
        /// Gets or sets property. The ID of the customer.
        /// </summary>
        [Required]
        public int CustomerID { get; set; }

        /// <summary>
        /// Gets or sets property. The ID of the cake.
        /// </summary>
        [Required]
        public int CakeID { get; set; }

        /// <summary>
        /// Gets or sets property. The status of the order.
        /// </summary>
        public string OrderStatus { get; set; }

        /// <summary>
        /// Gets or sets virtual property. The customer belonging to the order.
        /// </summary>
        [NotMapped]
        public virtual Customer Customer { get; set; }

        /// <summary>
        /// Gets or sets virtual property. The cake belonging to the order.
        /// </summary>
        [NotMapped]
        public virtual Cake Cake { get; set; }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">object.</param>
        /// <returns>True if the specified object is equal to the current object, otherwise, false.</returns>
        public override bool Equals(object obj)
        {
            if (obj is Order)
            {
                Order other = obj as Order;
                return this.OrderID == other.OrderID &&
                    this.CakeID == other.CakeID &&
                    this.CustomerID == other.CustomerID &&
                    this.OrderDate == other.OrderDate &&
                    this.OrderStatus == other.OrderStatus;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Override ToString.
        /// </summary>
        /// <returns>string.</returns>
        public override string ToString()
        {
            return $"Id = {this.OrderID}, CakeId = {this.CakeID}, CustomerId = {this.CustomerID}, Orderdate = {this.OrderDate}, OrderStatus = {this.OrderStatus}";
        }

        /// <summary>
        /// Override GetHashCode.
        /// </summary>
        /// <returns>int.</returns>
        public override int GetHashCode()
        {
            return this.OrderID;
        }
    }
}
