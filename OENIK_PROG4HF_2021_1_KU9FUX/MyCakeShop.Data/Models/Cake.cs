﻿// <copyright file="Cake.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyCakeShop.Data
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using GalaSoft.MvvmLight;
    using MyCakeShop.Data;

    /// <summary>
    /// This class contains the details of a cake.
    /// </summary>
    public class Cake
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Cake"/> class.
        /// </summary>
        public Cake()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Cake"/> class.
        /// </summary>
        /// <param name="cakeID">The ID of the cake.</param>
        /// <param name="name">The name of the cake.</param>
        /// <param name="type">The type of cake.</param>
        /// <param name="madeDate">The date the cake was made.</param>
        /// <param name="expirationDate">The expiration date of the cake.</param>
        /// <param name="price">The price of the cake in HUF per piece.</param>
        /// <param name="lactoseFree">This cake lactose free.</param>
        /// <param name="popularity">How popular the particular cake is among consumers.</param>
        /// <param name="makerID">The ID of the maker of the cake.</param>
        public Cake(int cakeID, string name, string type, DateTime madeDate, DateTime expirationDate, int price, bool lactoseFree, int popularity, int makerID)
        {
            this.CakeID = cakeID;
            this.Name = name;
            this.Type = type;
            this.MadeDate = madeDate;
            this.ExpirationDate = expirationDate;
            this.Price = price;
            this.LactoseFree = lactoseFree;
            this.Popularity = popularity;
            this.MakerID = makerID;
            this.Orders = new HashSet<Order>();
        }

        /// <summary>
        /// Gets or sets property. The ID of the cake.
        /// </summary>
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CakeID { get; set; }

        /// <summary>
        /// Gets or sets property. The name of the cake.
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets property. The type of cake.
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets property. The date the cake was made.
        /// </summary>
        [Required]
        public DateTime MadeDate { get; set; }

        /// <summary>
        /// Gets or sets property. The expiration date of the cake.
        /// </summary>
        public DateTime ExpirationDate { get; set; }

        /// <summary>
        /// Gets or sets property. The price of the cake in HUF per piece.
        /// </summary>
        public int Price { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether gets or sets property. This cake lactose free.
        /// </summary>
        public bool LactoseFree { get; set; }

        /// <summary>
        /// Gets or sets property. How popular the particular cake is among consumers.
        /// </summary>
        public int Popularity { get; set; }

        /// <summary>
        /// Gets or sets property. The ID of the maker of the cake.
        /// </summary>
        [Required]
        public int MakerID { get; set; }

        /// <summary>
        /// Gets or sets ICollection. Includes orders that include that particular cake.
        /// </summary>
        [NotMapped]
        public virtual ICollection<Order> Orders { get; set; }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">object.</param>
        /// <returns>True if the specified object is equal to the current object, otherwise, false.</returns>
        public override bool Equals(object obj)
        {
            if (obj is Cake)
            {
                Cake other = obj as Cake;
                return this.CakeID == other.CakeID &&
                    this.Name == other.Name &&
                    this.Type == other.Type &&
                    this.MadeDate == other.MadeDate &&
                    this.ExpirationDate == other.ExpirationDate &&
                    this.Price == other.Price &&
                    this.LactoseFree == other.LactoseFree &&
                    this.Popularity == other.Popularity &&
                    this.MakerID == other.MakerID;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Override ToString.
        /// </summary>
        /// <returns>string.</returns>
        public override string ToString()
        {
            return $"Id = {this.CakeID}, Name = {this.Name}, Típus = {this.Type}, MadeDate = {this.MadeDate}, ExpirationDate = {this.ExpirationDate}, Price = {this.Price}, Lactosefree = {this.LactoseFree}, Popularity = {this.Popularity}, MakerID = {this.MakerID}";
        }

        /// <summary>
        /// Override GetHashCode.
        /// </summary>
        /// <returns>int.</returns>
        public override int GetHashCode()
        {
            return this.CakeID;
        }
    }
}