﻿// <copyright file="Customer.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyCakeShop.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Text;
    using Castle.Components.DictionaryAdapter;
    using MyCakeShop.Data;
    using KeyAttribute = System.ComponentModel.DataAnnotations.KeyAttribute;

    /// <summary>
    /// This class contains the details of a customer.
    /// </summary>
    public class Customer
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Customer"/> class.
        /// </summary>
        public Customer()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Customer"/> class.
        /// </summary>
        /// <param name="customerID">The ID of the customer.</param>
        /// <param name="lastName">The last name of the customer.</param>
        /// <param name="firstName">The first name of the customer.</param>
        /// <param name="phoneNumber">The phone number of the customer.</param>
        /// <param name="birthDate">The birth date of the customer.</param>
        /// <param name="email">The email address of the customer.</param>
        /// <param name="address">The address of the customer.</param>
        public Customer(int customerID, string lastName, string firstName, string phoneNumber, DateTime birthDate, string email, string address)
        {
            this.CustomerID = customerID;
            this.LastName = lastName;
            this.FirstName = firstName;
            this.PhoneNumber = phoneNumber;
            this.BirthDate = birthDate;
            this.Email = email;
            this.Address = address;
            this.Orders = new HashSet<Order>();
        }

        /// <summary>
        /// Gets or sets property. The ID of the customer.
        /// </summary>
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CustomerID { get; set; }

        /// <summary>
        /// Gets or sets property. The last name of the customer.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets property. The first name of the customer.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets property. The phone number of the customer.
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Gets or sets property. The birth date of the customer.
        /// </summary>
        public DateTime BirthDate { get; set; }

        /// <summary>
        /// Gets or sets property. The email address of the customer.
        /// </summary>
        [MaxLength(50)]
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets property. The address of the customer.
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Gets or sets ICollection. Includes orders that include that particular customer.
        /// </summary>
        [NotMapped]
        public virtual ICollection<Order> Orders { get; set; }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">object.</param>
        /// <returns>True if the specified object is equal to the current object, otherwise, false.</returns>
        public override bool Equals(object obj)
        {
            if (obj is Customer)
            {
                Customer other = obj as Customer;
                return this.CustomerID == other.CustomerID &&
                    this.LastName == other.LastName &&
                    this.FirstName == other.FirstName &&
                    this.PhoneNumber == other.PhoneNumber &&
                    this.Email == other.Email &&
                    this.Address == other.Address &&
                    this.BirthDate == other.BirthDate;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Override ToString.
        /// </summary>
        /// <returns>string.</returns>
        public override string ToString()
        {
            return $"Id = {this.CustomerID}, LastName = {this.LastName}, FirstName = {this.FirstName}, PhoneNumber = {this.PhoneNumber}, Email = {this.Email}, Address = {this.Address}, Birthdate = {this.BirthDate}";
        }

        /// <summary>
        /// Override GetHashCode.
        /// </summary>
        /// <returns>int.</returns>
        public override int GetHashCode()
        {
            return this.CustomerID;
        }
    }
}
