﻿// <copyright file="Confectioner.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyCakeShop.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Text;

    /// <summary>
    /// This class contains the details of a confectioner.
    /// </summary>
    public class Confectioner
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Confectioner"/> class.
        /// </summary>
        public Confectioner()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Confectioner"/> class.
        /// </summary>
        /// <param name="confectionerID">The ID of the confectioner.</param>
        /// <param name="lastName">The last name of the confectioner.</param>
        /// <param name="firstName">The first name of the confectioner.</param>
        /// <param name="phoneNumber">The phone number of the confectioner.</param>
        /// <param name="hireDate">The hire date of the confectioner.</param>
        /// <param name="email">The email address of the confectioner.</param>
        /// <param name="birthDate">The birth date of the confectioner.</param>
        public Confectioner(int confectionerID, string lastName, string firstName, string phoneNumber, DateTime hireDate, string email, DateTime birthDate)
        {
            this.ConfectionerID = confectionerID;
            this.LastName = lastName;
            this.FirstName = firstName;
            this.PhoneNumber = phoneNumber;
            this.HireDate = hireDate;
            this.Email = email;
            this.BirthDate = birthDate;
            this.Cakes = new HashSet<Cake>();
        }

        /// <summary>
        /// Gets or sets property. The ID of the confectioner.
        /// </summary>
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ConfectionerID { get; set; }

        /// <summary>
        /// Gets or sets property. The last name of the confectioner.
        /// </summary>
        [Required]
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets property. The first name of the confectioner.
        /// </summary>
        [Required]
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets property. The phone number of the confectioner.
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Gets or sets property. The hire date of the confectioner.
        /// </summary>
        public DateTime HireDate { get; set; }

        /// <summary>
        /// Gets or sets property. The email address of the confectioner.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets property. The birth date of the confectioner.
        /// </summary>
        public DateTime BirthDate { get; set; }

        /// <summary>
        /// Gets or sets ICollection. Includes cakes made by the confectioner.
        /// </summary>
        [NotMapped]
        public virtual ICollection<Cake> Cakes { get; set; }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">object.</param>
        /// <returns>True if the specified object is equal to the current object, otherwise, false.</returns>
        public override bool Equals(object obj)
        {
            if (obj is Confectioner)
            {
                Confectioner other = obj as Confectioner;
                return this.ConfectionerID == other.ConfectionerID &&
                    this.LastName == other.LastName &&
                    this.FirstName == other.FirstName &&
                    this.PhoneNumber == other.PhoneNumber &&
                    this.HireDate == other.HireDate &&
                    this.Email == other.Email &&
                    this.BirthDate == other.BirthDate;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Override ToString.
        /// </summary>
        /// <returns>string.</returns>
        public override string ToString()
        {
            return $"Id = {this.ConfectionerID}, LastName = {this.LastName}, FirstName = {this.FirstName}, PhoneNumber = {this.PhoneNumber}, Email = {this.Email}, HireDate = {this.HireDate}, Birthdate = {this.BirthDate}";
        }

        /// <summary>
        /// Override GetHashCode.
        /// </summary>
        /// <returns>int.</returns>
        public override int GetHashCode()
        {
            return this.ConfectionerID;
        }
    }
}
