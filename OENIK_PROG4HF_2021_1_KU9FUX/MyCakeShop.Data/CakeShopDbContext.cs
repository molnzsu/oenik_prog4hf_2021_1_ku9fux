﻿// <copyright file="CakeShopDbContext.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyCakeShop.Data
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// DbContext partial class for database.
    /// </summary>
    public partial class CakeShopDbContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CakeShopDbContext"/> class.
        /// </summary>
        public CakeShopDbContext()
        {
            this.Database.EnsureCreated();
        }

        /// <summary>
        /// Gets or sets virtual DbSet objects for cake.
        /// </summary>
        public virtual DbSet<Cake> Cakes { get; set; }

        /// <summary>
        /// Gets or sets virtual DbSet objects for confectioner.
        /// </summary>
        public virtual DbSet<Confectioner> Confectioners { get; set; }

        /// <summary>
        /// Gets or sets virtual DbSet objects for customer.
        /// </summary>
        public virtual DbSet<Customer> Customers { get; set; }

        /// <summary>
        /// Gets or sets virtual DbSet objects for order.
        /// </summary>
        public virtual DbSet<Order> Orders { get; set; }

        /// <summary>
        /// Navigation property creation.
        /// </summary>
        /// <param name="modelBuilder">The modelbuilder.</param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            if (modelBuilder != null)
            {
                modelBuilder.Entity<Order>(entity =>
                {
                    entity.HasOne(orders => orders.Cake).WithMany(cakes => cakes.Orders).HasForeignKey(orders => orders.CakeID).OnDelete(DeleteBehavior.Cascade);
                });

                modelBuilder.Entity<Order>(entity =>
                {
                    entity.HasOne(orders => orders.Customer).WithMany(customers => customers.Orders).HasForeignKey(orders => orders.CustomerID).OnDelete(DeleteBehavior.Cascade);
                });
            }

            Confectioner conf1 = new Confectioner(1, "Kiss", "Angéla", "06308364158", new DateTime(2012, 01, 21), "kang@gmail.com", new DateTime(1972, 10, 05));
            Confectioner conf2 = new Confectioner(2, "Molnár", "Miklós", "06307254968", new DateTime(2020, 01, 01), "molmik@gmail.com", new DateTime(1985, 09, 10));
            Confectioner conf3 = new Confectioner(3, "Kovács", "Henrietta", "06305284512", new DateTime(2015, 10, 30), "kovhen@gmail.com", new DateTime(1990, 12, 27));
            Confectioner conf4 = new Confectioner(4, "Horváth", "György", "06304253866", new DateTime(2014, 08, 21), "hogy@gmail.com", new DateTime(1994, 10, 24));
            Confectioner conf5 = new Confectioner(5, "Németh", "Attila", "06302438556", new DateTime(2019, 11, 24), "nemeth.a@gmail.com", new DateTime(1984, 01, 05));
            Confectioner conf6 = new Confectioner(6, "Nagy", "Krisztina", "06305377865", new DateTime(2008, 06, 26), "nagykri@gmail.com", new DateTime(1989, 06, 09));
            Confectioner conf7 = new Confectioner(7, "Halász", "Klaudia", "06302345443", new DateTime(2012, 12, 21), "halkla@gmail.com", new DateTime(1995, 11, 24));
            Confectioner conf8 = new Confectioner(8, "Károly", "József", "06301233491", new DateTime(2018, 05, 02), "karoly@gmail.com", new DateTime(1981, 04, 15));
            Confectioner conf9 = new Confectioner(9, "Szabó", "Judit", "06304425676", new DateTime(2015, 07, 12), "sz.judi@gmail.com", new DateTime(1993, 08, 30));
            Confectioner conf10 = new Confectioner(10, "Kiss", "Zsolt", "06303388647", new DateTime(2012, 06, 15), "kisszsolt@gmail.com", new DateTime(1999, 01, 02));

            Cake cake1 = new Cake(1, "Sajtos pogácsa", "sós", new DateTime(2020, 10, 26), new DateTime(2020, 10, 31), 120, false, 1, 3);
            Cake cake2 = new Cake(2, "Dobostorta", "torta", new DateTime(2020, 10, 25), new DateTime(2020, 10, 31), 5600, false, 5, 5);
            Cake cake3 = new Cake(3, "Zalakocka", "édes", new DateTime(2020, 10, 24), new DateTime(2020, 10, 31), 230, false, 3, 4);
            Cake cake4 = new Cake(4, "Rigó Jancsi", "édes", new DateTime(2020, 10, 26), new DateTime(2020, 10, 31), 360, true, 4, 3);
            Cake cake5 = new Cake(5, "Sajtos kifli", "sós", new DateTime(2020, 10, 27), new DateTime(2020, 10, 31), 100, false, 5, 10);
            Cake cake6 = new Cake(6, "Sajtos croissant", "sós", new DateTime(2020, 10, 26), new DateTime(2020, 10, 29), 230, false, 2, 7);
            Cake cake7 = new Cake(7, "Csokis croissant", "édes", new DateTime(2020, 10, 28), new DateTime(2020, 10, 29), 250, false, 5, 6);
            Cake cake8 = new Cake(8, "Csokis fánk", "fánk", new DateTime(2020, 10, 25), new DateTime(2020, 10, 29), 180, true, 5, 3);
            Cake cake9 = new Cake(9, "Puncsos fánk", "fánk", new DateTime(2020, 10, 24), new DateTime(2020, 10, 29), 190, false, 4, 3);
            Cake cake10 = new Cake(10, "Vaníliás fánk", "fánk", new DateTime(2020, 10, 26), new DateTime(2020, 11, 02), 170, false, 5, 3);
            Cake cake11 = new Cake(11, "Sajttorta", "torta", new DateTime(2020, 10, 23), new DateTime(2020, 10, 29), 3200, false, 3, 2);
            Cake cake12 = new Cake(12, "Csokitorta", "torta", new DateTime(2020, 10, 22), new DateTime(2020, 10, 30), 4400, true, 5, 1);
            Cake cake14 = new Cake(13, "Citromtorta", "torta", new DateTime(2020, 10, 21), new DateTime(2020, 10, 29), 4200, false, 4, 9);
            Cake cake15 = new Cake(14, "Gyümölcstorta", "torta", new DateTime(2020, 10, 26), new DateTime(2020, 10, 30), 7800, true, 3, 8);
            Cake cake16 = new Cake(15, "Csokis muffin", "édes", new DateTime(2020, 10, 24), new DateTime(2020, 10, 26), 110, false, 5, 8);
            Cake cake17 = new Cake(16, "Pudingos muffin", "édes", new DateTime(2020, 10, 24), new DateTime(2020, 10, 29), 190, false, 5, 2);

            Customer cust1 = new Customer(1, "Kiss", "Andrea", "06308215464", new DateTime(1980, 12, 24), "kissa@gmail.com", "Budapest, Balázs u. 11.");
            Customer cust2 = new Customer(2, "Nagy", "Norbert", "06308215434", new DateTime(1985, 11, 20), "nagynorb@gmail.com", "Budapest, Balzac utca 70.");
            Customer cust3 = new Customer(3, "Kovács", "Attila", "06308215444", new DateTime(1992, 08, 12), "k.att@gmail.com", "Budapest, Visegrádi utca 100.");
            Customer cust4 = new Customer(4, "Horváth", "Bernadett", "06308215467", new DateTime(1975, 04, 08), "horvath@gmail.com", "Budapest, Ipoly utca 2.");
            Customer cust5 = new Customer(5, "Molnár", "Kamilla", "06308215466", new DateTime(1990, 02, 11), "molnkami@gmail.com", "Budapest, Balaton utca 33.");
            Customer cust6 = new Customer(6, "Nagy", "András", "06308215433", new DateTime(1993, 06, 02), "n.andras@gmail.com", "Budapest, Pozsonyi út 20.");
            Customer cust7 = new Customer(7, "Kiss", "Zsolt", "06308215411", new DateTime(1972, 10, 21), "kzs@gmail.com", "Budapest, Tüzér utca 45.");
            Customer cust8 = new Customer(8, "Juhász", "Evelin", "06308215422", new DateTime(1968, 12, 24), "juhev@gmail.com", "Budapest, Dózsa György út 134.");
            Customer cust9 = new Customer(9, "Halász", "Erzsébet", "06308215431", new DateTime(1989, 01, 12), "halasze@gmail.com", "Budapest, Vaskapu utca 78.");
            Customer cust10 = new Customer(10, "Falusi", "Ádám", "06308215454", new DateTime(1970, 05, 02), "falusi@gmail.com", "Budapest, Fehérvári út 68.");
            Customer cust11 = new Customer(11, "Mészáros", "Lili", "06308215453", new DateTime(1960, 11, 10), "mili@gmail.com", "Budapest, Dobóvári út 32.");
            Customer cust12 = new Customer(12, "Molnár", "Barnabás", "06308215452", new DateTime(1962, 08, 24), "moln.barna@gmail.com", "Budapest, Ecsed utca 24.");
            Customer cust13 = new Customer(13, "Szabó", "Ákos", "06308215451", new DateTime(1975, 07, 20), "szabo.akos@gmail.com", "Budapest, Ercsi út 50.");
            Customer cust14 = new Customer(14, "Péter", "Petra", "06308215450", new DateTime(1991, 12, 01), "petpet@gmail.com", "Budapest, Bocskai út 21.");
            Customer cust15 = new Customer(15, "Kovács", "Alexandra", "06308215449", new DateTime(1989, 01, 01), "kovalexa@gmail.com", "Budapest, Vadon utca 13.");
            Customer cust16 = new Customer(16, "Szabó", "Bence", "06308215445", new DateTime(1986, 10, 28), "szbence@gmail.com", "Budapest, Dolgos utca 5.");
            Customer cust17 = new Customer(17, "Nagy", "Aladár", "06308215441", new DateTime(1974, 08, 12), "naladar@gmail.com", "Budapest, Várfok utca 26.");

            Order order1 = new Order(1, 2, 1, new DateTime(2020, 10, 29), "Aktív");
            Order order2 = new Order(2, 5, 5, new DateTime(2020, 10, 24), "Aktív");
            Order order3 = new Order(3, 7, 2, new DateTime(2020, 10, 25), "Lezárt");
            Order order4 = new Order(4, 1, 2, new DateTime(2020, 10, 26), "Aktív");
            Order order5 = new Order(5, 3, 1, new DateTime(2020, 10, 30), "Lezárt");
            Order order6 = new Order(6, 4, 9, new DateTime(2020, 10, 29), "Aktív");
            Order order7 = new Order(7, 9, 2, new DateTime(2020, 10, 27), "Aktív");
            Order order8 = new Order(8, 10, 1, new DateTime(2020, 10, 28), "Aktív");
            Order order9 = new Order(9, 10, 4, new DateTime(2020, 10, 29), "Lezárt");
            Order order10 = new Order(10, 2, 12, new DateTime(2020, 10, 29), "Aktív");
            Order order11 = new Order(11, 2, 16, new DateTime(2020, 10, 29), "Aktív");
            Order order12 = new Order(12, 5, 10, new DateTime(2020, 10, 24), "Aktív");
            Order order13 = new Order(13, 7, 4, new DateTime(2020, 10, 25), "Lezárt");
            Order order14 = new Order(14, 1, 7, new DateTime(2020, 10, 26), "Aktív");
            Order order15 = new Order(15, 3, 13, new DateTime(2020, 10, 30), "Lezárt");
            Order order16 = new Order(16, 4, 16, new DateTime(2020, 10, 29), "Aktív");
            Order order17 = new Order(17, 9, 5, new DateTime(2020, 10, 27), "Aktív");
            Order order18 = new Order(18, 10, 11, new DateTime(2020, 10, 28), "Aktív");
            Order order19 = new Order(19, 10, 14, new DateTime(2020, 10, 29), "Lezárt");
            Order order20 = new Order(20, 12, 1, new DateTime(2020, 10, 29), "Aktív");
            Order order21 = new Order(21, 10, 3, new DateTime(2020, 10, 29), "Aktív");
            Order order22 = new Order(22, 9, 1, new DateTime(2020, 10, 24), "Aktív");
            Order order23 = new Order(23, 6, 2, new DateTime(2020, 10, 25), "Lezárt");
            Order order24 = new Order(24, 10, 2, new DateTime(2020, 10, 26), "Aktív");
            Order order25 = new Order(25, 13, 1, new DateTime(2020, 10, 30), "Lezárt");
            Order order26 = new Order(26, 14, 9, new DateTime(2020, 10, 29), "Aktív");
            Order order27 = new (27, 10, 2, new DateTime(2020, 10, 27), "Aktív");
            Order order28 = new Order(28, 6, 1, new DateTime(2020, 10, 28), "Aktív");
            Order order29 = new Order(29, 3, 4, new DateTime(2020, 10, 29), "Lezárt");

            if (modelBuilder != null)
            {
                modelBuilder.Entity<Confectioner>().HasData(conf1, conf2, conf3, conf4, conf5, conf6, conf7, conf8, conf9, conf10);
                modelBuilder.Entity<Cake>().HasData(cake1, cake10, cake11, cake12, cake14, cake15, cake16, cake17, cake2, cake3, cake4, cake5, cake6, cake7, cake8, cake9);
                modelBuilder.Entity<Customer>().HasData(cust1, cust10, cust11, cust12, cust13, cust14, cust15, cust16, cust17, cust2, cust3, cust4, cust5, cust6, cust7, cust8, cust9);
                modelBuilder.Entity<Order>().HasData(order1, order2, order3, order4, order5, order6, order7, order8, order9, order10, order11, order12, order13, order14, order15, order16, order17, order18, order19, order20, order21, order22, order23, order24, order25, order26, order27, order28, order29);
            }
        }

        /// <summary>
        /// Connection configuration.
        /// </summary>
        /// <param name="optionsBuilder">The optionsbuilder.</param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (optionsBuilder != null && !optionsBuilder.IsConfigured)
            {
                base.OnConfiguring(optionsBuilder);
                optionsBuilder
                    .UseLazyLoadingProxies()
                    .UseSqlServer(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\CakeDatabase.mdf;Integrated Security=True");
            }
        }
    }
}
