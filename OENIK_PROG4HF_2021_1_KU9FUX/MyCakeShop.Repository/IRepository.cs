﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyCakeShop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// T generic repository interface.
    /// </summary>
    /// <typeparam name="T">Generic type which is a class.</typeparam>
    public interface IRepository<T>
        where T : class
    {
        /// <summary>
        /// Returns a T type object based on id.
        /// </summary>
        /// <param name="id">Entity identification number.</param>
        /// <returns>One entity.</returns>
        T GetOne(int id);

        /// <summary>
        /// Returns all T type objects.
        /// </summary>
        /// <returns>All entity which is type of T.</returns>
        IQueryable<T> GetAll();

        /// <summary>
        /// Insert the entity in database.
        /// </summary>
        /// <param name="entity">T type object what I want to change.</param>
        void Insert(T entity);

        /// <summary>
        /// Delete object from database by id.
        /// </summary>
        /// <param name="id">Identification number what object what I want to remove.</param>
        /// <returns>True if succesfull.</returns>
        bool Remove(int id);

        /// <summary>
        /// Delete object from database.
        /// </summary>
        /// <param name="entity">T type object what I want remove.</param>
        void Remove(T entity);
    }
}
