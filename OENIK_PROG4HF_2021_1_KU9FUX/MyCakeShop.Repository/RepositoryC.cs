﻿// <copyright file="RepositoryC.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyCakeShop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Microsoft.EntityFrameworkCore;
    using MyCakeShop.Data;

    /// <summary>
    /// T generic repository class.
    /// </summary>
    /// <typeparam name="T">Generic type which is a class.</typeparam>
    public abstract class RepositoryC<T> : IRepository<T>
        where T : class
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RepositoryC{T}"/> class.
        /// </summary>
        /// <param name="ctx">Database.</param>
        protected RepositoryC(DbContext ctx)
        {
            this.Ctx = ctx;
        }

        /// <summary>
        /// Gets or sets DbContext instantiation.
        /// </summary>
        protected DbContext Ctx { get; set; }

        /// <summary>
        /// Returns all T type objects.
        /// </summary>
        /// <returns>All entity which is type of T.</returns>
        public IQueryable<T> GetAll()
        {
            return this.Ctx.Set<T>();
        }

        /// <summary>
        /// Abstract. Returns a T type object based on id.
        /// </summary>
        /// <param name="id">Entity identification number.</param>
        /// <returns>One entity.</returns>
        public abstract T GetOne(int id);

        /// <summary>
        /// Insert the entity in database.
        /// </summary>
        /// <param name="entity">T type object what I want to change.</param>
        public abstract void Insert(T entity);

        /// <summary>
        /// Abstract. Delete object from database by id.
        /// </summary>
        /// <param name="id">Identification number what object what I want to remove.</param>
        /// <returns>True if succesfull.</returns>
        public abstract bool Remove(int id);

        /// <summary>
        /// Delete object from database.
        /// </summary>
        /// <param name="entity">T type object what I want remove.</param>
        public abstract void Remove(T entity);
    }
}
