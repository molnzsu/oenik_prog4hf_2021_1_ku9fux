﻿// <copyright file="ICustomerRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyCakeShop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using MyCakeShop.Data;

    /// <summary>
    /// Customer type repository interface.
    /// </summary>
    public interface ICustomerRepository : IRepository<Customer>
    {
        /// <summary>
        /// Change phone number.
        /// </summary>
        /// <param name="id">Identification number what object what I want to update.</param>
        /// <param name="newphonenumber">New phone number.</param>
        void ChangePhoneNumber(int id, string newphonenumber);

        /// <summary>
        /// Change email.
        /// </summary>
        /// <param name="id">Identification number what object what I want to update.</param>
        /// <param name="newemail">New email.</param>
        void ChangeEmail(int id, string newemail);

        /// <summary>
        /// Change address.
        /// </summary>
        /// <param name="id">Identification number what object what I want to update.</param>
        /// <param name="newaddress">New address.</param>
        void ChangeAddress(int id, string newaddress);
    }
}
