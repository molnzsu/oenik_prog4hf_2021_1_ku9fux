﻿// <copyright file="IOrderRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyCakeShop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using MyCakeShop.Data;

    /// <summary>
    /// Order type repository interface.
    /// </summary>
    public interface IOrderRepository : IRepository<Order>
    {
        /// <summary>
        /// Change order status.
        /// </summary>
        /// <param name="id">Identification number what object what I want to update.</param>
        /// <param name="neworderstatus">New order status.</param>
        void ChangeOrderStatus(int id, string neworderstatus);
    }
}
