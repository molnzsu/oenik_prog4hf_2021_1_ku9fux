﻿// <copyright file="IConfectionerRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyCakeShop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Text;
    using MyCakeShop.Data;

    /// <summary>
    /// Confectioner type repository interface.
    /// </summary>
    public interface IConfectionerRepository : IRepository<Confectioner>
    {
        /// <summary>
        /// Change phone number.
        /// </summary>
        /// <param name="id">Identification number what object what I want to update.</param>
        /// <param name="newphonenumber">New phone number.</param>
        void ChangePhoneNumber(int id, string newphonenumber);

        /// <summary>
        /// Change phone number.
        /// </summary>
        /// <param name="id">Identification number what object what I want to update.</param>
        /// <param name="newemail">New email.</param>
        void ChangeEmail(int id, string newemail);
    }
}
