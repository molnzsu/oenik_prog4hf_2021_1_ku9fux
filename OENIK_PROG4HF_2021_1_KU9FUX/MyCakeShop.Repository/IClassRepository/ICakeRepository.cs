﻿// <copyright file="ICakeRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyCakeShop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using MyCakeShop.Data;

    /// <summary>
    /// Cake type repository interface.
    /// </summary>
    public interface ICakeRepository : IRepository<Cake>
    {
        /// <summary>
        /// Change the price.
        /// </summary>
        /// <param name="id">Identification number what object what I want to update.</param>
        /// <param name="newprice">New price.</param>
        /// <returns>True if succesfull.</returns>
        bool ChangePrice(int id, int newprice);

        /// <summary>
        /// Change expiration date.
        /// </summary>
        /// <param name="id">Identification number what object what I want to update.</param>
        /// <param name="newdate">New date.</param>
        /// <returns>True if succesfull.</returns>
        bool ChangeExpirationDate(int id, DateTime newdate);

        /// <summary>
        /// Change popularity.
        /// </summary>
        /// <param name="id">Identification number what object what I want to update.</param>
        /// <param name="newpopularity">New popularity.</param>
        /// <returns>True if succesfull.</returns>
        bool ChangePopularity(int id, int newpopularity);
    }
}
