﻿// <copyright file="OrderRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyCakeShop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Microsoft.EntityFrameworkCore;
    using MyCakeShop.Data;

    /// <summary>
    /// Order type repository class.
    /// </summary>
    public class OrderRepository : RepositoryC<Order>, IOrderRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OrderRepository"/> class.
        /// </summary>
        /// <param name="ctx">Database.</param>
        public OrderRepository(DbContext ctx)
            : base(ctx)
        {
        }

        /// <summary>
        /// Change order status.
        /// </summary>
        /// <param name="id">Identification number what object what I want to update.</param>
        /// <param name="neworderstatus">New order status.</param>
        public void ChangeOrderStatus(int id, string neworderstatus)
        {
            var order = this.GetOne(id);
            if (order == null)
            {
                throw new InvalidOperationException("not found");
            }

            order.OrderStatus = neworderstatus;
            this.Ctx.SaveChanges();
        }

        /// <summary>
        /// Returns a Order type object based on id.
        /// </summary>
        /// <param name="id">Entity identification number.</param>
        /// <returns>One entity.</returns>
        public override Order GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(x => x.OrderID == id);
        }

        /// <summary>
        /// Insert order.
        /// </summary>
        /// <param name="entity">Order.</param>
        public override void Insert(Order entity)
        {
            (this.Ctx as CakeShopDbContext).Orders.Add(entity);
            this.Ctx.SaveChanges();
        }

        /// <summary>
        /// Delete object from database by id.
        /// </summary>
        /// <param name="id">Identification number what object what I want to remove.</param>
        /// <returns>True if succesfull.</returns>
        public override bool Remove(int id)
        {
            var order = this.GetOne(id);
            if (order == null)
            {
                throw new InvalidOperationException("not found");
            }

            (this.Ctx as CakeShopDbContext).Orders.Remove(order);
            this.Ctx.SaveChanges();
            return true;
        }

        /// <summary>
        /// Remove order.
        /// </summary>
        /// <param name="entity">Order.</param>
        public override void Remove(Order entity)
        {
            (this.Ctx as CakeShopDbContext).Orders.Remove(entity);
            this.Ctx.SaveChanges();
        }
    }
}
