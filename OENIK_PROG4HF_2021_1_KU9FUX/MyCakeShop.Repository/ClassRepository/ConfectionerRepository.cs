﻿// <copyright file="ConfectionerRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyCakeShop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Microsoft.EntityFrameworkCore;
    using MyCakeShop.Data;

    /// <summary>
    /// Confectioner type repository class.
    /// </summary>
    public class ConfectionerRepository : RepositoryC<Confectioner>, IConfectionerRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ConfectionerRepository"/> class.
        /// </summary>
        /// <param name="ctx">Database.</param>
        public ConfectionerRepository(DbContext ctx)
            : base(ctx)
        {
        }

        /// <summary>
        /// Change email.
        /// </summary>
        /// <param name="id">Identification number what object what I want to update.</param>
        /// <param name="newemail">New email.</param>
        public void ChangeEmail(int id, string newemail)
        {
            var conf = this.GetOne(id);
            if (conf == null)
            {
                throw new InvalidOperationException("not found");
            }

            conf.Email = newemail;
            this.Ctx.SaveChanges();
        }

        /// <summary>
        /// Change phone number.
        /// </summary>
        /// <param name="id">Identification number what object what I want to update.</param>
        /// <param name="newphonenumber">New phone number.</param>
        public void ChangePhoneNumber(int id, string newphonenumber)
        {
            var conf = this.GetOne(id);
            if (conf == null)
            {
                throw new InvalidOperationException("not found");
            }

            conf.PhoneNumber = newphonenumber;
            this.Ctx.SaveChanges();
        }

        /// <summary>
        /// Returns a Confectioner type object based on id.
        /// </summary>
        /// <param name="id">Entity identification number.</param>
        /// <returns>One entity.</returns>
        public override Confectioner GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(x => x.ConfectionerID == id);
        }

        /// <summary>
        /// Insert confecioner.
        /// </summary>
        /// <param name="entity">Confectioner.</param>
        public override void Insert(Confectioner entity)
        {
            (this.Ctx as CakeShopDbContext).Confectioners.Add(entity);
            this.Ctx.SaveChanges();
        }

        /// <summary>
        /// Delete object from database by id.
        /// </summary>
        /// <param name="id">Identification number what object what I want to remove.</param>
        /// <returns>True if succesfull.</returns>
        public override bool Remove(int id)
        {
            var conf = this.GetOne(id);
            if (conf == null)
            {
                throw new InvalidOperationException("not found");
            }

            (this.Ctx as CakeShopDbContext).Confectioners.Remove(conf);
            this.Ctx.SaveChanges();
            return true;
        }

        /// <summary>
        /// Remove confectioner.
        /// </summary>
        /// <param name="entity">Confectioner.</param>
        public override void Remove(Confectioner entity)
        {
            (this.Ctx as CakeShopDbContext).Confectioners.Remove(entity);
            this.Ctx.SaveChanges();
        }
    }
}
