﻿// <copyright file="CakeRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyCakeShop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Microsoft.EntityFrameworkCore;
    using MyCakeShop.Data;

    /// <summary>
    /// Cake type repository class.
    /// </summary>
    public class CakeRepository : RepositoryC<Cake>, ICakeRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CakeRepository"/> class.
        /// </summary>
        /// <param name="ctx">Database.</param>
        public CakeRepository(DbContext ctx)
            : base(ctx)
        {
        }

        /// <summary>
        /// Change expiration date.
        /// </summary>
        /// <param name="id">Identification number what object what I want to update.</param>
        /// <param name="newdate">New date.</param>
        /// <returns>True if succesfull.</returns>
        public bool ChangeExpirationDate(int id, DateTime newdate)
        {
            var cake = this.GetOne(id);
            if (cake == null)
            {
                throw new InvalidOperationException("not found");
            }

            cake.ExpirationDate = newdate;
            this.Ctx.SaveChanges();
            return true;
        }

        /// <summary>
        /// Change popularity.
        /// </summary>
        /// <param name="id">Identification number what object what I want to update.</param>
        /// <param name="newpopularity">New popularity.</param>
        /// <returns>True if succesfull.</returns>
        public bool ChangePopularity(int id, int newpopularity)
        {
            var cake = this.GetOne(id);
            if (cake == null)
            {
                throw new InvalidOperationException("not found");
            }

            cake.Popularity = newpopularity;
            this.Ctx.SaveChanges();
            return true;
        }

        /// <summary>
        /// Change the price.
        /// </summary>
        /// <param name="id">Identification number what object what I want to update.</param>
        /// <param name="newprice">New price.</param>
        /// <returns>True if succesfull.</returns>
        public bool ChangePrice(int id, int newprice)
        {
            var cake = this.GetOne(id);
            if (cake == null)
            {
                throw new InvalidOperationException("not found");
            }

            cake.Price = newprice;
            this.Ctx.SaveChanges();
            return true;
        }

        /// <summary>
        /// Returns a Cake type object based on id.
        /// </summary>
        /// <param name="id">Entity identification number.</param>
        /// <returns>One entity.</returns>
        public override Cake GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(x => x.CakeID == id);
        }

        /// <inheritdoc/>
        public override void Insert(Cake entity)
        {
            (this.Ctx as CakeShopDbContext).Cakes.Add(entity);
            this.Ctx.SaveChanges();
        }

        /// <summary>
        /// Delete object from database by id.
        /// </summary>
        /// <param name="id">Identification number what object what I want to remove.</param>
        /// <returns>True if succesfull.</returns>
        public override bool Remove(int id)
        {
            var cake = this.GetOne(id);
            if (cake == null)
            {
                throw new InvalidOperationException("not found");
            }

            (this.Ctx as CakeShopDbContext).Cakes.Remove(cake);
            this.Ctx.SaveChanges();
            return true;
        }

        /// <inheritdoc/>
        public override void Remove(Cake entity)
        {
            (this.Ctx as CakeShopDbContext).Cakes.Remove(entity);
            this.Ctx.SaveChanges();
        }
    }
}
