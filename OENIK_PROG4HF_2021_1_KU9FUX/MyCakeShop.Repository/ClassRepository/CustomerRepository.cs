﻿// <copyright file="CustomerRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyCakeShop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Microsoft.EntityFrameworkCore;
    using MyCakeShop.Data;

    /// <summary>
    /// Customer type repository class.
    /// </summary>
    public class CustomerRepository : RepositoryC<Customer>, ICustomerRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerRepository"/> class.
        /// </summary>
        /// <param name="ctx">Database.</param>
        public CustomerRepository(DbContext ctx)
            : base(ctx)
        {
        }

        /// <summary>
        /// Change address.
        /// </summary>
        /// <param name="id">Identification number what object what I want to update.</param>
        /// <param name="newaddress">New address.</param>
        public void ChangeAddress(int id, string newaddress)
        {
            var custom = this.GetOne(id);
            if (custom == null)
            {
                throw new InvalidOperationException("not found");
            }

            custom.Address = newaddress;
            this.Ctx.SaveChanges();
        }

        /// <summary>
        /// Change email.
        /// </summary>
        /// <param name="id">Identification number what object what I want to update.</param>
        /// <param name="newemail">New email.</param>
        public void ChangeEmail(int id, string newemail)
        {
            var custom = this.GetOne(id);
            if (custom == null)
            {
                throw new InvalidOperationException("not found");
            }

            custom.Email = newemail;
            this.Ctx.SaveChanges();
        }

        /// <summary>
        /// Change phone number.
        /// </summary>
        /// <param name="id">Identification number what object what I want to update.</param>
        /// <param name="newphonenumber">New phone number.</param>
        public void ChangePhoneNumber(int id, string newphonenumber)
        {
            var custom = this.GetOne(id);
            if (custom == null)
            {
                throw new InvalidOperationException("not found");
            }

            custom.PhoneNumber = newphonenumber;
            this.Ctx.SaveChanges();
        }

        /// <summary>
        /// Returns a Customer type object based on id.
        /// </summary>
        /// <param name="id">Entity identification number.</param>
        /// <returns>One entity.</returns>
        public override Customer GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(x => x.CustomerID == id);
        }

        /// <summary>
        /// Insert customer.
        /// </summary>
        /// <param name="entity">Customer.</param>
        public override void Insert(Customer entity)
        {
            (this.Ctx as CakeShopDbContext).Customers.Remove(entity);
            this.Ctx.SaveChanges();
        }

        /// <summary>
        /// Delete object from database by id.
        /// </summary>
        /// <param name="id">Identification number what object what I want to remove.</param>
        /// <returns>True if succesfull.</returns>
        public override bool Remove(int id)
        {
            var custom = this.GetOne(id);
            if (custom == null)
            {
                throw new InvalidOperationException("not found");
            }

            (this.Ctx as CakeShopDbContext).Customers.Remove(custom);
            this.Ctx.SaveChanges();
            return true;
        }

        /// <summary>
        /// Remove customer.
        /// </summary>
        /// <param name="entity">Customer.</param>
        public override void Remove(Customer entity)
        {
            (this.Ctx as CakeShopDbContext).Customers.Remove(entity);
            this.Ctx.SaveChanges();
        }
    }
}
