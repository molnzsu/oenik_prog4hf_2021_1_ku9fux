﻿// <copyright file="MainViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyCakeShop.WpfApp.VM
{
    using System;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Input;
    using CommonServiceLocator;
    using ComponetWpfApp.Data;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;
    using MyCakeShop.Data;
    using MyCakeShop.WpfApp.BL;

    /// <summary>
    /// Main view model ViewModelBase class.
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        private ICakeLogic logic;

        /// <summary>
        /// The selected cake.
        /// </summary>
        private CakeWpf cakeSelected;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        /// <param name="logic">Cake logic.</param>
        public MainViewModel(ICakeLogic logic)
        {
            this.cakeSelected = new CakeWpf();
            this.logic = logic;
            this.Cakes = new ObservableCollection<CakeWpf>();
            if (this.IsInDesignMode)
            {
                CakeWpf c1 = new CakeWpf() { Id = 1, Name = "Cake1", Type = "sós", MadeDate = new DateTime(2021, 03, 12), ExpirationDate = new DateTime(2021, 03, 12), Price = 500, LactoseFree = false, Popularity = 4 };
                CakeWpf c2 = new CakeWpf() { Name = "Cake2" };
                this.Cakes.Add(c1);
                this.Cakes.Add(c2);
            }
            else
            {
                this.logic.GetAllCakes().ToList().ForEach(item => this.Cakes.Add(item));
            }

            this.AddCommand = new RelayCommand(() => this.logic.AddCake(this.Cakes));
            this.ModCommand = new RelayCommand(() => this.logic.ModCake(this.CakeSelected));
            this.DelCommand = new RelayCommand(() => this.logic.DelCake(this.Cakes, this.CakeSelected));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        public MainViewModel()
            : this(IsInDesignModeStatic ? null
                 : ServiceLocator.Current.GetInstance<ICakeLogic>())
        {
        }

        /// <summary>
        /// Gets OservableCollection of all cakes.
        /// </summary>
        public ObservableCollection<CakeWpf> Cakes { get; private set; }

        /// <summary>
        /// Gets or sets selected cake property.
        /// </summary>
        public CakeWpf CakeSelected
        {
            get { return this.cakeSelected; } set { this.Set(ref this.cakeSelected, value); }
        }

        /// <summary>
        /// Gets add command.
        /// </summary>
        public ICommand AddCommand { get; private set; }

        /// <summary>
        /// Gets modify command.
        /// </summary>
        public ICommand ModCommand { get; private set; }

        /// <summary>
        /// Gets delete command.
        /// </summary>
        public ICommand DelCommand { get; private set; }
    }
}
