﻿// <copyright file="EditorViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyCakeShop.WpfApp.VM
{
    using System;
    using System.Collections.ObjectModel;
    using ComponetWpfApp.Data;
    using GalaSoft.MvvmLight;
    using MyCakeShop.Data;

    /// <summary>
    /// Editor view model class.
    /// </summary>
    public class EditorViewModel : ViewModelBase
    {
        private CakeWpf cake;
        private ObservableCollection<string> types;

        /// <summary>
        /// Initializes a new instance of the <see cref="EditorViewModel"/> class.
        /// </summary>
        public EditorViewModel()
        {
            this.cake = new CakeWpf();
            this.cake.Name = "Unknown";
            this.Types = new ObservableCollection<string>();
            this.Types.Add("sós");
            this.Types.Add("édes");
            this.Types.Add("torta");
            this.Types.Add("édes");
        }

        /// <summary>
        /// Gets or sets cake property.
        /// </summary>
        public CakeWpf Cake
        {
            get { return this.cake; } set { this.Set(ref this.cake, value); }
        }

        /// <summary>
        /// Gets or sets type list.
        /// </summary>
        public ObservableCollection<string> Types { get => this.types; set => this.types = value; }
    }
}
