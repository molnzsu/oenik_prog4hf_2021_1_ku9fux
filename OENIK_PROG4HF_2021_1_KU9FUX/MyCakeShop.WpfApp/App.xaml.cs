﻿// <copyright file="App.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyCakeShop.WpfApp
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight.Ioc;
    using GalaSoft.MvvmLight.Messaging;
    using Microsoft.EntityFrameworkCore;
    using MyCakeShop.Data;
    using MyCakeShop.Logic;
    using MyCakeShop.Repository;
    using MyCakeShop.WpfApp.BL;
    using MyCakeShop.WpfApp.UI;

    /// <summary>
    /// Interaction logic for App.xaml.
    /// </summary>
    public partial class App : Application
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="App"/> class.
        /// </summary>
        public App()
        {
            ServiceLocator.SetLocatorProvider(() => MyIoc.Instance);
            MyIoc.Instance.Register<IEditorService, EditorServiceViaWindow>();
            MyIoc.Instance.Register<DbContext, CakeShopDbContext>();
            MyIoc.Instance.Register<ICakeRepository, CakeRepository>();
            MyIoc.Instance.Register<IConfectionerRepository, ConfectionerRepository>();
            MyIoc.Instance.Register<ICustomerRepository, CustomerRepository>();
            MyIoc.Instance.Register<IOrderRepository, OrderRepository>();
            MyIoc.Instance.Register<IOrderLogic, OrderLogic>();
            MyIoc.Instance.Register<IStockLogic, StockLogic>();
            MyIoc.Instance.Register<IMessenger>(() => Messenger.Default);
            MyIoc.Instance.Register<ICakeLogic, CakeLogic>();
        }
    }
}
