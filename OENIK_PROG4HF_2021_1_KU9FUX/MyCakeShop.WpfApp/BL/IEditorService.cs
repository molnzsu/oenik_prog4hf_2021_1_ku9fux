﻿// <copyright file="IEditorService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyCakeShop.WpfApp.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using ComponetWpfApp.Data;
    using MyCakeShop.Data;

    /// <summary>
    /// Editor service interface.
    /// </summary>
    public interface IEditorService
    {
        /// <summary>
        /// Edit cake.
        /// </summary>
        /// <param name="cake">Cake.</param>
        /// <returns>Bool type.</returns>
        bool EditCake(CakeWpf cake);
    }
}
