﻿// <copyright file="CakeLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyCakeShop.WpfApp.BL
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using ComponetWpfApp.Data;
    using GalaSoft.MvvmLight.Messaging;
    using MyCakeShop.Data;
    using MyCakeShop.Logic;

    /// <summary>
    /// Cake logic class.
    /// </summary>
    public class CakeLogic : ICakeLogic
    {
        private IEditorService editorService;
        private IMessenger messengerService;
        private IStockLogic logic;

        /// <summary>
        /// Initializes a new instance of the <see cref="CakeLogic"/> class.
        /// </summary>
        /// <param name="editorService">Editor service interface.</param>
        /// <param name="messengerService">Messenger service interface.</param>
        /// <param name="logic">Stock logic interface.</param>
        public CakeLogic(IEditorService editorService, IMessenger messengerService, IStockLogic logic)
        {
            this.editorService = editorService;
            this.messengerService = messengerService;
            this.logic = logic;
        }

        /// <summary>
        /// Convert to cake.
        /// </summary>
        /// <param name="cakeWpf">Web cake.</param>
        /// <returns>Data cake.</returns>
        public static Cake CakeWpfToCake(CakeWpf cakeWpf)
        {
            if (cakeWpf != null)
            {
                Cake cake = new Cake(cakeWpf.Id, cakeWpf.Name, cakeWpf.Type, cakeWpf.MadeDate, cakeWpf.ExpirationDate, cakeWpf.Price, cakeWpf.LactoseFree, cakeWpf.Popularity, cakeWpf.MakerId);
                return cake;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Convert to cake.
        /// </summary>
        /// <param name="cake">Data cake.</param>
        /// <returns>Web cake.</returns>
        public static CakeWpf CakeToCakeWpf(Cake cake)
        {
            if (cake != null)
            {
                CakeWpf cakeWpf = new CakeWpf(cake.CakeID, cake.Name, cake.Type, cake.MadeDate, cake.ExpirationDate, cake.Price, cake.LactoseFree, cake.Popularity, cake.MakerID);
                return cakeWpf;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Add cake.
        /// </summary>
        /// <param name="list">Cake list.</param>
        public void AddCake(IList<CakeWpf> list)
        {
            CakeWpf newCake = new CakeWpf();
            if (this.editorService.EditCake(newCake) == true)
            {
                if (list != null)
                {
                    list.Add(newCake);
                    this.logic.InsertCake(CakeWpfToCake(newCake));
                    this.messengerService.Send("ADD OK", "LogicResult");
                }
            }
            else
            {
                this.messengerService.Send("ADD CANCEL", "LogicResult");
            }
        }

        /// <summary>
        /// Delete a cake from list and database.
        /// </summary>
        /// <param name="list">Cake list.</param>
        /// <param name="cake">Cake to deleted.</param>
        public void DelCake(IList<CakeWpf> list, CakeWpf cake)
        {
            if (list != null)
            {
                if (cake != null && list.Remove(cake))
                {
                    this.logic.RemoveCake(CakeWpfToCake(cake));
                    this.messengerService.Send("DELETE OK", "LogicResult");
                }
                else
                {
                    this.messengerService.Send("DELETE FAILED", "LogicResult");
                }
            }
        }

        /// <summary>
        /// Modify cake.
        /// </summary>
        /// <param name="cakeToModify">Cake to modified.</param>
        public void ModCake(CakeWpf cakeToModify)
        {
            if (cakeToModify == null)
            {
                this.messengerService.Send("EDIT FAILED", "LogicResult");
                return;
            }

            CakeWpf clone = new CakeWpf();
            clone.CopyFrom(cakeToModify);
            if (this.editorService.EditCake(clone))
            {
                cakeToModify.CopyFrom(clone);

                // this.logic.GetOneCake(cakeToModify.CakeID).Copyfrom(cakeToModify);
                this.messengerService.Send("MODIFY OK", "LogicResult");
            }
            else
            {
                this.messengerService.Send("MODIFY FAILED", "LogicResult");
            }
        }

        /// <summary>
        /// Get all cakes.
        /// </summary>
        /// <returns>Cake list.</returns>
        public IList<CakeWpf> GetAllCakes()
        {
            IList<Cake> cakes = this.logic.GetAllCakes();
            IList<CakeWpf> c = new List<CakeWpf>();
            foreach (var item in cakes)
            {
                c.Add(CakeToCakeWpf(item));
            }

            return c;
        }
    }
}
