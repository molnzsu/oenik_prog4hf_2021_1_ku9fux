﻿// <copyright file="ICakeLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyCakeShop.WpfApp.BL
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using ComponetWpfApp.Data;
    using MyCakeShop.Data;

    /// <summary>
    /// Cake logic interface.
    /// </summary>
    public interface ICakeLogic
    {
        /// <summary>
        /// Add cake method.
        /// </summary>
        /// <param name="list">Cake list.</param>
        void AddCake(IList<CakeWpf> list);

        /// <summary>
        /// Modify cake method.
        /// </summary>
        /// <param name="cakeToModify">Cake to be modified.</param>
        void ModCake(CakeWpf cakeToModify);

        /// <summary>
        /// Delete cake method.
        /// </summary>
        /// <param name="list">Cake list.</param>
        /// <param name="cake">Cake to be deleted.</param>
        void DelCake(IList<CakeWpf> list, CakeWpf cake);

        /// <summary>
        /// Get all cakes.
        /// </summary>
        /// <returns>Cake list.</returns>
        IList<CakeWpf> GetAllCakes();
    }
}
