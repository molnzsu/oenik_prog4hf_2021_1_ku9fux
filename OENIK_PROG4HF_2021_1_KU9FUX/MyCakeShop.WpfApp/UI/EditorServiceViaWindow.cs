﻿// <copyright file="EditorServiceViaWindow.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyCakeShop.WpfApp.UI
{
    using ComponetWpfApp.Data;
    using MyCakeShop.Data;
    using MyCakeShop.WpfApp.BL;

    /// <summary>
    /// Editor service via window class.
    /// </summary>
    public class EditorServiceViaWindow : IEditorService
    {
        /// <summary>
        /// Edit cake.
        /// </summary>
        /// <param name="cake">Cake.</param>
        /// <returns>Bool.</returns>
        public bool EditCake(CakeWpf cake)
        {
            EditorWindow win = new EditorWindow(cake);
            return win.ShowDialog() ?? false;
        }
    }
}
