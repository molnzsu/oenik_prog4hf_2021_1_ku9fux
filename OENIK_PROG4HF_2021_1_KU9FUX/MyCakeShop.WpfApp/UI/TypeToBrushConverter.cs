﻿// <copyright file="TypeToBrushConverter.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyCakeShop.WpfApp.UI
{
    using System;
    using System.Globalization;
    using System.Windows.Data;
    using System.Windows.Media;

    /// <summary>
    /// Type to brush converter class.
    /// </summary>
    public class TypeToBrushConverter : IValueConverter
    {
        /// <summary>
        /// Convert brush.
        /// </summary>
        /// <param name="value">Value.</param>
        /// <param name="targetType">Target type.</param>
        /// <param name="parameter">Parameter.</param>
        /// <param name="culture">Culture.</param>
        /// <returns>Brushes.</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string type = (string)value;
            if (type == "sós")
            {
                return Brushes.LightYellow;
            }
            else if (type == "édes")
            {
                return Brushes.LightSeaGreen;
            }
            else if (type == "fánk")
            {
                return Brushes.LightSkyBlue;
            }
            else
            {
                return Brushes.LightSalmon;
            }
        }

        /// <summary>
        /// Convert back.
        /// </summary>
        /// <param name="value">Value.</param>
        /// <param name="targetType">Target type.</param>
        /// <param name="parameter">Parameter.</param>
        /// <param name="culture">Culture.</param>
        /// <returns>Nothing.</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
}
