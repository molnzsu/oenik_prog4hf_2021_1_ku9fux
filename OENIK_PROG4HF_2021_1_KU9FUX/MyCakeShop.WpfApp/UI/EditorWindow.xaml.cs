﻿// <copyright file="EditorWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyCakeShop.WpfApp.UI
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using ComponetWpfApp.Data;
    using MyCakeShop.Data;
    using MyCakeShop.WpfApp.VM;

    /// <summary>
    /// Interaction logic for EditorWindow.xaml.
    /// </summary>
    public partial class EditorWindow : Window
    {
        private EditorViewModel vM;

        private ObservableCollection<string> types;

        /// <summary>
        /// Initializes a new instance of the <see cref="EditorWindow"/> class.
        /// </summary>
        public EditorWindow()
        {
            this.InitializeComponent();
            this.vM = this.FindResource("VM") as EditorViewModel;
            this.Types = new ObservableCollection<string>();
            this.Types.Add("sós");
            this.Types.Add("édes");
            this.Types.Add("fánk");
            this.Types.Add("torta");
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EditorWindow"/> class.
        /// </summary>
        /// <param name="oldCake">Old cake.</param>
        public EditorWindow(CakeWpf oldCake)
            : this()
        {
            this.vM.Cake = oldCake;
        }

        /// <summary>
        /// Gets cake parameter.
        /// </summary>
        public CakeWpf Cake { get => this.vM.Cake; }

        /// <summary>
        /// Gets or sets cake types.
        /// </summary>
        public ObservableCollection<string> Types { get => this.types; set => this.types = value; }

        private void OK_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
