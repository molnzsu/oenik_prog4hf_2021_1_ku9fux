﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "<Incorrible>", Scope = "member", Target = "~P:MyCakeShop.WpfApp.UI.EditorWindow.Types")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1014:MarkAssembliesWithCLSCompliant", Justification = "<Incorrible>")]
[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "<Incorrible>", Scope = "member", Target = "~P:MyCakeShop.WpfApp.VM.EditorViewModel.Types")]
