﻿// <copyright file="MainLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyCakeShop.WpfClient
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Text;
    using System.Text.Json;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight.Messaging;

    /// <summary>
    /// Main logic class.
    /// </summary>
    public class MainLogic : IMainLogic
    {
        private string url = "http://localhost:2662/CakesApi/";
        private HttpClient client = new HttpClient();
        private JsonSerializerOptions jsonOptions = new JsonSerializerOptions(JsonSerializerDefaults.Web);

        /// <summary>
        /// Get all cakes.
        /// </summary>
        /// <returns>cake list.</returns>
        public List<CakeVM> ApiGetCakes()
        {
            string json = this.client.GetStringAsync(this.url + "all").Result;
            var list = JsonSerializer.Deserialize<List<CakeVM>>(json, this.jsonOptions);
            return list;
        }

        /// <summary>
        /// Delete cake.
        /// </summary>
        /// <param name="cake">cake.</param>
        public void ApiDelCake(CakeVM cake)
        {
            bool success = false;
            if (cake != null)
            {
                string json = this.client.GetStringAsync(this.url + "del/" + cake.Id.ToString()).Result;
                JsonDocument doc = JsonDocument.Parse(json);
                success = doc.RootElement.EnumerateObject().First().Value.GetRawText() == "true";
            }

            SendMessage(success);
        }

        /// <summary>
        /// Edit cake.
        /// </summary>
        /// <param name="cake">cake.</param>
        /// <param name="editorFunc">editor func.</param>
        public void EditCake(CakeVM cake, Func<CakeVM, bool> editorFunc)
        {
            CakeVM clone = new CakeVM();
            if (cake != null)
            {
                clone.CopyFrom(cake);
            }

            bool? success = editorFunc?.Invoke(clone);
            if (success == true)
            {
                if (cake != null)
                {
                    success = this.ApiEditCake(clone, true);
                }
            }

            SendMessage(success == true);
        }

        /// <summary>
        /// Add cake.
        /// </summary>
        /// <param name="addFunc">add func.</param>
        public void AddCake(Func<CakeVM, bool> addFunc)
        {
            CakeVM clone = new CakeVM();
            bool? success = addFunc?.Invoke(clone);
            if (success == true)
            {
                success = this.ApiEditCake(clone, false);
            }

            SendMessage(success == true);
        }

        private static void SendMessage(bool success)
        {
            string msg = success ? "Operation completed successfully" : "Operation failed";
            Messenger.Default.Send(msg, "CakeResult");
        }

        private bool ApiEditCake(CakeVM cake, bool isEditing)
        {
            if (cake == null)
            {
                return false;
            }

            string myUrl = isEditing ? this.url + "mod" : this.url + "add";

            Dictionary<string, string> postData = new Dictionary<string, string>();
            if (isEditing)
            {
                postData.Add("id", cake.Id.ToString());
            }

            postData.Add("name", cake.Name);
            postData.Add("type", cake.Type);
            postData.Add("madeDate", cake.MadeDate.ToString());
            postData.Add("expirationDate", cake.ExpirationDate.ToString());
            postData.Add("price", cake.Price.ToString());
            postData.Add("lactoseFree", cake.LactoseFree.ToString());
            postData.Add("popularity", cake.Popularity.ToString());
            postData.Add("makerId", cake.MakerId.ToString());

            string json = this.client.PostAsync(myUrl, new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
            JsonDocument doc = JsonDocument.Parse(json);
            return doc.RootElement.EnumerateObject().First().Value.GetRawText() == "true";
        }
    }
}
