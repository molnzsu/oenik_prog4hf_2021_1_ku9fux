﻿// <copyright file="MainVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyCakeShop.WpfClient
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;

    /// <summary>
    /// Main vm class.
    /// </summary>
    public class MainVM : ViewModelBase
    {
        private IMainLogic logic;
        private CakeVM selectedCake;
        private ObservableCollection<CakeVM> allCakes;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainVM"/> class.
        /// </summary>
        /// <param name="mainLogic">main logic.</param>
        public MainVM(IMainLogic mainLogic)
        {
            this.logic = mainLogic;
            this.LoadCmd = new RelayCommand(() => this.AllCakes = new ObservableCollection<CakeVM>(this.logic.ApiGetCakes()));
            this.DelCmd = new RelayCommand(() => this.logic.ApiDelCake(this.selectedCake));
            this.AddCmd = new RelayCommand(() => this.logic.AddCake(this.AddFunc));
            this.ModCmd = new RelayCommand(() => this.logic.EditCake(this.selectedCake, this.EditorFunc));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainVM"/> class.
        /// </summary>
        public MainVM()
            : this(IsInDesignModeStatic ? null
                 : ServiceLocator.Current.GetInstance<IMainLogic>())
        {
        }

        /// <summary>
        /// Gets or sets cakes collection.
        /// </summary>
        public ObservableCollection<CakeVM> AllCakes
        {
            get { return this.allCakes; }
            set { this.Set(ref this.allCakes, value); }
        }

        /// <summary>
        /// Gets or sets selected cake.
        /// </summary>
        public CakeVM SelectedCake
        {
            get { return this.selectedCake; }
            set { this.Set(ref this.selectedCake, value); }
        }

        /// <summary>
        /// Gets or sets editor func.
        /// </summary>
        public Func<CakeVM, bool> EditorFunc { get; set; }

        /// <summary>
        /// Gets or sets add func.
        /// </summary>
        public Func<CakeVM, bool> AddFunc { get; set; }

        /// <summary>
        /// Gets add command.
        /// </summary>
        public ICommand AddCmd { get; private set; }

        /// <summary>
        /// Gets delete command.
        /// </summary>
        public ICommand DelCmd { get; private set; }

        /// <summary>
        /// Gets modify command.
        /// </summary>
        public ICommand ModCmd { get; private set; }

        /// <summary>
        /// Gets load command.
        /// </summary>
        public ICommand LoadCmd { get; private set; }
    }
}
