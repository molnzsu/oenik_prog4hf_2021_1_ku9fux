﻿// <copyright file="IMainLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyCakeShop.WpfClient
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Main logic interface.
    /// </summary>
    public interface IMainLogic
    {
        /// <summary>
        /// Get all cakes.
        /// </summary>
        /// <returns>cake list.</returns>
        public List<CakeVM> ApiGetCakes();

        /// <summary>
        /// Delete cake.
        /// </summary>
        /// <param name="cake">cake.</param>
        public void ApiDelCake(CakeVM cake);

        /// <summary>
        /// Edit cake.
        /// </summary>
        /// <param name="cake">cake.</param>
        /// <param name="editorFunc">editor func.</param>
        public void EditCake(CakeVM cake, Func<CakeVM, bool> editorFunc);

        /// <summary>
        /// Add cake.
        /// </summary>
        /// <param name="addFunc">add func.</param>
        public void AddCake(Func<CakeVM, bool> addFunc);
    }
}
