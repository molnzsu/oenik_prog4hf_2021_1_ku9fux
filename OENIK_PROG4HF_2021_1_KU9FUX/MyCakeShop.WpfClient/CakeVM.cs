﻿// <copyright file="CakeVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyCakeShop.WpfClient
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// Cake vm class.
    /// </summary>
    public class CakeVM : ObservableObject
    {
        private int id;
        private string name;
        private string type;
        private DateTime madeDate;
        private DateTime expirationDate;
        private int price;
        private bool lactoseFree;
        private int popularity;
        private int makerId;

        /// <summary>
        /// Gets or sets id.
        /// </summary>
        public int Id
        {
            get { return this.id; }
            set { this.Set(ref this.id, value); }
        }

        /// <summary>
        /// Gets or sets cake name.
        /// </summary>
        public string Name
        {
            get { return this.name; }
            set { this.Set(ref this.name, value); }
        }

        /// <summary>
        /// Gets or sets property. The type of cake.
        /// </summary>
        public string Type
        {
            get { return this.type; }
            set { this.Set(ref this.type, value); }
        }

        /// <summary>
        /// Gets or sets property. The date the cake was made.
        /// </summary>
        public DateTime MadeDate
        {
            get { return this.madeDate; }
            set { this.Set(ref this.madeDate, value); }
        }

        /// <summary>
        /// Gets or sets property. The expiration date of the cake.
        /// </summary>
        public DateTime ExpirationDate
        {
            get { return this.expirationDate; }
            set { this.Set(ref this.expirationDate, value); }
        }

        /// <summary>
        /// Gets or sets property. The price of the cake in HUF per piece.
        /// </summary>
        public int Price
        {
            get { return this.price; }
            set { this.Set(ref this.price, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether gets or sets property. This cake lactose free.
        /// </summary>
        public bool LactoseFree
        {
            get { return this.lactoseFree; }
            set { this.Set(ref this.lactoseFree, value); }
        }

        /// <summary>
        /// Gets or sets property. How popular the particular cake is among consumers.
        /// </summary>
        public int Popularity
        {
            get { return this.popularity; }
            set { this.Set(ref this.popularity, value); }
        }

        /// <summary>
        /// Gets or sets property. The ID of the maker of the cake.
        /// </summary>
        public int MakerId
        {
            get { return this.makerId; }
            set { this.Set(ref this.makerId, value); }
        }

        /// <summary>
        /// Copy a cake object.
        /// </summary>
        /// <param name="other">other cake.</param>
        public void CopyFrom(CakeVM other)
        {
            if (other != null)
            {
                this.Id = other.Id;
                this.Name = other.Name;
                this.Type = other.Type;
                this.MadeDate = other.MadeDate;
                this.ExpirationDate = other.expirationDate;
                this.Type = other.Type;
                this.MadeDate = other.MadeDate;
                this.ExpirationDate = other.expirationDate;
                this.Price = other.Price;
                this.LactoseFree = other.LactoseFree;
                this.Popularity = other.Popularity;
                this.MakerId = other.MakerId;
            }
        }
    }
}
