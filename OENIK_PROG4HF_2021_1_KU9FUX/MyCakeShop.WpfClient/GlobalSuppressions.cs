﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Design", "CA1002:Do not expose generic lists", Justification = "<Uninteresting>", Scope = "member", Target = "~M:MyCakeShop.WpfClient.MainLogic.ApiGetCakes~System.Collections.Generic.List{MyCakeShop.WpfClient.CakeVM}")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<Uninteresting>", Scope = "member", Target = "~M:MyCakeShop.WpfClient.MainLogic.ApiEditCake(MyCakeShop.WpfClient.CakeVM,System.Boolean)~System.Boolean")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<Uninteresting>", Scope = "member", Target = "~M:MyCakeShop.WpfClient.MainLogic.ApiDelCake(MyCakeShop.WpfClient.CakeVM)")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1014:MarkAssembliesWithCLSCompliant", Justification = "Uninteresting")]
[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "<Uninteresting>", Scope = "member", Target = "~P:MyCakeShop.WpfClient.MainVM.AllCakes")]
[assembly: SuppressMessage("Usage", "CA2234:Pass system uri objects instead of strings", Justification = "<Uninteresting>", Scope = "member", Target = "~M:MyCakeShop.WpfClient.MainLogic.ApiEditCake(MyCakeShop.WpfClient.CakeVM,System.Boolean)~System.Boolean")]
[assembly: SuppressMessage("Usage", "CA2234:Pass system uri objects instead of strings", Justification = "<Uninteresting>", Scope = "member", Target = "~M:MyCakeShop.WpfClient.MainLogic.ApiGetCakes~System.Collections.Generic.List{MyCakeShop.WpfClient.CakeVM}")]
[assembly: SuppressMessage("Usage", "CA2234:Pass system uri objects instead of strings", Justification = "<Uninteresting>", Scope = "member", Target = "~M:MyCakeShop.WpfClient.MainLogic.ApiDelCake(MyCakeShop.WpfClient.CakeVM)")]
[assembly: SuppressMessage("Design", "CA1001:Types that own disposable fields should be disposable", Justification = "<Uninteresting>", Scope = "type", Target = "~T:MyCakeShop.WpfClient.MainLogic")]
[assembly: SuppressMessage("Reliability", "CA2000:Dispose objects before losing scope", Justification = "<Uninteresting>", Scope = "member", Target = "~M:MyCakeShop.WpfClient.MainLogic.ApiEditCake(MyCakeShop.WpfClient.CakeVM,System.Boolean)~System.Boolean")]
[assembly: SuppressMessage("Design", "CA1002:Do not expose generic lists", Justification = "<Uninteresting>", Scope = "member", Target = "~M:MyCakeShop.WpfClient.IMainLogic.ApiGetCakes~System.Collections.Generic.List{MyCakeShop.WpfClient.CakeVM}")]
